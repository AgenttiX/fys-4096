# fys-4096

TUT/TUNI FYS-4096 Computational Physics

[![pipeline status](https://gitlab.com/AgenttiX/fys-4096/badges/master/pipeline.svg)](https://gitlab.com/AgenttiX/fys-4096/commits/master)
[![coverage report](https://gitlab.com/AgenttiX/fys-4096/badges/master/coverage.svg)](https://gitlab.com/AgenttiX/fys-4096/commits/master)

Mika Mäki, 2019


Notes for installing FEniCS to a virtualenv
- [Install FEniCS as usual](https://fenicsproject.org/download/)
- The virtualenv should have system site packages enabled. If enabled, you're ready to go. Otherwise follow the instructions below.
Please note that the PyCharm tool for enabling these is broken, and these instructions should be followed instead.
- Delete your current virtualenv (and remove it from the IDE such as PyCharm)
- Create a new virtualenv by running "python3 -m venv venv --system-site-packages" in your project folder
- Add the virtualenv to your IDE (In PyCharm: File -> Settings -> Project -> Project Interpreter -> Add -> Existing environment )
- (Re)install the requirements of your project, for example by running
"pip install -r requirements.txt" with the virtualenv enabled, or by using your IDE
(PyCharm may complain that installation of packages may require administrative privileges, but the warning is false)
- Note! The Sympy used must be older than 1.2. This can be accomplished by having the line "sympy < 1.2" in the requirements.txt

Notes for running the FEniCS examples:
- The function interactive() has been removed from FEniCS, and the following can be used instead:
"import matplotlib.pyplot as plt" and "plt.show()"
