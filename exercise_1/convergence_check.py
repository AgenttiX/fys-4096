"""Convergence checker for the functions of num_calculus

Sorry for the quality of this code, I did this on Friday morning
before the exercise session since I had an exam on Thursday

As an afterthought, it might have been a better idea for the num_calculus functions to take Numpy arrays as arguments
instead of callables.
"""

import typing as tp

import matplotlib.pyplot as plt
import numba
import numpy as np

import num_calculus


# Functions to be analyzed

@numba.jit(nopython=True)
def fun1(x):
    """A simple sinusoidal function"""
    return np.sin(2*x) + 3


@numba.jit(nopython=True)
def fun1_der(x):
    return 2*np.cos(2*x)


@numba.jit(nopython=True)
def fun1_der2(x):
    return -4*np.sin(2*x)


@numba.jit(nopython=True)
def fun1_int(x):
    return -0.5*np.cos(2*x) + 3*x


@numba.jit(nopython=True)
def fun2(x):
    """A simple polynomial function"""
    return 2*(x**2) + 3*x


@numba.jit(nopython=True)
def fun2_der(x):
    return 4*x + 3


@numba.jit(nopython=True)
def fun2_der2(x):
    return 4


@numba.jit(nopython=True)
def fun2_int(x):
    return 2/3*(x**3) + 3/2*(x**2)


@numba.jit(nopython=True)
def fun3(x):
    """A more complex function"""
    return np.exp(0.1*x) + np.sin(x) - 3*x


@numba.jit(nopython=True)
def fun3_der(x):
    return 0.1*np.exp(0.1*x) + np.cos(x) - 3


@numba.jit(nopython=True)
def fun3_der2(x):
    return 0.01*np.exp(0.1*x) - np.sin(x)


@numba.jit(nopython=True)
def fun3_int(x):
    return 10*np.exp(0.1*x) - np.cos(x) - 3/2*(x**2)


# Actual code

# def der_convergence(
#         function: callable, der_function: callable, der_calculator: callable,
#         dx_min: float, dx_max: float, dx_count: int,
#         x_min: float, x_max: float, x_count: int):
#     values = np.zeros(shape=(dx_count, x_count))
#     dx_step = (dx_max - dx_min) / dx_count
#     x_step = (x_max - x_min) / x_count
#
#     for i_dx in range(dx_count):
#         for i_x in range(x_count):
#             x = x_min + i_x * x_step
#             dx = dx_min + i_dx * dx_step
#
#             values[i_dx, i_x] = der_calculator(function, x, dx) - der_function(x)
#
#     return np.mean(values, axis=1)


# @numba.jit(nopython=True, parallel=True)
def der_convergence(
        function: callable, der_function: callable, der_calculator: callable,
        dx_arr: np.ndarray, x_arr: np.ndarray):

    relative_errors = np.zeros(shape=(dx_arr.size, x_arr.size))

    for i_dx, dx in enumerate(dx_arr):
        for i_x, x in enumerate(x_arr):
            derivative = der_function(x)
            if derivative == 0:
                relative_errors[i_dx, i_x] = np.nan
            else:
                relative_errors[i_dx, i_x] = (der_calculator(function, x, dx) - derivative) / derivative

    return np.mean(relative_errors, axis=1)


@numba.jit(nopython=True, parallel=True)
def int_convergence(
        function: callable, int_function: callable, int_calculator: callable,
        steps_arr: np.ndarray, x_min: float, x_max: float):
    errors = np.zeros(steps_arr.shape)

    integral = int_function(x_max) - int_function(x_min)

    for i, steps in enumerate(steps_arr):
        errors[i] = int_calculator(function, x_min, x_max, steps) - integral

    relative_errors = errors / integral

    return relative_errors


@numba.jit(nopython=True, parallel=True)
def int_convergence_mc(
        function: callable, int_function: callable,
        iters_arr: np.ndarray, blocks: int, x_min: float, x_max: float):
    errors = np.zeros(iters_arr.shape)
    estimates = np.zeros(iters_arr.shape)

    integral = int_function(x_max) - int_function(x_min)

    for i, iters in enumerate(iters_arr):
        estimate, error_estimate = num_calculus.monte_carlo_integration(function, x_min, x_max, blocks, iters)

        errors[i] = estimate - integral
        estimates[i] = error_estimate

    relative_errors = errors / integral
    relative_estimates = estimates / integral

    return relative_errors, relative_estimates


def plotter(dx: np.ndarray, y_values: tp.List[np.ndarray], fig, subplot_indices: tp.Tuple[int, int, int], title: str, x_label: str):
    ax = fig.add_subplot(*subplot_indices)

    for i, y in enumerate(y_values):
        plt.plot(dx, y, label="f{}".format(i+1))

    plt.legend()
    ax.set_title(title)
    ax.set_xlabel(x_label)
    ax.set_ylabel("relative error")


def main():
    x_min = 0
    x_max = 10
    x_count = 10

    dx_max = 1
    dx_min = 0.00001
    dx_count = 100

    dx = np.linspace(dx_max, dx_min, dx_count)
    x = np.linspace(x_min, x_max, x_count)

    fig = plt.figure()

    err_fun1_der = der_convergence(fun1, fun1_der, num_calculus.eval_derivative, dx, x)
    err_fun2_der = der_convergence(fun2, fun2_der, num_calculus.eval_derivative, dx, x)
    err_fun3_der = der_convergence(fun3, fun3_der, num_calculus.eval_derivative, dx, x)

    plotter(dx, [err_fun1_der, err_fun2_der, err_fun3_der], fig, (2, 1, 1), "1st derivative", "dx")

    err_fun1_der2 = der_convergence(fun1, fun1_der2, num_calculus.eval_2nd_derivative, dx, x)
    err_fun2_der2 = der_convergence(fun2, fun2_der2, num_calculus.eval_2nd_derivative, dx, x)
    err_fun3_der2 = der_convergence(fun3, fun3_der2, num_calculus.eval_2nd_derivative, dx, x)

    plotter(dx, [err_fun1_der2, err_fun2_der2, err_fun3_der2], fig, (2, 1, 2), "2nd derivative", "dx")

    steps = np.linspace(1, 1000, 1000, dtype=np.int_)

    fig2 = plt.figure()

    int_dx = (x_max - x_min) / steps
    # print(int_dx)

    err_fun1_riemann = int_convergence(fun1, fun1_int, num_calculus.riemann, steps, x_min, x_max)
    err_fun2_riemann = int_convergence(fun2, fun2_int, num_calculus.riemann, steps, x_min, x_max)
    err_fun3_riemann = int_convergence(fun3, fun3_int, num_calculus.riemann, steps, x_min, x_max)

    plotter(int_dx, [err_fun1_riemann, err_fun2_riemann, err_fun3_riemann], fig2, (2, 2, 1), "Riemann", "dx")

    err_fun1_trapezoid = int_convergence(fun1, fun1_int, num_calculus.trapezoid, steps, x_min, x_max)
    err_fun2_trapezoid = int_convergence(fun2, fun2_int, num_calculus.trapezoid, steps, x_min, x_max)
    err_fun3_trapezoid = int_convergence(fun3, fun3_int, num_calculus.trapezoid, steps, x_min, x_max)

    plotter(int_dx, [err_fun1_trapezoid, err_fun2_trapezoid, err_fun3_trapezoid], fig2, (2, 2, 2), "Trapezoid", "dx")

    err_fun1_simpson = int_convergence(fun1, fun1_int, num_calculus.simpson, steps, x_min, x_max)
    err_fun2_simpson = int_convergence(fun2, fun2_int, num_calculus.simpson, steps, x_min, x_max)
    err_fun3_simpson = int_convergence(fun3, fun3_int, num_calculus.simpson, steps, x_min, x_max)
    
    plotter(int_dx, [err_fun1_simpson, err_fun2_simpson, err_fun3_simpson], fig2, (2, 2, 3), "Simpson", "dx")

    fig3 = plt.figure()

    mc_iters = np.linspace(1, 1000, 1000, dtype=np.int_)
    mc_blocks = 10

    err_fun1_mc, err_fun1_mc_est = int_convergence_mc(fun1, fun1_int, mc_iters, mc_blocks, x_min, x_max)
    err_fun2_mc, err_fun2_mc_est = int_convergence_mc(fun2, fun2_int, mc_iters, mc_blocks, x_min, x_max)
    err_fun3_mc, err_fun3_mc_est = int_convergence_mc(fun3, fun3_int, mc_iters, mc_blocks, x_min, x_max)

    plotter(mc_iters, [err_fun1_mc, err_fun2_mc, err_fun3_mc], fig3, (2, 1, 1), "Monte Carlo", "iterations")
    plotter(mc_iters, [err_fun1_mc_est, err_fun2_mc_est, err_fun3_mc_est], fig3, (2, 1, 2), "Monte Carlo estimates", "iterations")

    plt.show()


if __name__ == "__main__":
    main()
