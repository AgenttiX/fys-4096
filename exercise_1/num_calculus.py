"""Methods for numerical calculus

The corresponding unit tests are in tests.py
"""

import numba
import numpy as np


@numba.jit(nopython=True)
def eval_derivative(function: callable, x: float, dx: float) -> float:
    """Evaluate derivative according to the equation 6 of the lecture slides"""
    return (function(x + dx) - function(x - dx)) / (2*dx)


@numba.jit(nopython=True)
def eval_2nd_derivative(function: callable, x: float, dx: float) -> float:
    """Evaluate second derivative according to the equation 8 of the lecture slides"""
    return (function(x + dx) + function(x - dx) - 2*function(x))/(dx**2)


@numba.jit(nopython=True)
def riemann(function: callable, x_min: float, x_max: float, steps: int):
    """Riemann sum integration"""

    # Apparently one cannot throw Python errors from a nopython-compiled function
    # (the compilation fails if this is uncommented)
    # if x_max < x_min:
    #     raise ValueError("Invalid range {} - {}".format(x_min, x_max))
    # if steps < 1:
    #     raise ValueError("Steps must be positive")
    step = (x_max - x_min) / steps

    num_sum = 0
    x = x_min
    # Iterate over the intervals and sum the values at each
    for i in range(steps):
        num_sum += function(x)
        x += step

    return num_sum * step


@numba.jit(nopython=True)
def trapezoid(function: callable, x_min: float, x_max: float, steps: int):
    """Trapezoid integration"""
    # if x_max < x_min:
    #     raise ValueError("Invalid range {} - {}".format(x_min, x_max))
    # if steps < 1:
    #     raise ValueError("Steps must be positive")
    step = (x_max - x_min) / steps

    num_sum = 0
    x = x_min
    for i in range(steps):
        num_sum += function(x) + function(x + step)
        x += step

    # Multiplication is faster than division :P
    return 0.5 * num_sum * step


@numba.jit(nopython=True)
def simpson(func: callable, x_min: float, x_max: float, steps: int):
    """Simpson integration

    Note that the parameters differ from the exercise description (as allowed), since
    this syntax doesn't require O(n) memory for storing the values
    """
    # if x_max < x_min:
    #     raise ValueError("Invalid range {} - {}".format(x_min, x_max))
    # if steps < 1:
    #     raise ValueError("Steps must be positive")
    step = (x_max - x_min) / steps

    # If there is an odd number of intervals
    if steps % 2 != 0:
        steps_even = steps - 1
        odd_steps = True
    else:
        steps_even = steps
        odd_steps = False

    num_sum = 0
    x = x_min
    # Compute the even interval
    for i in range(steps_even//2):
        num_sum += func(x) + 4*func(x + step) + func(x + 2 * step)
        x += 2*step
    num_sum = num_sum * step / 3

    # If the number of intervals is odd, compute the last interval separately
    if odd_steps:
        num_sum += step / 12 * (-func(x_max - 2 * step) + 8*func(x_max - step) + 5*func(x_max))

    return num_sum


@numba.jit(nopython=True, parallel=True)
def monte_carlo_integration(fun: callable, x_min: float, x_max: float, blocks: int, iters: int) -> tuple:
    """Monte Carlo integration

    Iterates over "blocks * iters" points

    :param fun: function to be integrated
    :param x_min: integration area lower limit
    :param x_max: integration area upper limit
    :param blocks: how many blocks to calculate the error estimate for
    :param iters: amount of iterations in each block
    :return: integral, error estimate
    """
    # Create an array for the mean values in each block
    block_values = np.zeros(shape=(blocks,))

    # Integration area
    l = x_max - x_min

    # Iterate over blocks*iters
    for block in range(blocks):
        for i in range(iters):
            # Choose a random x from the integration area
            x = x_min + np.random.rand() * l
            # Add the function value at the point to the block sum
            block_values[block] += fun(x)
        # Set the block value as the mean of the computed values
        block_values[block] /= iters

    # Integral is the mean value of the function times the integration area
    i = l * np.mean(block_values)

    # Compute the error estimate sigma_{I_N}
    di = l * np.std(block_values) / np.sqrt(blocks)

    return i, di
