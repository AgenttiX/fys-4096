# import numba


# @numba.jit(nopython=True)
def eval_derivative(function: callable, x: float, dx: float):
    """Evaluate derivative according to the equation 6 of the lecture slides"""
    return (function(x + dx) - function(x - dx)) / (2*dx)
