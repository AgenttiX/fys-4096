import unittest
import numpy as np
from problem_5 import num_calculus


class CalculusTest(unittest.TestCase):
    def test_first(self):
        self.assertAlmostEqual(1, num_calculus.eval_derivative(np.sin, 0, 0.001), delta=0.001)
        self.assertAlmostEqual(0, num_calculus.eval_derivative(np.sin, np.pi / 2, 0.001), delta=0.001)

        def fun(x):
            return 3 * (x ** 2)

        self.assertAlmostEqual(0, num_calculus.eval_derivative(fun, 0, 0.001), delta=0.001)
        self.assertAlmostEqual(6, num_calculus.eval_derivative(fun, 1, 0.001), delta=0.001)
