"""Configuration for Problem 5 packaging

(venv) mika@agx-ud7-kubuntu:~/Git/fys-4096/exercise_1/problem_5$ python setup.py test
running test
running egg_info
creating problem_5.egg-info
writing problem_5.egg-info/PKG-INFO
writing dependency_links to problem_5.egg-info/dependency_links.txt
writing top-level names to problem_5.egg-info/top_level.txt
writing manifest file 'problem_5.egg-info/SOURCES.txt'
reading manifest file 'problem_5.egg-info/SOURCES.txt'
writing manifest file 'problem_5.egg-info/SOURCES.txt'
running build_ext
test_first (problem_5.tests.test_eval_derivative.CalculusTest) ... ok

----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
"""


from setuptools import setup

setup(
    name="problem_5",
    version="0.1",
    description="ASDF",
    author="Mika Mäki")
