"""Unit tests for the functions of num_calculus.py"""

import unittest

import numba
import numpy as np
from exercise_1 import num_calculus


@numba.jit(nopython=True)
def sin(x):
    """A simple but non-trivial test function

    This function is specifically JIT compiled, since
    apparently nopython-compiled functions cannot take Numpy ufuncs as arguments
    """
    return np.sin(x)


@numba.jit(nopython=True)
def flat(*args):
    """A very simple testing function

    Note that this is deterministic for Monte Carlo, and we can therefore use
    unittest.TestCase.assertEqual() for it as well
    """
    return 1


@numba.jit(nopython=True)
def fun(x):
    """A polynomial testing function"""
    return 3 * (x ** 2)


class CalculusTest(unittest.TestCase):
    def test_first(self):
        self.assertAlmostEqual(1, num_calculus.eval_derivative(sin, 0, 0.001), delta=0.001)
        self.assertAlmostEqual(0, num_calculus.eval_derivative(sin, np.pi/2, 0.001), delta=0.001)

        self.assertAlmostEqual(0, num_calculus.eval_derivative(fun, 0, 0.001), delta=0.001)
        self.assertAlmostEqual(6, num_calculus.eval_derivative(fun, 1, 0.001), delta=0.001)

    def test_second(self):
        self.assertAlmostEqual(0, num_calculus.eval_2nd_derivative(sin, 0, 0.001), delta=0.001)
        self.assertAlmostEqual(-1, num_calculus.eval_2nd_derivative(sin, np.pi/2, 0.001), delta=0.001)

    def test_riemann(self):
        self.assertEqual(1, num_calculus.riemann(flat, x_min=0, x_max=1, steps=1))
        self.assertEqual(1, num_calculus.riemann(flat, x_min=0, x_max=1, steps=100))

        self.assertAlmostEqual(0, num_calculus.riemann(sin, x_min=0, x_max=2*np.pi, steps=100))

    def test_trapezoid(self):
        self.assertEqual(1, num_calculus.trapezoid(flat, x_min=0, x_max=1, steps=1))
        self.assertEqual(1, num_calculus.trapezoid(flat, x_min=0, x_max=1, steps=100))

        self.assertAlmostEqual(0, num_calculus.trapezoid(sin, x_min=0, x_max=2 * np.pi, steps=100))

    def test_simpson(self):
        self.assertEqual(1, num_calculus.simpson(flat, x_min=0, x_max=1, steps=1))
        self.assertEqual(1, num_calculus.simpson(flat, x_min=0, x_max=1, steps=100))
        self.assertEqual(1, num_calculus.simpson(flat, x_min=0, x_max=1, steps=101))

        self.assertAlmostEqual(0, num_calculus.simpson(sin, x_min=0, x_max=2*np.pi, steps=100))

    def test_monte_carlo(self):
        """Testing for the Monte Carlo numerical integration"""

        # The simplest test case
        i, di = num_calculus.monte_carlo_integration(flat, x_min=0, x_max=1, blocks=100, iters=100)
        self.assertEqual(1, i)

        # A bit more complex test case: sin() from 0 to 2*pi
        i, di = num_calculus.monte_carlo_integration(sin, x_min=0, x_max=2*np.pi, blocks=100, iters=1000)
        self.assertAlmostEqual(0, i, delta=0.1)

        # sin() from 0 to pi
        i, di = num_calculus.monte_carlo_integration(sin, x_min=0, x_max=np.pi, blocks=100, iters=1000)
        self.assertAlmostEqual(2, i, delta=0.1)

        i, di = num_calculus.monte_carlo_integration(fun, x_min=0, x_max=2, blocks=100, iters=1000)
        self.assertAlmostEqual(8, i, delta=0.1)
