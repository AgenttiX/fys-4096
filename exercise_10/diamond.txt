[student058@taito-login3 vmc]$ qmca -e 5 -q ev *scalar.dat
                            LocalEnergy               Variance           ratio
vmc  series 0  -87.719340 +/- 0.124549   22.428914 +/- 2.448097   0.2557
[student058@taito-login3 vmc]$


[student058@taito-login3 optJ2]$ qmca -e 5 -q ev *scalar.dat

                            LocalEnergy               Variance           ratio
opt  series 0  -89.833909 +/- 0.017057   2.738394 +/- 0.051151   0.0305
opt  series 1  -90.245895 +/- 0.012906   1.798149 +/- 0.032933   0.0199
opt  series 2  -90.266930 +/- 0.011346   1.914254 +/- 0.050054   0.0212
opt  series 3  -90.276787 +/- 0.011669   1.911247 +/- 0.048781   0.0212
opt  series 4  -90.281318 +/- 0.013253   1.925428 +/- 0.038740   0.0213
[student058@taito-login3 optJ2]$


[student058@taito-login3 production]$ qmca -e 5 -q ev *scalar.dat
/appl/opt/python/2.7.6-gcc482-shared/lib/python2.7/site-packages/numpy/lib/npyio.py:816: UserWarning: loadtxt: Empty input file: "qmc.s002.scalar.dat"
  warnings.warn('loadtxt: Empty input file: "%s"' % fname)
problem encountered for qmc.s002.scalar.dat, results unavailable

                            LocalEnergy               Variance           ratio
qmc  series 0  -90.269581 +/- 0.019407   1.869963 +/- 0.068778   0.0207
qmc  series 1  -90.475373 +/- 0.006762   1.975124 +/- 0.012351   0.0218
[student058@taito-login3 production]$


It can be seen that the energy becomes lower in each phase of the workflow, as expected, since it converges towards
the exact value from above.
The error at DMC might be caused by a forced exit of the simulation due to the limited CPU time.
