"""
Problem 2: Periodic free particle density matrix

I'm not sure about how this should look like, as the concept of density matrix isn't that clear to me, and the
concept of a state R is also a bit fuzzy.
However, here is an attempt.

Please see periodic.jpg for the derivations of the equations used here.


When would the Gaussian be adequate for periodic simulations? Why?
- Gaussian would be sufficient at least when the wavefunction doesn't extend significantly outside the box. That is,
    when the box is large enough.

What would be needed for obtaining the periodic free particle density matrix in a three dimensional cubic box?
- the n in the program should be a matrix (=two-dimensional) with each row containing the quantum numbers of each state
- therefore I suppose that the method 1 might work as it is (depending on how np.dot() operates on matrices) and
    method 2 might require a for loop to extract the R, Rp and n vectors for each state (or some Numpy indexing tricks)
    might work as well)
"""

import typing as tp

import matplotlib.figure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def density_box1(L: float, d: int, beta: float, lam: float,
                 n: np.ndarray, R: tp.Union[float, np.ndarray], Rp: tp.Union[float, np.ndarray]):
    """ Density matrix of a particle in a cubic box

    Using the eigenfunctions and eigenenergies of a free particle
    """
    if n.ndim == 1:
        n_scalar = n
        n_R_mult = n * (R - Rp)
    else:
        n_scalar = np.sqrt(np.sum(n**2, axis=-1))
        n_R_mult = np.dot(n, (R - Rp))

    return L**(-d) * np.sum(np.exp(-beta*lam*(2*np.pi*n_scalar/L)**2) * np.exp(-1j*2*np.pi/L * n_R_mult))


def density_box2(lam: float, tau: float, d: int, N: int,
                 R: tp.Union[float, np.ndarray], Rp: tp.Union[float, np.ndarray], n: np.ndarray, L: float):
    """ Density matrix of a particle in a cubic box

    Summing over periodic images of the Gaussian density matrix
    """
    return (4*np.pi*lam*tau)**(-d*N/2) * np.sum(np.exp(-(R + n*L - Rp)**2 / (4*lam*tau)))


def fix_3d_plot(plot):
    """ Workaround for Matplotlib bugs """
    plot._facecolors2d = plot._facecolors3d
    plot._edgecolors2d = plot._edgecolors3d


def main():
    L = 2
    d = 1
    lam = 0.5
    beta = 1
    N = 1

    n_max = 10
    n = np.arange(0, n_max+1)
    tau = np.linspace(0.1, 10, 100)

    # Due to the lack of better knowledge on the state configuration let's presume these to be the following
    R = 0
    Rp = 0

    # d1 = density_box1(L, d, beta, lam, n, R, Rp)
    # d2 = density_box2(lam, tau, d, N, R, Rp, n, L)

    # n_grid, tau_grid = np.meshgrid(n, tau)
    tau_grid, n_grid = np.meshgrid(tau, n)

    z1 = np.zeros((n.size, tau.size), dtype=np.complex128)
    z2 = np.zeros((n.size, tau.size))

    for i in range(n.size):
        for j in range(tau.size):
            z1[i, j] = density_box1(L, d, beta, lam, n[:i+1], R, Rp)
            z2[i, j] = density_box2(lam, tau[j], d, N, R, Rp, n[:i+1], L)

    fig: matplotlib.figure.Figure = plt.figure()
    fig.suptitle("Density matrix")
    ax1 = fig.add_subplot(111, projection="3d")
    surf1 = ax1.plot_surface(n_grid, tau_grid, np.real(z1), label="using eigen")
    surf2 = ax1.plot_surface(n_grid, tau_grid, z2, label="summing periodic images")
    ax1.set_xlabel("n")
    ax1.set_ylabel(r"$\tau$")
    ax1.set_zlabel(r"$\rho$")

    fix_3d_plot(surf1)
    fix_3d_plot(surf2)
    ax1.legend()

    plt.show()


if __name__ == "__main__":
    main()
