""" Path integral Monte Carlo

To run only some simulations, please comment lines in "if __name__ == ..."

a) The temperatures should be printed for each simulation
b) I've added some comments, although some details of PIMC are still a bit fuzzy to me
c) Running the code creates the necessary figures. Since the time-step simulation takes a few minutes, those
    figures are also in the folder "figures"
d) Running the code creates the figures. However, there may be some bug, as the energy
    for the uniform distribution is significantly higher than for the normal distribution.
"""

import ctypes
import time
import typing as tp

import numba
# from numpy import *
import numpy as np
# from matplotlib.pyplot import *
import matplotlib.cm
import matplotlib.figure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# from scipy.special import erf

from exercise_10 import cython_jitter


@numba.jitclass([
    ("Ne", numba.int64),
    ("Re", numba.float64[:, :]),
    ("spins", numba.int64[:]),
    ("Nn", numba.int64),
    ("Rn", numba.float64[:, :]),
    ("Zn", numba.float64[:]),
    ("tau", numba.float64),
    ("sys_dim", numba.int64),
    ("dim", numba.int64)
])
class Walker:
    def __init__(
            self,
            Ne: int,
            Re: np.ndarray,
            spins: np.ndarray,
            Nn: int,
            Rn: np.ndarray,
            Zn: np.ndarray,
            tau: float,
            dim: int):
        self.Ne = Ne
        self.Re = Re
        self.spins = spins
        self.Nn = Nn
        self.Rn = Rn
        self.Zn = Zn
        self.tau = tau
        self.sys_dim = dim

    def w_copy(self) -> "Walker":
        return Walker(
            self.Ne,
            self.Re.copy(),
            self.spins.copy(),
            self.Nn,
            self.Rn.copy(),
            self.Zn,    # Todo: should there be a copy() here?
            self.tau,
            self.sys_dim
        )


@numba.njit
def kinetic_action(r1: np.ndarray, r2: np.ndarray, tau: float, lambda1: float):
    """ The latter part of equation 15 """
    return np.sum((r1 - r2) ** 2) / (lambda1 * tau * 4)


# If the whole pimc isn't jitted, decorating this makes things slower,
# most likely due to reflecting the list of walkers
@numba.njit
def potential_action(walkers: tp.List[Walker], time_slice1: int, time_slice2: int, tau: float):
    return 0.5 * tau * (potential(walkers[time_slice1]) + potential(walkers[time_slice2]))


@numba.njit
def bisection(r0: float, r1: float, r2: float, sys_dim: int, sigma: float, sigma2: float):
    """ Bisection sampling using normal distribution"""
    # bisection sampling
    r02_ave = (r0 + r2) / 2
    # NOPE Equation 15 (the former part substracts away)
    # Ilkka's PhD thesis, equation 2.23
    log_S_Rp_R = -np.sum((r1 - r02_ave) ** 2) / (2 * sigma2)
    # Rp is moved from r02_ave by a Gaussian random value
    Rp = r02_ave + np.random.randn(sys_dim) * sigma
    # Ilkka's PhD thesis, equation 2.22
    log_S_R_Rp = -np.sum((Rp - r02_ave) ** 2) / (2 * sigma2)

    return Rp, log_S_R_Rp, log_S_Rp_R


@numba.njit
def uniform(r0: float, r1: float, r2: float, sys_dim: int, sigma: float, sigma2: float):
    """ Bisection sampling using uniform distribution """
    r02_ave = (r0 + r2) / 2

    # Limit of the uniform distribution
    # Calculated starting from
    # https://en.wikipedia.org/wiki/Uniform_distribution_(continuous)#Moments
    lim = np.sqrt(3) * sigma
    # print((np.random.rand(sys_dim) - 0.5) * 2)
    uniform_val = (np.random.rand(sys_dim) - 0.5) * 2 * lim

    Rp = r02_ave + uniform_val

    # For uniform distribution all moves are equally probable
    # (as long as we're within the distribution)
    log_S_R_Rp = 1
    log_S_Rp_R = 1

    return Rp, log_S_R_Rp, log_S_Rp_R


@numba.njit
def pimc(n_blocks: int, n_iters: int, walkers: tp.List[Walker], movement: callable = bisection):
    M = len(walkers)
    Ne = walkers[0].Ne * 1
    sys_dim = 1 * walkers[0].sys_dim
    tau = 1.0 * walkers[0].tau
    lambda1 = 0.5
    Eb = np.zeros((n_blocks,))
    Accept = np.zeros((n_blocks,))
    AccCount = np.zeros((n_blocks,))

    # This particular selection for sigma causes exp(kinetic) = 1
    sigma2 = lambda1 * tau
    sigma = np.sqrt(sigma2)

    obs_interval = 5
    for i in range(n_blocks):
        EbCount = 0
        for j in range(n_iters):
            # Choose a random time (=walker) and its two followers
            time_slice0 = int(np.random.rand() * M)
            time_slice1 = (time_slice0 + 1) % M
            time_slice2 = (time_slice1 + 1) % M
            # Random electron
            ptcl_index = int(np.random.rand() * Ne)

            # The position of the chosen electron at the chosen times
            r0 = walkers[time_slice0].Re[ptcl_index]
            r1 = 1.0 * walkers[time_slice1].Re[ptcl_index]
            r2 = walkers[time_slice2].Re[ptcl_index]

            # The actions at the points above
            kinetic_action_old = kinetic_action(r0, r1, tau, lambda1) + \
                                 kinetic_action(r1, r2, tau, lambda1)
            potential_action_old = potential_action(walkers, time_slice0, time_slice1, tau) + \
                                   potential_action(walkers, time_slice1, time_slice2, tau)

            Rp, log_S_R_Rp, log_S_Rp_R = movement(r0, r1, r2, sys_dim, sigma, sigma2)

            # Walkers list is changed here
            walkers[time_slice1].Re[ptcl_index] = 1.0 * Rp

            # Now that the particle has been moved, calculate the actions again
            kinetic_action_new = kinetic_action(r0, Rp, tau, lambda1) + \
                                 kinetic_action(Rp, r2, tau, lambda1)
            potential_action_new = potential_action(walkers, time_slice0, time_slice1, tau) + \
                                   potential_action(walkers, time_slice1, time_slice2, tau)

            deltaK = kinetic_action_new - kinetic_action_old
            deltaU = potential_action_new - potential_action_old

            # print('delta K', deltaK)
            # print('delta logS', log_S_R_Rp-log_S_Rp_R)
            # print('exp(dS-dK)', np.exp(log_S_Rp_R-log_S_R_Rp-deltaK))
            # print('deltaU', deltaU)

            # Ilkka's PhD thesis, last equation of page 18
            # (the T/T part is in exponential form)
            q_R_Rp = np.exp(log_S_Rp_R - log_S_R_Rp - deltaK - deltaU)
            A_RtoRp = min(1.0, q_R_Rp)
            if A_RtoRp > np.random.rand():
                Accept[i] += 1.0
            else:
                walkers[time_slice1].Re[ptcl_index] = 1.0 * r1

            AccCount[i] += 1

            if j % obs_interval == 0:
                E_kin, E_pot = Energy(walkers)
                # print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1
            # exit()

        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        # print('Block {0}/{1}'.format(i + 1, n_blocks))
        # print('    E   = {0:.5f}'.format(Eb[i]))
        # print('    Acc = {0:.5f}'.format(Accept[i]))
        if i % 10 == 0:
            print("Block", i+1, "/", n_blocks)
            print("    E   =", Eb[i])
            print("    Acc =", Accept[i])

    return walkers, Eb, Accept


@numba.njit
def Energy(walkers: tp.List[Walker]) -> tp.Tuple[float, float]:
    """ Calculate the average energy of the walkers """
    M = len(walkers)
    d = 1.0 * walkers[0].sys_dim
    tau = walkers[0].tau
    lambda1 = 0.5
    U = 0.0
    K = 0.0
    # For each walker
    for i in range(M):
        U += potential(walkers[i])
        # For each electron
        for j in range(walkers[i].Ne):
            if i < M - 1:
                K += d / (2 * tau) - np.sum((walkers[i].Re[j] - walkers[i + 1].Re[j]) ** 2) / 4 / lambda1 / tau ** 2
            # Handle the last walker separately
            else:
                K += d / (2 * tau) - np.sum((walkers[i].Re[j] - walkers[0].Re[j]) ** 2) / 4 / lambda1 / tau ** 2
    return K / M, U / M


erf = cython_jitter.njit(
    module="scipy.special.cython_special",
    func="__pyx_fuse_1erf",
    functype=(ctypes.c_double, ctypes.c_double),
    # numba_args={
    #     "cache": True
    # }
)


@numba.njit
def potential(walker: Walker) -> float:
    """ Calculate the potential of a walker

    Is a sum of electron-electron, electron-ion and ion-ion potentials
    """
    V = 0.0
    r_cut = 1.0e-12
    # e-e
    for i in range(walker.Ne - 1):
        for j in range(i + 1, walker.Ne):
            r = np.sqrt(np.sum((walker.Re[i] - walker.Re[j]) ** 2))
            V += 1.0 / max(r_cut, r)

    # The following were added according to the second part of c)

    sigma_smooth = 0.1
    # e-Ion
    for i in range(walker.Ne):
        for j in range(walker.Nn):
            r = np.sqrt(np.sum((walker.Re[i] - walker.Rn[j]) ** 2))
            # V -= walker.Zn[j] / max(r_cut, r)
            V -= walker.Zn[j] * erf(r / np.sqrt(2*sigma_smooth)) / r

    # Ion-Ion
    for i in range(walker.Nn - 1):
        for j in range(i + 1, walker.Nn):
            r = np.sqrt(np.sum((walker.Rn[i] - walker.Rn[j]) ** 2))
            V += 1.0 / max(r_cut, r)

    Vext = external_potential(walker)

    return V + Vext


@numba.njit
def external_potential(walker: Walker) -> float:
    V = 0.0
    for i in range(walker.Ne):
        V += 0.5 * np.sum(walker.Re[i] ** 2)

    return V


def temp_kelvin(walkers: int, tau: float) -> float:
    kb = 3.116801e-6    # Hartree / Kelvin
    return 1 / (kb * walkers * tau)


def show_results(walkers: tp.List[Walker], Eb: np.ndarray, Acc: np.ndarray, title: str):
    plt.figure()

    plt.plot(Eb)
    plt.xlabel("block")
    plt.ylabel("E")
    plt.title(title)

    Eb_cut = Eb[20:]
    print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(np.mean(Eb_cut), np.std(Eb_cut) / np.sqrt(len(Eb_cut))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(np.std(Eb_cut) / np.mean(Eb_cut))))
    temp = temp_kelvin(len(walkers), walkers[0].tau)
    print("Temperature: {0:.5f} K".format(temp))
    plt.show()


@numba.njit
def walker_list(walker: Walker, count: int) -> tp.List[Walker]:
    walkers = []
    for i in range(count):
        walkers.append(walker.w_copy())
    return walkers


def hydrogen():
    """ H2 molecule """
    print("Hydrogen")

    walker = Walker(
        Ne=2,
        Re=np.array([[0.5, 0, 0], [-0.5, 0, 0]]),
        spins=np.array([0, 1]),
        Nn=2,
        Rn=np.array([[-0.7, 0, 0], [0.7, 0, 0]]),
        Zn=np.array([1.0, 1.0]),
        tau=0.1,
        dim=3
    )
    walker_count = 100
    walkers = walker_list(walker, walker_count)

    n_blocks = 200
    n_iters = 100

    start_time = time.perf_counter()
    walkers, Eb, Acc = pimc(n_blocks, n_iters, walkers)
    print("Simulation took {:.3f} s".format(time.perf_counter() - start_time))

    show_results(walkers, Eb, Acc, "Hydrogen")


def quantum_dot():
    """ 2D quantum dot """
    print("Quantum dot")

    walker = Walker(
        Ne=2,
        Re=np.array([[0.5, 0], [-0.5, 0]]),
        spins=np.array([0, 1]),
        # Nn=2,  # not used
        # Rn=np.array([[-0.7, 0], [0.7, 0]]),  # not used
        # Zn=np.array([1.0, 1.0]),  # not used
        Nn=0,
        Rn=np.array([[]]),
        Zn=np.array([]),
        tau=0.25,
        dim=2
    )
    walker_count = 100
    walkers = walker_list(walker, walker_count)

    n_blocks = 200
    n_iters = 100

    start_time = time.perf_counter()
    walkers, Eb, Acc = pimc(n_blocks, n_iters, walkers)
    print("Simulation took {:.3f} s".format(time.perf_counter() - start_time))

    show_results(walkers, Eb, Acc, "2D quantum dot")


@numba.njit(parallel=True)
def run_pimc_multi(walker: Walker, n_blocks: int, n_iters: int, temp_constant: float, taus: np.ndarray):
    walker_counts = np.zeros(taus.shape)
    energies = np.zeros((taus.size, n_blocks))
    accs = np.zeros_like(energies)

    for i in numba.prange(taus.size):
        walker_counts[i], energies[i, :], accs[i, :] = run_pimc(walker, temp_constant, taus[i], n_blocks, n_iters)

    return walker_counts, energies, accs


@numba.njit
def run_pimc(walker: Walker, temp_constant, tau, n_blocks, n_iters):
    iter_walker = walker.w_copy()
    iter_walker.tau = tau
    walker_count = int(temp_constant / tau)

    walkers = walker_list(walker, walker_count)
    walkers_out, energy, acc = pimc(n_blocks, n_iters, walkers)

    return walker_count, energy, acc


def quantum_dot_timestep():
    """ 2D quantum dot time-step extrapolation """
    print("Timestep")

    walker = Walker(
        Ne=2,
        Re=np.array([[0.5, 0], [-0.5, 0]]),
        spins=np.array([0, 1]),
        # Nn=2,  # not used
        # Rn=np.array([[-0.7, 0], [0.7, 0]]),  # not used
        # Zn=np.array([1.0, 1.0]),  # not used
        Nn=0,
        Rn=np.array([[]]),
        Zn=np.array([]),
        tau=0.25,
        dim=2
    )
    # Temperature is dependent on tau * walker_count, so it needs to stay constant
    temperature_constant = walker.tau * 100

    n_blocks = 500
    n_iters = 500
    # walker_counts = []
    # walkers = []
    # energies = []
    # energy_stds = []
    # energy_vecs = []
    # accs = []
    taus = np.array([
        0.02, 0.05, 0.1, 0.2, 0.25,
        0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1,
        1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2
    ])

    # for tau in taus:
    #     walker.tau = tau
    #     count_float = temperature_constant / tau
    #     count = int(count_float)
    #     print("Walkers:", count)
    #     walker_counts.append(count)
    #
    #     iter_start_walkers = walker_list(walker, count)
    #     iter_walkers, iter_energy, iter_acc = pimc(n_blocks, n_iters, iter_start_walkers)
    #     walkers.append(iter_walkers)
    #     energy_vecs.append(iter_energy)
    #
    #     iter_energy_cut = iter_energy[100:]
    #     energies.append(np.mean(iter_energy_cut))
    #     energy_stds.append(np.std(iter_energy_cut) / np.sqrt(len(iter_energy_cut)))
    #     accs.append(iter_acc)

    temp = temp_kelvin(100, walker.tau)
    print("Temperature: {0:.5f} K".format(temp))

    walker_counts, energies, accs = run_pimc_multi(walker, n_blocks, n_iters, temperature_constant, taus)

    energies_cut = energies[:, 100:]
    conv_energies = np.mean(energies_cut, axis=-1)
    energy_stds = np.std(energies_cut, axis=-1) / np.sqrt(energies_cut.size)

    fig1: matplotlib.figure.Figure = plt.figure()
    fig1.suptitle("Time-step extrapolation")
    ax1_1 = fig1.add_subplot(121)
    ax1_1.errorbar(taus, conv_energies, yerr=energy_stds)
    ax1_1.set_xlabel(r"$\tau$")
    ax1_1.set_ylabel("E")
    ax1_1.set_title("Energy by tau")

    ax1_2 = fig1.add_subplot(122)
    ax1_2.plot(taus, walker_counts)
    ax1_2.set_xlabel(r"$\tau$")
    ax1_2.set_ylabel("walker count")
    ax1_2.set_title("Keeping the temperature constant")

    grid_iters, grid_taus = np.meshgrid(list(range(n_blocks)), taus)

    fig2: matplotlib.figure.Figure = plt.figure()
    fig2.suptitle("Time-step extrapolation")
    ax2_1 = fig2.add_subplot(121, projection="3d")
    ax2_1.plot_surface(grid_iters, grid_taus, energies, cmap=matplotlib.cm.get_cmap("plasma"))
    ax2_1.set_xlabel("block")
    ax2_1.set_ylabel(r"$\tau$")
    ax2_1.set_zlabel("E")
    ax2_1.set_title("Energy")

    ax2_2 = fig2.add_subplot(122, projection="3d")
    ax2_2.plot_surface(grid_iters, grid_taus, accs, cmap=matplotlib.cm.get_cmap("plasma"))
    ax2_2.set_title("Acceptance")
    ax2_2.set_xlabel("block")
    ax2_2.set_ylabel(r"$\tau$")
    ax2_2.set_zlabel("Acceptance")

    plt.show()


def quantum_dot_movement():
    """ 2D quantum dot """
    print("Movement")

    walker = Walker(
        Ne=2,
        Re=np.array([[0.5, 0], [-0.5, 0]]),
        spins=np.array([0, 1]),
        # Nn=2,  # not used
        # Rn=np.array([[-0.7, 0], [0.7, 0]]),  # not used
        # Zn=np.array([1.0, 1.0]),  # not used
        Nn=0,
        Rn=np.array([[]]),
        Zn=np.array([]),
        tau=0.25,
        dim=2
    )
    walker_count = 100
    walkers = walker_list(walker, walker_count)

    temp = temp_kelvin(walker_count, walker.tau)
    print("Temperature: {0:.5f} K".format(temp))

    n_blocks = 400
    n_iters = 200

    start_time = time.perf_counter()
    walkers, Eb, Acc = pimc(n_blocks, n_iters, walkers)
    print("Simulation took {:.3f} s".format(time.perf_counter() - start_time))

    start_time = time.perf_counter()
    walkers_uni, Eb_uni, Acc_uni = pimc(n_blocks, n_iters, walkers, uniform)
    print("Simulation took {:.3f} s".format(time.perf_counter() - start_time))

    fig: matplotlib.figure.Figure = plt.figure()
    fig.suptitle("Bisection movement")

    ax1 = fig.add_subplot(121)
    ax1.plot(Eb, label="Normal distribution")
    ax1.plot(Eb_uni, label="Uniform distribution")
    ax1.set_xlabel("block")
    ax1.set_ylabel("E")
    ax1.set_title("Energy")
    ax1.legend()

    ax2 = fig.add_subplot(122)
    ax2.plot(Acc, label="Normal distribution")
    ax2.plot(Acc_uni, label="Uniform distribution")
    ax2.set_xlabel("block")
    ax2.set_ylabel("Acceptance")
    ax2.set_title("Acceptance")
    ax2.legend()

    plt.show()


if __name__ == "__main__":
    quantum_dot()
    hydrogen()
    quantum_dot_timestep()
    quantum_dot_movement()
