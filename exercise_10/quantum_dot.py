""" Problem 3: PIMC for 2D many-electron semiconductor quantum dot

I ran the simulations on my desktop instead of CSC since it appears to work fine
"""

import os.path
import typing as tp

import matplotlib.cm
import matplotlib.figure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas.io.parsers


def read_gnuplot(path: str) -> tp.Tuple[np.ndarray, np.ndarray, np.ndarray]:
    arr = pandas.io.parsers.read_csv(
        path,
        sep=" ",
        skipinitialspace=True,
        index_col=False,
        names=["x", "y", "z"],
        dtype={"x": np.float_, "y": np.float_, "z": np.float_},
        engine="c"
    ).values

    empty_rows = np.isnan(arr[:, 0])
    y_len = np.argmax(empty_rows)
    arr_no_nan = arr[np.logical_not(empty_rows), :]
    x_len = arr_no_nan.shape[0] // y_len

    x_vec = arr_no_nan[::x_len, 0]
    y_vec = arr_no_nan[:y_len, 1]

    # Todo: might require transposing etc.
    # (I'm not sure whether the axes should be swapped.)
    z = np.reshape(arr_no_nan[:, 2], (x_len, y_len))

    return x_vec, y_vec, z


def density():
    # me = 0.067
    # eps = 12.4
    # h_om = 11.857   # meV

    curr_path = os.path.dirname(os.path.abspath(__file__))
    s0_density_path = os.path.join(curr_path, "rpimc_2d_dot/state_S0/ag_density.dat")
    s3_density_path = os.path.join(curr_path, "rpimc_2d_dot/state_S3/ag_density.dat")

    x_vec_s0, y_vec_s0, density_s0 = read_gnuplot(s0_density_path)
    x_vec_s3, y_vec_s3, density_s3 = read_gnuplot(s3_density_path)

    y_grid_s0, x_grid_s0 = np.meshgrid(y_vec_s0, x_vec_s0)
    y_grid_s3, x_grid_s3 = np.meshgrid(y_vec_s3, x_vec_s3)

    fig1: matplotlib.figure.Figure = plt.figure()
    fig1.suptitle("PIMC for 2D many-electron semiconductor quantum dot")
    ax1_1 = fig1.add_subplot(121, projection="3d")
    ax1_1.plot_surface(x_grid_s0, y_grid_s0, density_s0, cmap=matplotlib.cm.get_cmap("plasma"))
    ax1_1.set_xlabel("x")
    ax1_1.set_ylabel("y")
    ax1_1.set_zlabel("density")
    ax1_1.set_title("S0 state")

    ax1_2 = fig1.add_subplot(122, projection="3d")
    ax1_2.plot_surface(x_grid_s3, y_grid_s3, density_s3, cmap=matplotlib.cm.get_cmap("plasma"))
    ax1_2.set_xlabel("x")
    ax1_2.set_ylabel("y")
    ax1_2.set_zlabel("density")
    ax1_2.set_title("S3 state")

    fig2: matplotlib.figure.Figure = plt.figure()
    fig2.suptitle("PIMC for 2D many-electron semiconductor quantum dot")
    ax2_1 = fig2.add_subplot(121)
    ax2_1.imshow(density_s0)
    ax2_1.set_title("S0 state")

    ax2_2 = fig2.add_subplot(122)
    ax2_2.imshow(density_s3)
    ax2_2.set_title("S3 state")

    # plt.show()


def read_vec(path) -> np.ndarray:
    return pandas.io.parsers.read_csv(
        path,
        index_col=False,
        engine="c"
    ).values


def print_energy(e_vec: np.ndarray, name: str):
    e_cut = e_vec[20:]
    print(name)
    # Printing copied from pimc_simple.py
    print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(np.mean(e_cut), np.std(e_cut) / np.sqrt(len(e_cut))))
    print('Variance to energy ratio: {0:.5f}'.format(abs(np.std(e_cut) / np.mean(e_cut))))


def energy():
    curr_path = os.path.dirname(os.path.abspath(__file__))
    s0_energy_path = os.path.join(curr_path, "rpimc_2d_dot/state_S0/E_tot.dat")
    s3_energy_path = os.path.join(curr_path, "rpimc_2d_dot/state_S3/E_tot.dat")

    s0_energy = read_vec(s0_energy_path)
    s3_energy = read_vec(s3_energy_path)

    s0_e_cut = s0_energy[20:]
    s3_e_cut = s3_energy[20:]
    mean_s0 = np.mean(s0_e_cut)
    mean_s3 = np.mean(s3_e_cut)
    err_s0 = np.std(s0_e_cut) / np.sqrt(len(s0_e_cut))
    err_s3 = np.std(s3_e_cut) / np.sqrt(len(s3_e_cut))

    fig1: matplotlib.figure.Figure = plt.figure()
    fig1.suptitle("PIMC for 2D many-electron semiconductor quantum dot")
    ax1_1 = fig1.add_subplot(111)
    ax1_1.plot(s0_energy, label="S0")
    ax1_1.axhline(mean_s0, color="k")
    ax1_1.axhline(mean_s0 + err_s0, color="k", ls=":")
    ax1_1.axhline(mean_s0 - err_s0, color="k", ls=":")

    ax1_1.axhline(mean_s3, color="k")
    ax1_1.axhline(mean_s3 + err_s3, color="k", ls=":")
    ax1_1.axhline(mean_s3 - err_s3, color="k", ls=":")

    ax1_1.plot(s3_energy, label="S3")
    ax1_1.legend()

    print_energy(s0_energy, "S0")
    print_energy(s3_energy, "S3")

    # plt.show()


if __name__ == "__main__":
    density()
    energy()
    plt.show()
