 2d Harmonic dot
  Num of electrons            6
  unpolarized 
  omega (Ha)          4.3573673298878386E-004
  omega (eff Ha)     0.99998328454261798     
  omega (meV)         11.856999999999999     
  T(K)                33.703710883631487     
  T_F(K)              337.03710883631481     
  T/T_F              0.10000000000000001     
  
  time-step         36.598243019036772     
  Trotter number                   256
  Notice: time-step in (E_F)**(-1)    3.9062500000000000E-002
