"""
I've done problem 4 but my solution has bugs, as the energy is too high, and
it doesn't work properly in the quantum case (M > 1).
Perhaps it's caused something with the temperature or the mass of the atoms?

In the quantum case the atoms merely fluctuate around their original locations
without converging to the potential minimum.
"""

import typing as tp

import numba
# from numpy import *
import numpy as np
import matplotlib.figure
# from matplotlib.pyplot import *
import matplotlib.pyplot as plt
# from scipy.special import erf


@numba.jitclass([
    ("n_atoms", numba.int64),
    ("r_atoms", numba.float64[:, :]),
    ("m_atoms", numba.float64[:]),
    ("tau", numba.float64),
    ("sys_dim", numba.int64)
])
class Walker:
    def __init__(
            self,
            n_atoms: int,
            r_atoms: np.ndarray,
            m_atoms: np.ndarray,
            tau: float,
            dim: int):
        self.n_atoms = n_atoms
        self.r_atoms = r_atoms
        self.m_atoms = m_atoms
        # self.spins = spins
        self.tau = tau
        self.sys_dim = dim

    def w_copy(self) -> "Walker":
        return Walker(
            self.n_atoms,
            self.r_atoms.copy(),
            self.m_atoms.copy(),
            # self.spins.copy(),
            self.tau,
            self.sys_dim
        )


@numba.njit
def kinetic_action(r1: np.ndarray, r2: np.ndarray, tau: float, lambda1: float):
    """ The latter part of equation 15 """
    return np.sum((r1 - r2) ** 2) / (lambda1 * tau * 4)


@numba.njit
def potential_action(walkers: tp.List[Walker], time_slice1: int, time_slice2: int, tau: float):
    """ Equation 16 (without the error term) """
    return 0.5 * tau * (potential(walkers[time_slice1])
                        + potential(walkers[time_slice2]))


@numba.njit
def pimc(n_blocks: int, n_iters: int, walkers: tp.List[Walker]):
    M = len(walkers)
    n_atoms = walkers[0].n_atoms * 1
    sys_dim = 1 * walkers[0].sys_dim
    tau = 1.0 * walkers[0].tau

    # lambda1 = 0.5
    # lambda = hbar / (2m)
    # The mass of the atoms is configured here
    lambda1 = 1/(2*walkers[0].m_atoms[0])

    Eb = np.zeros((n_blocks,))
    accept = np.zeros((n_blocks,))
    acc_count = np.zeros((n_blocks,))
    sigma2 = lambda1 * tau
    sigma = np.sqrt(sigma2)

    r_blocks = np.zeros_like(Eb)

    obs_interval = 5
    for i in range(n_blocks):
        EbCount = 0
        for j in range(n_iters):
            time_slice0 = int(np.random.rand() * M)
            time_slice1 = (time_slice0 + 1) % M
            time_slice2 = (time_slice1 + 1) % M
            ptcl_index = int(np.random.rand() * n_atoms)

            r0 = walkers[time_slice0].r_atoms[ptcl_index]
            r1 = 1.0 * walkers[time_slice1].r_atoms[ptcl_index]
            r2 = walkers[time_slice2].r_atoms[ptcl_index]

            KineticActionOld = kinetic_action(r0, r1, tau, lambda1) + \
                               kinetic_action(r1, r2, tau, lambda1)
            PotentialActionOld = potential_action(walkers, time_slice0, time_slice1, tau) + \
                                 potential_action(walkers, time_slice1, time_slice2, tau)

            # bisection sampling
            r02_ave = (r0 + r2) / 2
            log_S_Rp_R = -np.sum((r1 - r02_ave) ** 2) / 2 / sigma2
            Rp = r02_ave + np.random.randn(sys_dim) * sigma
            log_S_R_Rp = -np.sum((Rp - r02_ave) ** 2) / 2 / sigma2

            walkers[time_slice1].r_atoms[ptcl_index] = 1.0 * Rp
            KineticActionNew = kinetic_action(r0, Rp, tau, lambda1) + \
                               kinetic_action(Rp, r2, tau, lambda1)
            PotentialActionNew = potential_action(walkers, time_slice0, time_slice1, tau) + \
                                 potential_action(walkers, time_slice1, time_slice2, tau)

            deltaK = KineticActionNew - KineticActionOld
            deltaU = PotentialActionNew - PotentialActionOld
            # print('delta K', deltaK)
            # print('delta logS', log_S_R_Rp-log_S_Rp_R)
            # print('exp(dS-dK)', exp(log_S_Rp_R-log_S_R_Rp-deltaK))
            # print('deltaU', deltaU)
            q_R_Rp = np.exp(log_S_Rp_R - log_S_R_Rp - deltaK - deltaU)
            A_RtoRp = min(1.0, q_R_Rp)
            if A_RtoRp > np.random.rand():
                accept[i] += 1.0
            else:
                walkers[time_slice1].r_atoms[ptcl_index] = 1.0 * r1
            acc_count[i] += 1
            if j % obs_interval == 0:
                E_kin, E_pot = energy(walkers)
                # print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1

                r_blocks[i] += distance(walkers[time_slice1])
            # exit()

        Eb[i] /= EbCount
        accept[i] /= acc_count[i]
        r_blocks[i] /= EbCount
        # print('Block {0}/{1}'.format(i + 1, n_blocks))
        # print('    E   = {0:.5f}'.format(Eb[i]))
        # print('    Acc = {0:.5f}'.format(accept[i]))
        if i % 10 == 0:
            print("Block", i+1, "/", n_blocks)
            print("    E   =", Eb[i])
            print("    Acc =", accept[i])

    return walkers, Eb, accept, r_blocks


@numba.njit
def distance(walker: Walker) -> float:
    return np.sqrt(np.sum((walker.r_atoms[0, :] - walker.r_atoms[1, :])**2))


@numba.njit
def energy(walkers: tp.List[Walker]) -> tp.Tuple[float, float]:
    M = len(walkers)
    d = 1.0 * walkers[0].sys_dim
    tau = walkers[0].tau

    # lambda1 = 0.5
    # Accounting for the mass of these atoms
    lambda1 = 1 / (2 * walkers[0].m_atoms[0])

    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(walkers[i])
        for j in range(walkers[i].n_atoms):
            if i < M - 1:
                K += d / 2 / tau - np.sum((walkers[i].r_atoms[j] - walkers[i + 1].r_atoms[j]) ** 2) / 4 / lambda1 / tau ** 2
            else:
                K += d / 2 / tau - np.sum((walkers[i].r_atoms[j] - walkers[0].r_atoms[j]) ** 2) / 4 / lambda1 / tau ** 2
    return K / M, U / M


@numba.njit
def potential(walker: Walker) -> float:
    r = distance(walker)
    # return potential_lj(r)
    return potential_morse(r)


@numba.njit
def potential_lj(r: float) -> float:
    """ The Lennard-Jones potential has bugs

    According to Tuomo Ilkka said in the exercise session that this potential should not be used.
    """
    r_cut = 1e-12
    r = max(r_cut, r)

    # sigma_H = 0.281  # nm
    # sigma_H = 0.281e-9 / 5.29e-11   # as a multiple of Bohr radius a0
    # sigma_H = 1
    a0 = 1
    re = 1.40*a0
    # eps_H = 8.6     # eps/kB
    # Conversion to Hartree
    # eps_H = 8.6 * 1.38e-23 / 4.359e-18
    eps_H = 0.1745

    # return 4*eps_H*((sigma_H/r)**12 - (sigma_H/r)**6)
    return eps_H*((re/r)**12 - 2*(re/r)**6)


@numba.njit
def potential_lj2(r: float) -> float:
    r_cut = 1e-12
    r = max(r_cut, r)

    re = 1.4
    # sigma_H = 0.281e-9 / 5.29e-11
    sigma_H = re / 1.122

    # From eps/kB to Hartree (divide by kB, multiply by conversion factor)
    # eps_H = 8.6 * 1.38e-23 / 4.359e-18
    eps_H = 0.1745

    return 4*eps_H*((sigma_H/r)**12 - (sigma_H/r)**6)


@numba.njit
def potential_morse(r: float) -> float:
    De = 0.1745     # Hartree
    a0 = 1          # in atomic units
    re = 1.40*a0    # equilibrium distance
    # a = 1.0282/a0
    a = 1/(1.0282*a0)

    return De * (1 - np.exp(-a*(r-re)))**2 - De


def lennard_morse():
    fig = plt.figure()

    ax3 = fig.add_subplot(111)
    r_vec = np.linspace(1.1, 5, 100)
    V_morse_vec = np.zeros_like(r_vec)
    V_lj_vec = np.zeros_like(r_vec)
    V_lj2_vec = np.zeros_like(r_vec)

    for i in range(r_vec.size):
        V_morse_vec[i] = potential_morse(r_vec[i])
        V_lj_vec[i] = potential_lj(r_vec[i])
        V_lj2_vec[i] = potential_lj2(r_vec[i])

    ax3.plot(r_vec, V_morse_vec, label="Morse")
    ax3.plot(r_vec, V_lj_vec, label="L-J")
    ax3.plot(r_vec, V_lj2_vec, label="L-J (orig.)")
    ax3.legend()
    ax3.set_xlabel("r")
    ax3.set_ylabel("V")
    ax3.set_title("Potentials")

    plt.show()


def main():
    x = 1.2

    M = 1
    T = 300
    # To set a fixed temperature
    tau = 1/(T*M)
    print("Tau:", tau)

    walkers = [
        Walker(
            n_atoms=2,
            r_atoms=np.array([[0.5*x, 0, 0], [-0.5*x, 0, 0]]),
            m_atoms=np.array([1836.0, 1836.0]),
            tau=tau,
            dim=3
        )
    ]

    for i in range(M - 1):
        walkers.append(walkers[i].w_copy())
    n_blocks = 4000
    n_iters = 200

    walkers, Eb, Acc, r_blocks = pimc(n_blocks, n_iters, walkers)
    for walker in walkers:
        dist = np.sqrt(np.sum((walker.r_atoms[0] - walker.r_atoms[1])**2))
        print("pos")
        print(walker.r_atoms)
        print("Dist", dist, "a0,", dist * 5.29e-11, "m")

    fig: matplotlib.figure.Figure = plt.figure()
    fig.suptitle("Hydrogen molecule")

    ax1 = fig.add_subplot(221)
    ax1.plot(Eb)
    ax1.set_xlabel("block")
    ax1.set_ylabel("E")
    ax1.set_title("Energy")

    ax2 = fig.add_subplot(222)
    ax2.plot(r_blocks)
    ax2.set_xlabel("block")
    ax2.set_ylabel("r")
    ax2.set_title("Interatomic distance")

    ax3 = fig.add_subplot(223)
    ax3.scatter(r_blocks, Eb)
    ax3.set_xlabel("r")
    ax3.set_ylabel("E")
    ax3.set_title("Energy by distance")

    min_E_ind = np.argmin(Eb)
    min_E_r = r_blocks[min_E_ind]
    print("Minimum energy found at distance", min_E_r)

    Eb = Eb[20:]
    print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(np.mean(Eb), np.std(Eb) / np.sqrt(len(Eb))))
    print('Variance to energy ratio: {0:.5f}'.format(np.abs(np.std(Eb) / np.mean(Eb))))

    plt.show()


if __name__ == "__main__":
    # lennard_morse()
    main()
