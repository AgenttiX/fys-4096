"""
EXERCISE DESRIPTION
Simple Monte Carlo for Ising model

Related to course FYS-4096 Computational Physics

Problem 1:
- Make the code to work, that is, include code to where it reads "# ADD"
- Comment the parts with "# ADD" and make any additional comments you 
  think could be useful for yourself later.
- Follow the assignment from ex11.pdf.

Problem 2:
- Add observables: heat capacity, magnetization, magnetic susceptibility
- Follow the assignment from ex11.pdf.

Problem 3:
- Look at the temperature effects and locate the phase transition temperature.
- Follow the assignment from ex11.pdf.

ANSWERS
1) The simulation uses a periodic boundary condition.
Exchange constant is presumed to be 4 in site_energy().
For the values of the other parameters, please see problem2().

2) Please run the program to see the output.

3) Transition temperature is about 2.020100502512563 K.
Figures are at observables*.png.
"""

import os
import os.path
import typing as tp

import h5py
import numba
# from numpy import *
import numpy as np
import matplotlib.figure
# from matplotlib.pyplot import *
import matplotlib.pyplot as plt
# from scipy.special import erf
# import pyqtgraph as pg


@numba.jitclass([
    ("spin", numba.float64),
    ("nearest_neighbors", numba.int64[:]),
    ("sys_dim", numba.int64),
    ("coords", numba.int64[:])
])
class Walker:
    def __init__(self, spin: float, nn: np.ndarray, dim: int, coords: np.ndarray):
        self.spin = spin
        self.nearest_neighbors = nn
        self.sys_dim = dim
        self.coords = coords

    def w_copy(self) -> "Walker":
        return Walker(
            self.spin,
            self.nearest_neighbors.copy(),
            self.sys_dim,
            self.coords.copy()
        )


@numba.njit
def total_energy(walkers: tp.List[Walker]) -> float:
    """ Total energy of the system """
    E = 0.0
    # J = 4.0  # given in units of k_B

    # Since we don't have a data structure to store the
    # distances we cannot iterate over all combinations
    # for i in range(len(walkers)):
    #     for j in range(i+1, len(walkers)):
    #         E += -J * walkers[i].spin * walkers[j].spin

    for walker in walkers:
        E += site_energy(walkers, walker)

    # The energy has to be divided by 2 since each "arc" is considered twice
    return E / 2


@numba.njit
def site_energy(walkers: tp.List[Walker], walker: Walker) -> float:
    """ Energy of one walker """
    E = 0.0
    J = 4.0  # given in units of k_B
    for k in range(len(walker.nearest_neighbors)):
        j = walker.nearest_neighbors[k]
        E += -J * walker.spin * walkers[j].spin
    return E


@numba.njit
def ising(n_blocks: int, n_iters: int, walkers: tp.List[Walker], beta: float):
    """ Monte Carlo simulation of the Ising model"""
    M = len(walkers)
    E_blocks = np.zeros((n_blocks,))
    accept = np.zeros((n_blocks,))
    acc_count = np.zeros((n_blocks,))

    E_blocks2 = np.zeros((n_blocks,))
    mag_blocks = np.zeros((n_blocks,))
    mag_blocks2 = np.zeros((n_blocks,))

    # grid_side = int(np.sqrt(len(walkers)))
    # grids = np.zeros((n_blocks*n_iters, grid_side, grid_side))

    obs_interval = 5
    print_interval = 100
    for i in range(n_blocks):
        Eb_count = 0
        for j in range(n_iters):
            site = int(np.random.rand() * M)

            s_old = 1.0 * walkers[site].spin

            E_old = site_energy(walkers, walkers[site])

            # selection of new spin to variable s_new
            # (randomly chosen)
            # flip = np.random.randint(0, 2)
            # if flip:
            #     s_new = -s_old
            s_new = 0.5 * (-1)**np.random.randint(0, 2)

            walkers[site].spin = 1.0 * s_new

            E_new = site_energy(walkers, walkers[site])

            # Metropolis Monte Carlo
            q_s_sp = np.exp(-beta*(E_new - E_old))

            a_s_sp = min(1.0, q_s_sp)
            if a_s_sp > np.random.rand():
                accept[i] += 1.0
            else:
                walkers[site].spin = s_old

            acc_count[i] += 1

            if j % obs_interval == 0:
                E_tot = total_energy(walkers) / M  # energy per spin
                E_blocks[i] += E_tot
                Eb_count += 1

                # Heat capacity & magnetization
                mag = magnetization(walkers)
                mag_blocks[i] += mag
                mag_blocks2[i] += mag**2

                E_blocks2[i] += E_tot**2

            # grids[i, :, :] = spin_grid(walkers)

        E_blocks[i] /= Eb_count
        accept[i] /= acc_count[i]

        E_blocks2[i] /= Eb_count
        mag_blocks[i] /= Eb_count
        mag_blocks2[i] /= Eb_count

        if i % print_interval == 0:
            # print('Block {0}/{1}'.format(i + 1, n_blocks))
            # print('    E   = {0:.5f}'.format(E_blocks[i]))
            # print('    Acc = {0:.5f}'.format(accept[i]))
            print("Block", i+1, "/", n_blocks)
            print("    E   =", E_blocks[i])
            print("    Acc =", accept[i])

    return walkers, accept, E_blocks, E_blocks2, mag_blocks, mag_blocks2


@numba.njit
def spin_grid(walkers: tp.List[Walker]) -> np.ndarray:
    grid_side = int(np.sqrt(len(walkers)))
    grid = np.zeros((grid_side, grid_side))
    for i, walker in enumerate(walkers):
        grid[walker.coords] = walker.spin

    return grid


@numba.njit
def magnetization(walkers: tp.List[Walker]):
    s = 0.0
    for walker in walkers:
        s += walker.spin
    return s


# @numba.njit
def heat_capacity(energy: np.ndarray, energy2: np.ndarray, T: tp.Union[float, np.ndarray], kB: float = 1):
    return (np.mean(energy2, axis=-1) - np.mean(energy, axis=-1)**2) / (kB * T**2)


# @numba.njit
def susceptibility(M: np.ndarray, M2: np.ndarray, T: tp.Union[float, np.ndarray], kB: float = 1):
    return (np.mean(M2, axis=-1) - np.mean(M, axis=-1)**2) / (kB * T)


@numba.njit
def create_grid(dim: int, grid_side: int):
    walkers = []

    # Ising model nearest neighbors only
    mapping = np.zeros((grid_side, grid_side), dtype=np.int_)  # mapping
    inv_map = []  # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i, j] = ii
            inv_map.append([i, j])
            ii += 1

    # Create the walkers and configure the nearest neighbors for each
    for i in range(grid_side):
        for j in range(grid_side):
            # Periodic boundary condition
            j1 = mapping[i, (j - 1) % grid_side]
            j2 = mapping[i, (j + 1) % grid_side]
            i1 = mapping[(i - 1) % grid_side, j]
            i2 = mapping[(i + 1) % grid_side, j]
            walkers.append(
                Walker(
                    0.5,
                    np.array([j1, j2, i1, i2]),
                    dim,
                    np.array([i, j])
                )
            )

    return walkers


@numba.njit
def run_single_copy(n_blocks, n_iters, walkers, beta):
    walkers_iter = []
    for walker in walkers:
        walkers_iter.append(walker.w_copy())

    return ising(n_blocks, n_iters, walkers_iter, beta)


@numba.njit(parallel=True)
def run_multi_temp(walkers: tp.List[Walker], n_blocks: int, n_iters: int, betas: np.ndarray):
    accept = np.zeros((betas.size, n_blocks))
    E_blocks = np.zeros_like(accept)
    E_blocks2 = np.zeros_like(accept)
    mag_blocks = np.zeros_like(accept)
    mag_blocks2 = np.zeros_like(accept)

    for i in numba.prange(betas.size):
        walkers_out, accept[i, :], E_blocks[i, :], E_blocks2[i, :], mag_blocks[i, :], mag_blocks2[i, :] = \
            run_single_copy(n_blocks, n_iters, walkers, betas[i])

    return accept, E_blocks, E_blocks2, mag_blocks, mag_blocks2


def problem3():
    dim = 2
    grid_side = 10
    walkers = create_grid(dim, grid_side)

    n_temps = 200
    temps = np.linspace(0.5, 6, n_temps)
    betas = 1 / temps

    n_blocks = 600
    n_iters = 1000
    eq = 100

    accept, E_blocks, E_blocks2, mag_blocks, mag_blocks2 = run_multi_temp(walkers, n_blocks, n_iters, betas)

    dir = "./problem3"
    if not os.path.isdir(dir):
        os.mkdir(dir)

    for i in range(temps.size):
        save_data(
            os.path.join(dir, "sim_{}.hdf5".format(i)),
            accept[i, :], E_blocks[i, :], E_blocks2[i, :], mag_blocks[i, :], mag_blocks2[i, :],
            eq, temps[i]
        )

    cv = heat_capacity(E_blocks[:, eq:], E_blocks2[:, eq:], temps)
    susc = susceptibility(mag_blocks[:, eq:], mag_blocks2[:, eq:], temps)

    trans_ind = np.argmax(susc)
    trans_temp = temps[trans_ind]
    print("Transition temperature: {} K".format(trans_temp))

    fig1: matplotlib.figure.Figure = plt.figure()
    ax1_1 = fig1.add_subplot(221)
    ax1_1.plot(temps, np.mean(E_blocks[:, eq:], axis=-1),)
    ax1_1.axvline(trans_temp, color="k")
    ax1_1.set_xlabel("T")
    ax1_1.set_ylabel("E")
    ax1_1.set_title("Energy")

    ax1_2 = fig1.add_subplot(222)
    ax1_2.plot(temps, cv)
    ax1_2.axvline(trans_temp, color="k")
    ax1_2.set_xlabel("T")
    ax1_2.set_ylabel("$C_V$")
    ax1_2.set_title("Heat capacity")

    ax1_3 = fig1.add_subplot(223)
    ax1_3.plot(temps, np.mean(mag_blocks[:, eq:], axis=-1))
    ax1_3.axvline(trans_temp, color="k")
    ax1_3.set_xlabel("T")
    ax1_3.set_ylabel("M")
    ax1_3.set_title("Magnetization")

    ax1_4 = fig1.add_subplot(224)
    ax1_4.plot(temps, susc)
    ax1_4.axvline(trans_temp, color="k")
    ax1_4.set_xlabel("T")
    ax1_4.set_ylabel("$\chi$")
    ax1_4.set_title("Susceptibility")

    plt.show()


def problem2():
    dim = 2
    grid_side = 10
    # grid_size = grid_side ** dim

    walkers = create_grid(dim, grid_side)

    n_blocks = 200
    n_iters = 1000
    eq = 20  # equilibration "time"
    T = 3.0     # K
    # k_B = 1.38064852e-23
    beta = 1.0 / T
    # Notice: Energy is measured in units of k_B, which is why
    #         beta = 1/T instead of 1/(k_B T)
    walkers, accept, E_blocks, E_blocks2, mag_blocks, mag_blocks2 = ising(n_blocks, n_iters, walkers, beta)

    fig1: matplotlib.figure.Figure = plt.figure()
    ax1_1 = fig1.add_subplot(121)
    ax1_1.plot(E_blocks)
    ax1_1.set_xlabel("block")
    ax1_1.set_ylabel("E")
    ax1_1.set_title("Energy")

    ax1_2 = fig1.add_subplot(122)
    ax1_2.plot(mag_blocks)
    ax1_2.set_xlabel("block")
    ax1_2.set_ylabel("M")
    ax1_2.set_title("Magnetization")

    Eb_cut = E_blocks[eq:]
    print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(np.mean(Eb_cut), np.std(Eb_cut) / np.sqrt(len(Eb_cut))))
    print('Variance to energy ratio: {0:.5f}'.format(np.abs(np.std(Eb_cut) / np.mean(Eb_cut))))

    cv = heat_capacity(E_blocks[eq:], E_blocks2[eq:], T)
    print("Heat capacity {0:.5f}".format(cv))

    susc = susceptibility(mag_blocks[eq:], mag_blocks2[eq:], T)
    print("Susceptibility {0:.5f}".format(susc))

    save_data("problem2.hdf5", accept, E_blocks, E_blocks2, mag_blocks, mag_blocks2, eq, T)

    plt.show()


def save_data(path: str, accept, E_blocks, E_blocks2, mag_blocks, mag_blocks2, eq: int, T: float):
    cv = heat_capacity(E_blocks[eq:], E_blocks2[eq:], T)
    susc = susceptibility(mag_blocks[eq:], mag_blocks2[eq:], T)

    with h5py.File(path, "w") as f:
        accept_set = f.create_dataset("accept", data=accept, dtype="f")
        E_blocks_set = f.create_dataset("E_blocks", data=E_blocks, dtype="f")
        E_blocks2_set = f.create_dataset("E_blocks2", data=E_blocks2, dtype="f")
        mag_blocks_set = f.create_dataset("mag_blocks", data=mag_blocks, dtype="f")
        mag_blocks2_set = f.create_dataset("mag_blocks2", data=mag_blocks2, dtype="f")

        f.attrs["equilibration_steps"] = eq
        f.attrs["heat_capacity"] = cv
        f.attrs["susceptibility"] = susc
        f.attrs["temperature"] = T


if __name__ == "__main__":
    problem2()
    problem3()
