import typing as tp

import mpmath
# The observables class cannot be properly jitted, and therefore Numba is of little use, as the atom list would have
# to be reflected at each iteration
# import numba
import numpy as np


# Using a very high precision fo attempting to get a non-zero heat capacity
mpmath.dps = 400


# The "optional" types cause trouble when operations aren't defined for None
# @numba.jitclass([
#     ("index", numba.int64),
#     ("mass", numba.float64),
#     ("dt", numba.float64),
#     ("dims", numba.int64),
#     ("LJ_epsilon", numba.optional(numba.float64)),
#     ("LJ_sigma", numba.optional(numba.float64)),
#     ("R", numba.optional(numba.float64[:])),
#     ("v", numba.optional(numba.float64[:])),
#     ("force", numba.optional(numba.float64[:]))
# ])

# @numba.jitclass([
#     ("index", numba.int64),
#     ("mass", numba.float64),
#     ("dt", numba.float64),
#     ("dims", numba.int64),
#     ("LJ_epsilon", numba.float64),
#     ("LJ_sigma", numba.float64),
#     ("R", numba.float64[:]),
#     ("v", numba.float64[:]),
#     ("force", numba.float64[:])
# ])
class Atom:
    """ The state of a single atom in the simulation """
    def __init__(self, index: int, mass: float, dt: float, dims: int):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        # self.LJ_epsilon = None
        # self.LJ_sigma = None
        # self.R = None
        # self.v = None
        # self.force = None
        self.LJ_epsilon = 0
        self.LJ_sigma = 0
        self.R = np.zeros((dims,))
        self.v = np.zeros((dims,))
        self.force = np.zeros((dims,))

    def set_LJ_parameters(self, LJ_epsilon: float, LJ_sigma: float) -> None:
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self, coordinates: np.ndarray) -> None:
        self.R = coordinates

    def set_velocity(self, velocity: np.ndarray) -> None:
        self.v = velocity

    def set_force(self, force) -> None:
        self.force = force


# The lists here are severely problematic
# @numba.jitclass([
#     ("E_kin", numba.types.List(numba.float64)),
#     ("E_pot", numba.types.List(numba.float64)),
#     ("distance", numba.types.List(numba.float64)),
#     ("temperature", numba.types.List(numba.float64))
# ])
class Observables:
    """ A holder for the observables """
    def __init__(self):
        # A bit of trickery for Numba type inference :P
        # self.E_kin = [0.0]
        # self.E_pot = [0.0]
        # self.distance = [0.0]
        # self.temperature = [0.0]
        #
        # self.E_kin.pop()
        # self.E_pot.pop()
        # self.distance.pop()
        # self.temperature.pop()

        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.temperature = []


# @numba.njit
def calculate_energetics(atoms: tp.List[Atom], pair_potential: callable) -> tp.Tuple[float, float]:
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    # ADDED calculation of kinetic and potential energy
    for i in range(N):
        E_kin += 0.5 * atoms[i].mass * np.sum(atoms[i].v**2)
        # Sum of pair potentials
        for j in range(i+1, N):
            V += pair_potential(atoms[i], atoms[j])

        # Madelung sum is not necessary in this case

    return E_kin, V


# @numba.njit
def calculate_force(atoms: tp.List[Atom], pair_force: callable) -> np.ndarray:
    """ Calculates the intermolecular forces

    Returns an array of total forces for each atom with indices [atom, direction]
    """
    # ADDED comments, e.g., what are the elements of the return function F
    N = len(atoms)
    # Array of pair indices ("the pair of atoms i and j has the index x at Fs")
    ij_map = np.zeros((N, N), dtype=np.int_)
    Fs = []
    ind = 0
    # For each pair of atoms (counting each pair only once)
    for i in range(0, N - 1):
        for j in range(i + 1, N):
            # Calculate the force of that pair
            Fs.append(pair_force(atoms[i], atoms[j]))
            # Add the index of the pair to the array of pair indices
            ij_map[i, j] = ind
            ij_map[j, i] = ind
            ind += 1
    # F = []
    F = np.zeros((N, atoms[0].R.size))
    # For each pair of atoms (counting each pair twice)
    for i in range(N):
        # f = np.zeros(shape=atoms[i].R.shape)
        for j in range(N):
            ind = ij_map[i, j]
            if i < j:
                # f += Fs[ind]
                F[i, :] += Fs[ind]
            elif i > j:
                # f -= Fs[ind]
                F[i, :] -= Fs[ind]
            # There is no "else" since we don't count self-interaction
        # F.append(f)
    # F = np.array(F)
    return F


# @numba.njit
# def pair_force(atom1: Atom, atom2: Atom):
#     return morse_force(atom1, atom2)


# @numba.njit
# def pair_potential(atom1: Atom, atom2: Atom) -> float:
#     return morse_potential(atom1, atom2)


# @numba.njit
def morse_potential(atom1: Atom, atom2: Atom) -> float:
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    # ADDED comments and calculation of Morse potential
    r = np.sqrt(np.sum((atom1.R - atom2.R)**2))
    # Slides 11 equation 14
    return De*((1 - np.exp(-a*(r-re)))**2 - 1)


# @numba.njit
def morse_force(atom1: Atom, atom2: Atom) -> np.ndarray:
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    # ADDED comments and calculation of Morse force
    r_vec = atom1.R - atom2.R
    r = np.sqrt(np.sum(r_vec**2))

    # Slides 11 equation 14 derived by r according to slides 12 equation 3
    exp_term = np.exp(-a*(r-re))
    f = -2*a*De*(1-exp_term)*exp_term
    return f/r * r_vec


# @numba.njit
def lennard_jones_potential(atom1: Atom, atom2: Atom):
    epsilon = np.sqrt(atom1.LJ_epsilon * atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma + atom2.LJ_sigma) / 2
    # ADDED --> in problem 3: comments and calculation of LJ potential
    r = np.sqrt(np.sum((atom1.R - atom2.R)**2))
    # Slides 12 equation 13
    return 4*epsilon*((sigma/r)**12 - (sigma/r)**6)


# @numba.njit
def lennard_jones_force(atom1: Atom, atom2: Atom):
    epsilon = np.sqrt(atom1.LJ_epsilon * atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma + atom2.LJ_sigma) / 2
    # ADDED --> in problem 3: comments and calculation of LJ force
    r_vec = atom1.R - atom2.R
    r = np.sqrt(np.sum(r_vec**2))
    # Slides 11 equation 13 derived by r according to slides 12 equation 3
    return 24*epsilon*(2*sigma**12 * r**(-13) - sigma**6*r**(-7)) / r * r_vec


# @numba.njit
def velocity_verlet_update(atoms: tp.List[Atom], pair_force: callable) -> tp.List[Atom]:
    """ Update the status of the atoms using Velocity Verlet

    See the equations 10 and 11 of week 12 slides
    """
    # ADD comments and integration algorithm according to function name
    dt = atoms[0].dt
    dt2 = dt ** 2
    for i in range(len(atoms)):
        # Slides 12 equation 10
        atoms[i].R += dt * atoms[i].v + dt2/(2*atoms[i].mass) * atoms[i].force
    Fnew = calculate_force(atoms, pair_force=pair_force)
    for i in range(len(atoms)):
        # Slides 12 equation 11
        atoms[i].v += dt/(2*atoms[i].mass)*(Fnew[i] + atoms[i].force)
        atoms[i].force = Fnew[i]  # update force
    return atoms


# @numba.njit
def initialize_positions(atoms: tp.List[Atom]) -> None:
    # diatomic case
    atoms[0].set_position(np.array([-0.8, 0.0, 0.0]))
    atoms[1].set_position(np.array([0.7, 0.0, 0.0]))


# @numba.njit
def initialize_velocities(atoms: tp.List[Atom], T: float = 10) -> None:
    # ADDED --> in problem 4: comment on what is v_max
    # diatomic case
    # dims = atoms[0].dims
    kB = 3.16e-6  # in hartree/Kelvin
    for i in range(len(atoms)):
        # 1/2*mv^2 = 3/2*kB*T, multiply both sides by 2
        # -> Default T is 10
        # v_max = np.sqrt(3.0 / atoms[i].mass * kB * 10.0)
        v_max = np.sqrt(3 / atoms[i].mass * kB * T)
        atoms[i].set_velocity(np.array([1.0, 0.0, 0.0]) * v_max)

    # For some reason this line did not jit
    atoms[1].v = -1.0 * atoms[0].v
    # vel = -1.0 * atoms[0].v
    # atoms[1].v = vel


# @numba.njit
def initialize_force(atoms: tp.List[Atom], pair_force: callable) -> None:
    F = calculate_force(atoms, pair_force=pair_force)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])


# @numba.njit
def temperature(E_k: float, atoms: tp.List[Atom]) -> float:
    """ Calculate temperature according to equation 18 """
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6
    # ADDED calculation of temperature in problem 2
    return 2*E_k / (atoms[0].dims * len(atoms) * kB)


# @numba.njit
def avg_r(atoms: tp.List[Atom]) -> float:
    n = len(atoms)
    r_sum = 0
    pairs = 0
    for i in range(n):
        for j in range(i+1, n):
            r_sum += np.sqrt(np.sum((atoms[i].R - atoms[j].R)**2))
            pairs += 1
    return r_sum / pairs


def calculate_observables(atoms: tp.List[Atom], observables: Observables, pair_potential: callable) -> Observables:
    E_k, E_p = calculate_energetics(atoms, pair_potential=pair_potential)
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    # ADDED calculation of observables in problem 2
    observables.distance.append(avg_r(atoms))
    observables.temperature.append(temperature(E_k, atoms))
    return observables


# @numba.njit
def heat_capacity(E: np.ndarray, T: float) -> mpmath.mpf:
    """ Heat capacity

    Using arbitrary precision
    (but the results are still zero...)
    """
    kB = mpmath.mpf(3.16e-6)
    n = mpmath.mpf(len(E))
    E_mpf = mpmath.mpf(1) * E
    E_mean = mpmath.fsum(E_mpf) / n
    E2_mean = mpmath.fsum(E_mpf**2) / n
    # E2_mean = np.mean(E ** 2)
    # E_mean = np.mean(E)
    T_mpf = mpmath.mpf(T)
    return (E2_mean - E_mean**2) / (kB*T_mpf**2)


# @numba.njit
def morse_lj(atom: Atom, r_min: float = 0.4, r_max: float = 5, num=10000) \
        -> tp.Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    atom1 = Atom(atom.index, atom.mass, atom.dt, atom.dims)
    atom2 = Atom(atom.index, atom.mass, atom.dt, atom.dims)
    atom1.LJ_epsilon = atom.LJ_epsilon
    atom2.LJ_epsilon = atom.LJ_epsilon
    atom1.LJ_sigma = atom.LJ_sigma
    atom2.LJ_sigma = atom.LJ_sigma
    atom1.R = np.array([0.0, 0.0, 0.0])
    atom2.R = np.array([0.0, 0.0, 0.0])

    r = np.linspace(r_min, r_max, num)
    E_M = np.zeros_like(r)
    E_LJ = np.zeros_like(r)
    F_M = np.zeros_like(r)
    F_LJ = np.zeros_like(r)
    for i in range(r.size):
        atom2.R[0] = r[i]
        E_M[i] = morse_potential(atom1, atom2)
        E_LJ[i] = lennard_jones_potential(atom1, atom2)
        F_M[i] = morse_force(atom1, atom2)[0]
        F_LJ[i] = lennard_jones_force(atom1, atom2)[0]

    return r, E_M, E_LJ, F_M, F_LJ


def simulate(
        n_atoms: int, dims: int, dt: float, mass: float, T: float,
        lj_epsilon: float, lj_sigma: float,
        steps: int, obs_diff: int = 10,
        pair_force: callable = morse_force, pair_potential: callable = morse_potential) -> Observables:
    atoms = []
    for i in range(n_atoms):
        atoms.append(Atom(i, mass, dt, dims))
        atoms[i].set_LJ_parameters(lj_epsilon, lj_sigma)

    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms, T=T)
    initialize_force(atoms, pair_force=pair_force)

    for i in range(steps):
        atoms = velocity_verlet_update(atoms, pair_force=pair_force)
        if i % obs_diff == 0:
            observables = calculate_observables(atoms, observables, pair_potential=pair_potential)

    return observables
