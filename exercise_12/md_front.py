"""
Simple Molecular Dynamics code for course FYS-4096 Computational Physics

QUESTIONS:
Problem 1:
- Make the code to work and solve H2 using the Morse potential.
- Modify and comment especially at parts where it reads "# ADD"
- Follow instructions on ex12.pdf

Problems 2:
- Add observables: temperature, distance, and heat capacity.
- Follow instructions on ex12.pdf

Problem 3:
- Add Lennard-Jones capability etc.
- Follow instructions on ex12.pdf

Problem 4:
- Temperature dependent data and analysis
- Follow instructions on ex12.pdf


ANSWERS:
Problem 1:
- Default update/integration algorithm: velocity Verlet
- Boundary conditions: an infinite box
- Three-dimensional system
"""

import time

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

import exercise_12.md_back as md

# This is necessary for the fancier TeX-based labels to work
# plt.rc("text", usetex=True)


def plot_morse_lj():
    atom = md.Atom(
        index=0,
        mass=1860.0,
        dt=0.1,
        dims=3
    )
    atom.set_LJ_parameters(LJ_epsilon=0.1745, LJ_sigma=1.25)

    r, E_M, E_LJ, F_M, F_LJ = md.morse_lj(atom)

    fig1 = plt.figure()
    fig1.suptitle("Comparison of potentials")

    ax1_1 = fig1.add_subplot(121)
    ax1_1.plot(r, E_M, label="Morse")
    ax1_1.plot(r, E_LJ, label="Lennard-Jones")
    ax1_1.set_xlabel("r")
    ax1_1.set_ylabel("E")
    ax1_1.set_title("Potentials")
    ax1_1.set_ylim([-0.2, 0.3])
    ax1_1.legend()

    ax1_2 = fig1.add_subplot(122)
    ax1_2.plot(r, F_M, label="Morse")
    ax1_2.plot(r, F_LJ, label="Lennard-Jones")
    ax1_2.set_xlabel("r")
    ax1_2.set_ylabel("E")
    ax1_2.set_title("Forces")
    ax1_2.set_ylim([-1, 1])
    ax1_2.legend()

    # fig1.tight_layout()


def multi_temp():
    multi_start_time = time.perf_counter()
    temps = np.geomspace(start=100, stop=4000, num=10)
    obs_list = []

    steps = 100000
    dt = 0.1
    obs_diff = 10
    for i in range(temps.size):
        print("Simulating temperature {}/{}".format(i+1, temps.size))
        obs_list.append(
            md.simulate(
                n_atoms=2,
                dims=3,
                dt=dt,
                mass=1860.0,
                lj_epsilon=0.1745,
                lj_sigma=1.25,
                T=temps[i],
                steps=steps,
                obs_diff=obs_diff
            )
        )

    print("T variation took", time.perf_counter() - multi_start_time)

    # t = np.arange(0, steps // obs_diff, step=dt * obs_diff)

    obs_temps = np.array([obs.temperature for obs in obs_list])
    obs_distances = np.array([obs.distance for obs in obs_list])
    obs_e_kin = np.array([obs.E_kin for obs in obs_list])
    obs_e_pot = np.array([obs.E_pot for obs in obs_list])
    obs_e_tot = obs_e_kin + obs_e_pot

    obs_heat_capacities = np.zeros(temps.shape)
    for i in range(temps.size):
        obs_heat_capacities[i] = md.heat_capacity(obs_e_tot[i, :], temps[i])

    fig1 = plt.figure()
    fig1.suptitle("Temperature variation")

    ax1_1 = fig1.add_subplot(221)
    ax1_1.errorbar(
        x=temps, y=np.mean(obs_e_tot, axis=-1),
        yerr=np.std(obs_e_tot, axis=-1),
        fmt="o-",
        capsize=5,
        ecolor="black"
    )
    ax1_1.set_xlabel("T_start")
    ax1_1.set_ylabel("E_tot")
    ax1_1.set_title("Energy")
    
    ax1_2 = fig1.add_subplot(222)
    ax1_2.errorbar(
        x=temps, y=np.mean(obs_distances, axis=-1),
        yerr=np.std(obs_distances, axis=-1),
        fmt="o-",
        capsize=5,
        ecolor="black"
    )
    ax1_2.set_xlabel("T_start")
    ax1_2.set_ylabel("<r>")
    ax1_2.set_title("Average interatomic distance")

    ax1_3 = fig1.add_subplot(223)
    ax1_3.errorbar(
        x=temps, y=np.mean(obs_temps, axis=-1),
        yerr=np.std(obs_temps, axis=-1),
        fmt="o-",
        capsize=5,
        ecolor="black"
    )
    ax1_3.set_xlabel("T_start")
    ax1_3.set_ylabel("T_mean")
    ax1_3.set_title("Temperature")

    # Didn't have time to get this working
    # ax1_4 = fig1.add_subplot(224)
    # ax1_4.plot(
    #     x=temps, y=obs_heat_capacities,
    #     fmt="o-",
    # )


def run_single():
    start_time = time.perf_counter()

    steps = 100000
    dt = 0.1
    obs_diff = 10

    observables = md.simulate(
        n_atoms=2,
        dims=3,
        dt=dt,
        mass=1860.0,
        lj_epsilon=0.1745,
        lj_sigma=1.25,
        T=10,
        obs_diff=obs_diff,
        steps=steps
    )
    print("Morse simulation took", time.perf_counter() - start_time)

    start_time_lj = time.perf_counter()
    observables_lj = md.simulate(
        n_atoms=2,
        dims=3,
        dt=dt,
        mass=1860.0,
        lj_epsilon=0.1745,
        lj_sigma=1.25,
        T=10,
        obs_diff=obs_diff,
        steps=steps,
        pair_potential=md.lennard_jones_potential,
        pair_force=md.lennard_jones_force
    )
    print("LJ simulation took", time.perf_counter() - start_time_lj)

    plot_single(observables, steps=steps, obs_diff=obs_diff, dt=dt, title="Morse potential", check=True)
    print()
    plot_single(observables_lj, steps=steps, obs_diff=obs_diff, dt=dt, title="Lennard-Jones potential")


def plot_single(observables: md.Observables, steps: int, obs_diff: int, dt: float, title: str, check: bool = False):
    print(title)
    # Print energies
    E_kin = np.array(observables.E_kin)
    E_pot = np.array(observables.E_pot)
    E_tot = E_kin + E_pot
    print('E_kin', np.mean(E_kin))
    print('E_pot', np.mean(E_pot))
    print('E_tot', np.mean(E_kin) + np.mean(E_pot))

    if check:
        if abs(np.mean(E_tot) + 0.17274) > 1e-3:
            raise ValueError("A wild bug has appeared in the simulation code! Go fix it!")
        elif abs(np.mean(E_kin) - 0.000877) > 1e-3:
            raise ValueError("A wild bug has appeared in the simulation code! Go fix it!")

    T = np.mean(observables.temperature)
    T_std = np.std(observables.temperature)
    print("T mean", T)
    print("T std", T_std)
    print("Heat capacity", md.heat_capacity(E_tot, T))

    t = np.arange(0, steps // obs_diff, step=dt*obs_diff)

    fig1 = plt.figure()
    fig1.suptitle(title)
    ax1_1 = fig1.add_subplot(131)
    ax1_1.plot(t, E_tot)
    ax1_1.set_xlabel("t")
    ax1_1.set_ylabel(r"$E_{tot}$")
    ax1_1.set_title("Total energy as a function of simulation time")

    ax1_2 = fig1.add_subplot(132)
    ax1_2.plot(t, np.array(observables.distance))
    ax1_2.set_xlabel("t")
    ax1_2.set_ylabel("<r>")
    ax1_2.set_title("Average interatomic distance")

    ax1_3 = fig1.add_subplot(133)
    ax1_3.plot(t, np.array(observables.temperature))
    ax1_3.axhline(T, color="k")
    ax1_3.axhline(T + T_std, color="k", linestyle=":")
    ax1_3.axhline(T - T_std, color="k", linestyle=":")
    ax1_3.set_xlabel("t")
    ax1_3.set_ylabel("T")
    ax1_3.set_title("Temperature")


def main():
    run_single()
    print()
    plot_morse_lj()
    print()
    multi_temp()

    plt.show()


if __name__ == "__main__":
    main()
