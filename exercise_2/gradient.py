"""Problem 3: Gradient"""

import numba
import numpy as np

import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D


@numba.njit(parallel=True)
def test_fun_1d(x):
    return np.sin(x)


@numba.njit(parallel=True)
def test_fun_2d(coords: np.ndarray):
    return np.sin(coords[0, :]) - coords[1, :]


@numba.njit(parallel=True)
def test_fun_3d(coords: np.ndarray):
    return np.sin(coords[0, :]) + coords[1]**2 - coords[2]


# @numba.njit(parallel=True)
def gradient(function: callable, coords: np.ndarray, diff: np.ndarray = None) -> np.ndarray:
    """Compute the gradient at the given coordinates

    The N-dimensionality was quite a challenge

    :param function: the function for which to calculate the gradient
    :param coords: coordinates of the points to calculate at (point_count, num_dims)
    :param diff: distances to calculate the derivative at - array(num_dims)
    :return:
    """

    # if diff is None:
    #     if coords.ndim == 1:
    #         diff = 0.01
    #     else:
    #         diff = np.array([0.01]*coords.shape[0])

    assert(diff.ndim == 1)
    # assert(diff.ndim == 1 and coords.ndim == 1 or diff.shape[0] == coords.shape[0])

    # The one-dimensional case requires special handling due to the array indexing
    if diff.size == 1:
        return (function(coords + diff[0]) - function(coords - diff[0])) / (2 * diff[0])

    res_arr = np.zeros(coords.shape)

    # Similarily, the case of only one data point requires special handling
    if diff.size == coords.size:
        for i in range(diff.size):
            coords_diff = np.zeros(diff.shape)
            coords_diff[i] = diff[i]

            res = (function(coords + coords_diff) - function(coords - coords_diff)) / (2 * diff[i])
            res_arr[i] = res

        return res_arr

    # Compute the derivative for each dimension
    for i in range(coords.ndim):
        coords_diff = np.zeros(diff.shape)
        coords_diff[i] = diff[i]

        # This is equal to the one-dimensional derivative
        res = (function(coords + coords_diff[:, np.newaxis]) - function(coords - coords_diff[:, np.newaxis])) / (2 * diff[i])
        res_arr[i] = res

    return res_arr


def gradient_wrapper_2d(function: callable, x: np.ndarray, y: np.ndarray, diff: np.ndarray):
    coords = np.array([x.flatten(), y.flatten()])

    res = gradient(function, coords, diff)

    return res[0].reshape(x.shape), res[1].reshape(y.shape)


def test_gradient_1d():
    print("1D")
    x = np.linspace(0, np.pi, 20)
    f = test_fun_1d(x)

    np_grad = np.gradient(f) / np.gradient(x)

    diff = np.array([0.01])
    grad = gradient(test_fun_1d, x, diff)

    # print(np_grad)
    # print(grad)

    err = grad - np_grad
    print(np.sum(err) / err.size)


def test_gradient_2d():
    print("2D")
    x_range = np.linspace(0, np.pi, 100)
    y_range = np.linspace(0, np.pi, 100)

    x, y = np.meshgrid(x_range, y_range)
    coords = np.array([x.flatten(), y.flatten()])

    f = test_fun_2d(coords)
    f_numpy = f.reshape(100, 100)

    step = x_range[1] - x_range[0]
    np_grad = np.gradient(f_numpy) / step

    diff = np.array([0.01, 0.01])
    grad = gradient(test_fun_2d, coords, diff)
    grad_reshaped = grad.reshape(2, 100, 100)
    grad_reshaped = np.flip(grad_reshaped, axis=0)

    err = grad_reshaped - np_grad
    print(np.abs(err).sum() / err.size)


def test_gradient_3d():
    print("3D")
    grid_size = 100
    
    x_range = np.linspace(0, np.pi, grid_size)
    y_range = np.linspace(0, np.pi, grid_size)
    z_range = np.linspace(0, np.pi, grid_size)

    x, y, z = np.meshgrid(x_range, y_range, z_range)
    coords = np.array([x.flatten(), y.flatten(), z.flatten()])

    f = test_fun_3d(coords)
    f_numpy = f.reshape(grid_size, grid_size, grid_size)

    step = x_range[1] - x_range[0]
    np_grad = np.gradient(f_numpy) / step

    diff = np.array([0.01, 0.01, 0.01])
    grad = gradient(test_fun_3d, coords, diff)
    grad_reshaped = np.reshape(grad, (3, grid_size, grid_size, grid_size))

    # A manual attempt to decrease the error
    # grad_x = grad[0].reshape(grid_size, grid_size, grid_size)
    # grad_y = grad[1].reshape(grid_size, grid_size, grid_size)
    # grad_z = grad[2].reshape(grid_size, grid_size, grid_size)
    #
    # grad_reshaped = np.array([grad_x, grad_y, grad_z])

    print("Sum of the absolute values of the gradient")
    print(np.sum(np.abs(np_grad)))

    print("Sum of the absolute values of the error")
    err = grad_reshaped - np_grad
    print(np.abs(err).sum() / err.size)
    # This is relatively small compared to the sum of the absolute values of the gradient, so the gradient algorithm
    # is probably working
    # (although further testing is recommended)


def gradient_descent(function: callable, starting_point: np.ndarray, max_iter: int, eps: float, diff: np.ndarray, a: float = 0.1):
    x_vals = []
    gradients = []

    x = starting_point
    for i in range(max_iter):
        grad_x = gradient(function, x, diff)
        gamma = a / (np.linalg.norm(x, ord=2) + 1)

        x_vals.append(x)
        gradients.append(grad_x)

        if np.linalg.norm(grad_x, ord=2) < eps:
            break

        x = x - gamma * grad_x

    return np.array(x_vals), np.array(gradients)


def desc_test_fun1(x: np.ndarray):
    return np.linalg.norm(x, ord=2)


def desc_test_fun2(x: np.ndarray):
    return np.linalg.norm(x - np.array([4, 3, 2]), ord=2)


def test_desc():
    print("Gradient descent")
    x, grad = gradient_descent(desc_test_fun1, starting_point=np.array([2, 2, 3]), max_iter=1000, eps=0.01, diff=np.array([0.01, 0.01, 0.01]))

    # This is quite close to zero
    print(x[-1, :])

    plt.figure()
    plt.plot(np.sum(x, axis=1), label="x")
    plt.plot(np.sum(grad, axis=1), label="grad")
    plt.legend()
    plt.title("Gradient descent test 1")

    x2, grad2 = gradient_descent(desc_test_fun2, starting_point=np.array([1, 2, 3]), max_iter=1000, eps=0.01, diff=np.array([0.01, 0.01, 0.01]))

    # The point [4, 3, 2] was found correctly as well
    print(x2[-1, :])

    plt.figure()
    plt.plot(np.sum(x2, axis=1), label="x")
    plt.plot(np.sum(grad2, axis=1), label="grad")
    plt.legend()
    plt.title("Gradient descent test 2")
    plt.show()


def main():
    test_gradient_1d()
    test_gradient_2d()
    test_gradient_3d()

    print()
    test_desc()


if __name__ == "__main__":
    main()
