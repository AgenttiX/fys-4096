"""Problem 2: Integration"""

import time
import typing as tp

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# import mpmath
import numba
import numpy as np
import scipy.integrate
import sympy


@numba.njit(parallel=True)
def fun_a1(r):
    return r**2 * np.exp(-2*r)


@numba.njit(parallel=True)
def fun_a2(x):
    return np.sin(x) / x


@numba.njit(parallel=True)
def fun_a2_approx(x: np.ndarray):
    ret = np.zeros(x.shape)
    for i in range(ret.size):
        # Taylor approximation in case x is small
        if x[i] < 1e-16:
            ret[i] = 1 - (x[i]**2)/3
        else:
            ret[i] = np.sin(x[i]) / x[i]
    return ret


@numba.njit(parallel=True)
def fun_a3(x):
    return np.exp(np.sin(x**3))


# @numba.njit(parallel=True)
def int_accuracy(
        fun: callable,
        x_min: float, x_max: float,
        point_counts: np.ndarray) -> tp.Tuple[np.ndarray, np.ndarray]:
    """Compute the numeric integral for several point counts using the linspace(x_min, x_max, point_count)

    :param fun: function to integrate
    :param x_min: lower limit
    :param x_max: upper limit
    :param point_counts: array of different point counts to test
    :return: integral values for each point count
    """
    res_trapz = np.zeros(point_counts.size)
    res_simps = np.zeros(point_counts.size)

    for i, points in enumerate(point_counts):
        x = np.linspace(x_min, x_max, points)
        y = fun(x)
        # Numba does not yet support np.trapz
        res_trapz[i] = np.trapz(y, x)
        res_simps[i] = scipy.integrate.simps(y, x)

    return res_trapz, res_simps


def int_a():
    print("a)")

    # Test the integrations with several grid spacings
    max_points = 10000
    test_count = np.log10(max_points) * 10
    point_counts = np.logspace(1, int(np.log10(max_points)), num=test_count, dtype=np.int_)

    trapz1, simps1 = int_accuracy(fun_a1, 0, 1000, point_counts)
    print(trapz1[-1], simps1[-1])

    # There is a singularity at x = 0, so let's start at a small value
    x_min_a2 = np.finfo(np.float64).eps
    trapz2, simps2 = int_accuracy(fun_a2, x_min_a2, 1, point_counts)
    trapz2_approx, simps2_approx = int_accuracy(fun_a2_approx, 0, 1, point_counts)
    print(trapz2[-1], simps2[-1])

    trapz3, simps3 = int_accuracy(fun_a3, 0, 5, point_counts)
    print(trapz3[-1], simps3[-1])

    fig = plt.figure()
    # fig, axs = plt.subplots(3, 1, constrained_layout=True)
    # ax1 = axs[0]
    # ax2 = axs[1]
    # ax3 = axs[2]
    fig.suptitle("2 a)")
    x_label = "point count"
    y_label = "integral value"

    ax1 = fig.add_subplot(3, 1, 1)
    plt.plot(point_counts, trapz1, label="trapz")
    plt.plot(point_counts, simps1, label="simps")
    ax1.set_xlabel(x_label)
    ax1.set_ylabel(y_label)
    ax1.set_xscale("log")
    # ax1.set_title("2 a1")
    plt.legend()

    ax2 = fig.add_subplot(3, 1, 2)
    plt.plot(point_counts, trapz2, label="trapz")
    plt.plot(point_counts, simps2, label="simps")
    plt.plot(point_counts, trapz2_approx, "bo", label="trapz approx")
    plt.plot(point_counts, simps2_approx, "yo", label="simps approx")
    ax2.set_xlabel(x_label)
    ax1.set_ylabel(y_label)
    ax2.set_xscale("log")
    # ax1.set_title("2 a2")
    plt.legend()

    ax3 = fig.add_subplot(3, 1, 3)
    plt.plot(point_counts, trapz3, label="trapz")
    plt.plot(point_counts, simps3, label="simps")
    ax3.set_xlabel(x_label)
    ax1.set_ylabel(y_label)
    ax3.set_xscale("log")
    # ax1.set_title("2 a3")
    plt.legend()

    plt.show()


# Older more "manual" versions
#
# def int_a1():
#     x = np.linspace(0, 1000, 10000)
#     y = fun_a1(x)
#
#     print("fun1")
#     print(np.trapz(y, x))
#     print(scipy.integrate.simps(y, x))
#
#
# def int_a2():
#     print("fun2")
#     # There is a singularity at x = 0, so let's start from a small value
#     x_min = np.finfo(np.float64).eps
#     print("starting at", x_min, "(64-bit)")
#     x = np.linspace(x_min, 1, 10000)
#     y = fun_a2(x)
#
#     print(np.trapz(y, x))
#     print(scipy.integrate.simps(y, x))
#
#     # Let's start at a bit higher (in case someone is still using a 32-bit CPU and would need to adapt this code :P)
#     x_min = np.finfo(np.float32).eps
#     print("starting at", x_min, "(32-bit)")
#     x = np.linspace(x_min, 1, 10000)
#     y = fun_a2(x)
#
#     print(np.trapz(y, x))
#     print(scipy.integrate.simps(y, x))
#
#     print("approximated")
#     x = np.linspace(0, 1, 10000)
#     y = fun_a2_approx(x)
#
#     print(np.trapz(y, x))
#     print(scipy.integrate.simps(y, x))
#
#
# def int_a3():
#     x = np.linspace(0, 5, 10000)
#     y = fun_a3(x)
#
#     print("fun3")
#     print(np.trapz(y, x))
#     print(scipy.integrate.simps(y, x))


def int_b():
    print("b)")
    # Since the choice of method is free, of course I'll do it analytically!
    # <autism intensifies>
    x, y = sympy.symbols("x y")
    x0 = 0
    x1 = 2

    fun = x * sympy.exp(-(x**2 + y**2))

    # The symbols used for the order of integration in the problem are rather
    # ambiguous, since y is integrated from x0 to x1
    int_1 = sympy.integrate(fun, (x, -2, 2))
    int_2 = sympy.integrate(int_1, (y, x0, x1))
    print("dx dy")
    print(int_2)

    # However, if the order of integration is dy dx
    int_1 = sympy.integrate(fun, (y, -2, 2))
    int_2 = sympy.integrate(int_1, (x, x0, x1))
    print("dy dx")
    print(int_2)
    print(float(int_2))

    # Since the value was obtained symbolically and the result is rather simple, varying the parameters to measure
    # the accuracy of the output is rather unnecessary


# @numba.njit(parallel=True)
# def psi(r):
#     return np.exp(-r) / np.sqrt(np.pi)


def int_c_solution(r: float) -> float:
    """Analytic solution for exercise 2c

    :param r: R - the distance between rA and rB
    :return: value of the integral
    """
    return (1 - (1+r)*np.exp(-2*r)) / r


# @numba.njit(parallel=True)
# def int_c_integrable(r, ra, rb):
#     return np.exp(-2*np.linalg.norm(r-ra, ord=2)) / np.linalg.norm(r - rb, ord=2)


def int_c_constructor(ra: np.ndarray, rb: np.ndarray):
    """Construct a Numba-jitted function of three parameters

    It seemed like a good idea to include ra and rb into the compilation as global variables. However, this
    means that the function has to be compiled from scratch each time this constructor is called.

    (Some related documentation)
    https://numba.pydata.org/numba-doc/dev/user/faq.html#numba-doesn-t-seem-to-care-when-i-modify-a-global-variable
    """
    rax = ra[0]
    ray = ra[1]
    raz = ra[2]

    rbx = rb[0]
    rby = rb[1]
    rbz = rb[2]

    def integrable(rx: float, ry: float, rz: float):
        # Note that Scipy calls functions as fun(z, y, x)
        # However, the resulting integral is symmetric around the origin

        # r = np.sqrt(rx**2 + ry**2 + rz**2)
        rra = np.sqrt((rx-rax)**2 + (ry-ray)**2 + (rz-raz)**2)
        rrb = np.sqrt((rx-rbx)**2 + (ry-rby)**2 + (rz-rbz)**2)

        psi = np.exp(-rra) / np.sqrt(np.pi)
        return np.abs(psi)**2 / rrb

    # parallel=True cannot be applied as it results in
    # "NumbaWarning: parallel=True was specified but no transformation for parallel execution was possible."
    return numba.njit(integrable)


# def int_c_constructor_mpmath(ra: np.ndarray, rb: np.ndarray):
#     integrable = int_c_constructor(ra, rb)
#
#     def wrapper(rx, ry, rz):
#         return integrable(float(rx), float(ry), float(rz))
#
#     return wrapper


# Turned out to be too slow
# def int_c_symbolic():
#     print("Computing 2. c) symbolic")
#     rx, ry, rz = sympy.symbols("rx ry rz")
#     # rax, ray, raz = sympy.symbols("rax ray raz")
#     # rbx, rby, rbz = sympy.symbols("rbx rby rbz")
#
#     rax, ray, raz = (1, 0, 0)
#     rbx, rby, rbz = (0, 0, 0)
#
#     # r = sympy.sqrt(rx**2 + ry**2 + rz**2)
#     rra = sympy.sqrt((rx-rax)**2 + (ry-ray)**2 + (rz-raz)**2)
#     rrb = sympy.sqrt((rx-rbx)**2 + (ry-rby)**2 + (rz-rbz)**2)
#
#     psi_sym = sympy.exp(-rra) / sympy.sqrt(sympy.pi)
#
#     integrable = abs(psi_sym)**2 / abs(rrb)
#
#     int1 = sympy.integrate(integrable, (rx, -sympy.oo, sympy.oo))
#     int2 = sympy.integrate(int1, (ry, -sympy.oo, sympy.oo))
#     int3 = sympy.integrate(int2, (rz, -sympy.oo, sympy.oo))
#
#     limits = [-sympy.oo, sympy.oo]
#     # int3 = mpmath.quad(integrable, limits, limits, limits)
#
#     print(int3)
#     print(float(int3))


# Turned out to be too slow
# def int_c_mpmath():
#     print("Computing 2. c) using mpmath")
#
#     limits = [-mpmath.inf, mpmath.inf]
#     ra = np.array([0, 0, 0])
#     rb = np.array([1, 0, 0])
#     integrable = int_c_constructor_mpmath(ra, rb)
#
#     res = mpmath.quad(integrable, limits, limits, limits)
#
#     print(res)


# @numba.njit(parallel=True)
# def int_c_integrable_scipy(rz, ry, rx):
#     r = np.array([rx, ry, rz], dtype=np.float_)
#
#     # Set manually, since Scipy doesn't pass these
#     ra = np.array([0, 0, 0])
#     rb = np.array([1, 0, 0])
#
#     rra = np.linalg.norm(r - ra, ord=2)
#     rrb = np.linalg.norm(r - rb, ord=2)
#
#     wf = np.exp(-rra)/np.sqrt(np.pi)
#     ret = np.abs(wf)**2 / rrb
#     return ret


class LoopTimer:
    """A timer for measuring the duration of loops"""
    def __init__(self, total_loops: int):
        self.start_time = time.perf_counter()
        self.total_loops = total_loops
        self.latest_call = self.start_time

        self.call_times = []

    def time(self):
        now = time.perf_counter()
        elapsed = now - self.start_time
        self.call_times.append(now)

        loops_done = len(self.call_times)
        estimate = elapsed/loops_done * self.total_loops
        remaining = estimate - elapsed

        print("Loop {}/{}".format(loops_done, self.total_loops))
        print("Elapsed:", elapsed)
        print("Estimate:", estimate)
        print("Remaining:", remaining)

    def stats(self):
        print("Loop times")
        print(self.call_times)

        total_time = self.call_times[-1] - self.start_time
        print("Total time:", total_time)


def int_c_scipy_engine(ra: np.ndarray, rb: np.ndarray, max_dist: int) -> tp.Tuple[float, float, float]:
    """Compute the 2c integral value for given rA and rB

    :param ra: rA vector
    :param rb: rB vector
    :param max_dist: maximum distance to integrate to
    :return: analytic value, numerical value, error estimate for the numerical value
    """
    rr = np.linalg.norm(ra - rb, ord=2)
    analytic = int_c_solution(rr)

    integrable = int_c_constructor(ra, rb)

    # These give the same results
    # res, err = scipy.integrate.tplquad(
    #     integrable,
    #     a=-max_dist, b=max_dist,
    #     gfun=-max_dist, hfun=max_dist,
    #     qfun=-max_dist, rfun=max_dist,
    # )
    res2 = scipy.integrate.nquad(
        integrable,
        ranges=[[-max_dist, max_dist], [-max_dist, max_dist], [-max_dist, max_dist]],
        opts={
            "limit": 100,
        }
    )
    return analytic, res2[0], res2[1]


def int_c_scipy():
    print("2. c) using scipy")

    # Construct arrays for ra and rb so that we can test different values for them
    max_dist = 100
    ras = np.array([
        np.arange(1, 6, 1),
        np.zeros((5,)),
        np.zeros((5,))
    ]).transpose()
    rbs = np.array([
        np.zeros((5,)),
        np.arange(1, 6, 1),
        np.zeros((5,))
    ]).transpose()
    print("ra array")
    print(ras)
    print("rb array")
    print(rbs)

    # ra = np.array([0, 0, 0])
    # rb = np.array([1, 0, 0])

    analytic_arr = np.zeros((ras.shape[0], rbs.shape[0]))
    res_arr = np.zeros_like(analytic_arr)
    err_arr = np.zeros_like(analytic_arr)

    x_arr = np.zeros_like(analytic_arr)
    y_arr = np.zeros_like(analytic_arr)

    timer = LoopTimer(total_loops=25)

    # This could be parallelised using concurrent.futures.ProcessPoolExecutor.
    # However, this task has taken quite a bit of time already.
    for rai in range(ras.shape[0]):
        for rbi in range(rbs.shape[0]):
            analytic, res, err = int_c_scipy_engine(ras[rai, :], rbs[rbi, :], max_dist)
            analytic_arr[rai, rbi] = analytic
            res_arr[rai, rbi] = res
            err_arr[rai, rbi] = err

            # Constructing these coordinate arrays could be done more effectively, but its cost is insignificant
            # compared to the integration
            x_arr[rai, rbi] = ras[rai, 0]
            y_arr[rai, rbi] = rbs[rbi, 1]

            timer.time()

    timer.stats()

    print("Analytic")
    print(analytic_arr)
    print("Numeric")
    print(res_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_surface(x_arr, y_arr, analytic_arr)
    ax.set_xlabel("ra x")
    ax.set_ylabel("rb y")
    ax.set_title("Analytic")

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection="3d")
    ax2.plot_surface(x_arr, y_arr, res_arr)
    ax2.set_xlabel("ra x")
    ax2.set_ylabel("rb y")
    ax2.set_title("Numeric")

    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111, projection="3d")
    ax3.plot_surface(x_arr, y_arr, err_arr)
    ax3.set_xlabel("ra x")
    ax3.set_ylabel("rb y")
    ax3.set_title("Error estimate")

    fig4 = plt.figure()
    ax4 = fig4.add_subplot(111, projection="3d")
    ax4.plot_surface(x_arr, y_arr, res_arr - analytic_arr)
    ax4.set_xlabel("ra x")
    ax4.set_ylabel("rb y")
    ax4.set_title("Error (result - analytic)")

    plt.show()


def main():
    # int_a1()
    # int_a2()
    # int_a3()

    int_a()
    print()
    int_b()
    print()
    int_c_scipy()

    # int_c_symbolic()
    # int_c_mpmath()


if __name__ == "__main__":
    main()
