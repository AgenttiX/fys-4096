"""
Linear interpolation in 1d, 2d, and 3d

Intentionally unfinished :)

Related to FYS-4096 Computational Physics
exercise 2 assignments.

By Ilkka Kylanpaa on January 2019
"""

import numpy as np
import matplotlib.pyplot as plt

# It's rather weird that this unused import is necessary for Matplotlib to display 3D figures
from mpl_toolkits.mplot3d import Axes3D


# Basis functions for l1 and l2
def l1(t):
    """
    One-dimensional linear l1

    See the page 6 of the lecture slides
    """
    return 1 - t


def l2(t):
    """
    One-dimensional linear l2

    See the page 6 of the lecture slides
    """
    return t


# Note to the teacher: in Python 3 it's no longer necessary to inherit from object
# https://stackoverflow.com/questions/4015417/python-class-inherits-object
class LinearInterp:
    """Linear interpolation"""
    def __init__(self, dims: int, f, **kwargs):
        """Create the linear interpolator object

        Note for the teacher: the use of kwargs here is really nice, but specifying the arguments helps IDEs in finding
        possible programming mistakes (such as forgetting a necessary parameter).

        The hx, hy and hz variables provide the grid spacings

        :param dims: the number of dimensions of the function
        :param x: x points to use for the interpolation
        :param y: y points to use for the interpolation
        :param z: z points to use for the interpolation
        :param f: function values at the points
        """
        self.dims = dims
        if self.dims == 1:
            self.x = kwargs['x']
            self.f = f
            self.hx = np.diff(self.x)
        elif self.dims == 2:
            self.x = kwargs['x']
            self.y = kwargs['y']
            self.f = f
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
        elif self.dims == 3:
            self.x = kwargs['x']
            self.y = kwargs['y']
            self.z = kwargs['z']
            self.f = f
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
            self.hz = np.diff(self.z)
        else:
            raise ValueError('Either dims is missing or specific dims is not available')
    
    def eval1d(self, x) -> np.ndarray:
        """Interpolate the function for the given points (1D)

        :param x: the x points to interpolate at
        :return: interpolated function values at the points
        """
        if np.isscalar(x):
            x = np.array([x])
        N = len(self.x) - 1
        f = np.zeros((len(x),))
        ii = 0
        for val in x:
            # Find the largest index where self.x is smaller than the coordinate of the location we're interpolating.
            # In other words, find the nearest x which is smaller than val and for which we know the function value.
            i = np.floor(np.where(self.x <= val)[0][-1]).astype(int)
            if i == N:
                # If the interpolation location is outside self.x
                f[ii] = self.f[i]
            else:
                # t = relative location on the interval i - i+1
                t = (val-self.x[i])/self.hx[i]
                # Set the function value as the point on the line from f[i] to f[i+1]
                f[ii] = self.f[i]*l1(t)+self.f[i+1]*l2(t)
            ii += 1
        return f

    def eval2d(self, x, y) -> np.ndarray:
        """Interpolate the function for the given points (2D)

        :param x: x points to interpolate at
        :param y: y points to interpolate at
        :return: interpolated function values at the points
        """
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        Nx = len(self.x)-1
        Ny = len(self.y)-1
        f = np.zeros((len(x), len(y)))
        A = np.zeros((2, 2))
        ii = 0
        for valx in x:
            i = np.floor(np.where(self.x <= valx)[0][-1]).astype(int)
            if i == Nx:
                i -= 1
            jj = 0
            for valy in y:
                j = np.floor(np.where(self.y <= valy)[0][-1]).astype(int)
                if j == Ny:
                    j -= 1
                # Calculate the interpolated value using the equation 2 of the lecture slides
                tx = (valx-self.x[i])/self.hx[i]
                ty = (valy-self.y[j])/self.hy[j]
                ptx = np.array([l1(tx), l2(tx)])
                pty = np.array([l1(ty), l2(ty)])
                A[0, :] = np.array([self.f[i, j], self.f[i, j+1]])
                A[1, :] = np.array([self.f[i+1, j], self.f[i+1, j+1]])
                f[ii, jj] = np.dot(ptx, np.dot(A, pty))
                jj += 1
            ii += 1
        return f

    def eval3d(self, x, y, z) -> np.ndarray:
        """Interpolate the function for the given points (2D)

        :param x: x points to interpolate at
        :param y: y points to interpolate at
        :param z: z points to interpolate at
        :return: interpolated function values at the points
        """
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        if np.isscalar(z):
            z = np.array([z])
        Nx = len(self.x)-1
        Ny = len(self.y)-1
        Nz = len(self.z)-1
        f = np.zeros((len(x), len(y), len(z)))
        A = np.zeros((2, 2))
        B = np.zeros((2, 2))
        ii = 0

        # Iterate over all three dimensions
        for valx in x:
            # In my opinion this index finding is used so much that
            # it could have been made into a separate function for clarity
            i = np.floor(np.where(self.x <= valx)[0][-1]).astype(int)
            if i == Nx:
                i -= 1
            jj = 0
            for valy in y:
                j = np.floor(np.where(self.y <= valy)[0][-1]).astype(int)
                if j == Ny:
                    j -= 1
                kk = 0
                for valz in z:
                    k = np.floor(np.where(self.z <= valz)[0][-1]).astype(int)
                    if k == Nz:
                        k -= 1
                    # Calculate the interpolated value using the equation 3 of the lecture slides
                    tx = (valx-self.x[i])/self.hx[i]
                    ty = (valy-self.y[j])/self.hy[j]
                    tz = (valz-self.z[k])/self.hz[k]
                    ptx = np.array([l1(tx), l2(tx)])
                    pty = np.array([l1(ty), l2(ty)])
                    ptz = np.array([l1(tz), l2(tz)])

                    # Create the matrices "A_k" and "A_k+1"
                    B[0, :] = np.array([self.f[i, j, k], self.f[i, j, k+1]])
                    B[1, :] = np.array([self.f[i+1, j, k], self.f[i+1, j, k+1]])
                    A[:, 0] = np.dot(B, ptz)
                    B[0, :] = np.array([self.f[i, j+1, k], self.f[i, j+1, k+1]])
                    B[1, :] = np.array([self.f[i+1, j+1, k], self.f[i+1, j+1, k+1]])
                    A[:, 1] = np.dot(B, ptz)

                    # Perform the matrix multiplication
                    f[ii, jj, kk] = np.dot(ptx, np.dot(A, pty))
                    kk += 1
                jj += 1
            ii += 1
        return f


def main():
    fig1d = plt.figure()
    ax1d = fig1d.add_subplot(111)

    # 1d example
    x = np.linspace(0., 2.*np.pi, 10)
    y = np.sin(x)
    lin1d = LinearInterp(x=x,  f=y,  dims=1)
    xx = np.linspace(0., 2.*np.pi, 100)
    ax1d.plot(xx, lin1d.eval1d(xx))
    ax1d.plot(x, y, 'o', xx, np.sin(xx), 'r--')
    ax1d.set_title('function')

    # 2d example
    fig2d = plt.figure()
    ax2d = fig2d.add_subplot(221, projection='3d')
    ax2d2 = fig2d.add_subplot(222, projection='3d')
    ax2d3 = fig2d.add_subplot(223)
    ax2d4 = fig2d.add_subplot(224)

    x = np.linspace(-2.0, 2.0, 11)
    y = np.linspace(-2.0, 2.0, 11)
    X, Y = np.meshgrid(x, y)
    Z = X*np.exp(-1.0*(X*X+Y*Y))
    ax2d.plot_wireframe(X, Y, Z)
    ax2d3.pcolor(X, Y, Z)
    # ax2d3.contourf(X, Y, Z)

    lin2d=LinearInterp(x=x, y=y, f=Z, dims=2)
    x = np.linspace(-2.0, 2.0, 51)
    y = np.linspace(-2.0, 2.0, 51)
    X, Y = np.meshgrid(x, y)
    Z = lin2d.eval2d(x, y)
     
    ax2d2.plot_wireframe(X, Y, Z)
    ax2d4.pcolor(X, Y, Z)
    
    # 3d example
    x = np.linspace(0.0, 3.0, 10)
    y = np.linspace(0.0, 3.0, 10)
    z = np.linspace(0.0, 3.0, 10)
    X, Y, Z = np.meshgrid(x, y, z)
    F = (X+Y+Z)*np.exp(-1.0*(X*X+Y*Y+Z*Z))
    X, Y = np.meshgrid(x, y)
    fig3d = plt.figure()
    ax = fig3d.add_subplot(121)
    ax.pcolor(X, Y, F[..., int(len(z)/2)])
    lin3d = LinearInterp(x=x, y=y, z=z, f=F, dims=3)
    
    x = np.linspace(0.0, 3.0, 50)
    y = np.linspace(0.0, 3.0, 50)
    z = np.linspace(0.0, 3.0, 50)
    X, Y = np.meshgrid(x, y)
    F = lin3d.eval3d(x, y, z)
    ax2 = fig3d.add_subplot(122)
    ax2.pcolor(X, Y, F[..., int(len(z)/2)])

    plt.show()


if __name__ == "__main__":
    main()
