"""
This module contains functions for numerical calculus:
- first and second derivatives
- 1D integrals: Riemann, trapezoid, Simpson, 
  and Monte Carlo with uniform random numbers
"""

import numba
import numpy as np


# All this excessive use of Numba is mostly for testing what kinds of functions it can compile

@numba.njit(parallel=True)
def eval_derivative(function: callable, x, dx):
    """ 
    This calculates the first derivative with
    symmetric two point formula, which has O(h^2)
    accuracy. See, e.g., FYS-4096 lecture notes.
    """
    return (function(x+dx)-function(x-dx))/2/dx


@numba.njit(parallel=True)
def eval_2nd_derivative(function: callable, x, dx):
    """ 
    This calculates the second derivative with
    O(h^2) accuracy. See, e.g., FYS-4096 lecture 
    notes.
    """
    return (function(x+dx)+function(x-dx)-2.*function(x))/dx**2


@numba.njit(parallel=True)
def riemann_sum(x, function: callable):
    """ 
    Left Rieman sum for uniform grid. 
    See, e.g., FYS-4096 lecture notes.
    """
    dx = x[1]-x[0]
    f = function(x)
    return np.sum(f[0:-1])*dx


@numba.njit(parallel=True)
def trapezoid(x, function: callable):
    """ 
    Trapezoid for uniform grid. 
    See, e.g., FYS-4096 lecture notes.
    """
    dx=x[1]-x[0]
    f=function(x)
    return (f[0]/2+np.sum(f[1:-1])+f[-1]/2)*dx


@numba.njit(parallel=True)
def simpson_integration(x, function: callable):
    """ 
    Simpson rule for uniform grid 
    See, e.g., FYS-4096 lecture notes.
    """
    f=function(x)
    N = len(x)-1
    dx=x[1]-x[0]
    s0=s1=s2=0.
    for i in range(1,N,2):
        s0+=f[i]
        s1+=f[i-1]
        s2+=f[i+1]
    s=(s1+4.*s0+s2)/3
    if (N+1)%2 == 0:
        return dx*(s+(5.*f[N]+8.*f[N-1]-f[N-2])/12)
    else:
        return dx*s


@numba.njit(parallel=True)
def simpson_nonuniform(x, function: callable):
    """ 
    Simpson rule for nonuniform grid 
    See, e.g., FYS-4096 lecture notes.
    """
    f = function(x)
    N = len(x)-1
    h = np.diff(x)
    s = 0.
    for i in range(1, N, 2):
        hph=h[i]+h[i-1]
        s+=f[i]*(h[i]**3+h[i-1]**3+3.*h[i]*h[i-1]*hph)/6/h[i]/h[i-1]
        s+=f[i-1]*(2.*h[i-1]**3-h[i]**3+3.*h[i]*h[i-1]**2)/6/h[i-1]/hph
        s+=f[i+1]*(2.*h[i]**3-h[i-1]**3+3.*h[i-1]*h[i]**2)/6/h[i]/hph
    if (N+1)%2 == 0:
        s+=f[N]*(2.*h[N-1]**2+3.*h[N-2]*h[N-1])/6/(h[N-2]+h[N-1])
        s+=f[N-1]*(h[N-1]**2+3.*h[N-1]*h[N-2])/6/h[N-2]
        s-=f[N-2]*h[N-1]**3/6/h[N-2]/(h[N-2]+h[N-1])
    return s


@numba.njit(parallel=True)
def monte_carlo_integration(fun: callable, xmin, xmax, blocks: int, iters: int) -> tuple:
    """ 
    1D Monte Carlo integration with uniform random numbers
    in range [xmin,xmax]. As output one gets the value of 
    the integral and one sigma statistical error estimate,
    that is, ~68% reliability. Two sigma and three sigma
    estimates are with ~95% and ~99.7% reliability, 
    respectively. See, e.g., FYS-4096 lecture notes. 
    """
    block_values=np.zeros((blocks,))
    L=xmax-xmin
    for block in range(blocks):
        for i in range(iters):
            x = xmin+np.random.rand()*L
            block_values[block]+=fun(x)
        block_values[block]/=iters
    I = L*np.mean(block_values)
    dI = L*np.std(block_values)/np.sqrt(blocks)
    return I, dI


# Tests performed in main
# (The test functions have been moved to test_num_calculus.py)
# def main():
#     """ Performing all the tests related to this module """
#     test_first_derivative()
#     test_second_derivative()
#     test_riemann_sum()
#     test_trapezoid()
#     test_simpson_integration()
#     test_simpson_nonuniform()
#     test_monte_carlo_integration()


# if __name__=="__main__":
#     main()
