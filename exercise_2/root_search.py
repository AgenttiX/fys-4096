"""Problem 4: Root search

The problem appears somewhat ambiguous. Is it really so that we should only create the routines for finding the
indices needed in interpolation?
"""

import numpy as np


def grid(r_0: float, r_max: float, dim: int) -> np.ndarray:
    """Grid generator for problem 4b"""
    r = np.zeros((dim,))
    h = np.log(r_max / r_0 + 1) / (dim - 1)

    r[0] = 0.
    for i in range(1, dim):
        r[i] = r_0 * (np.exp(i * h) - 1)

    return r


def find_index(vec: np.ndarray, x: float):
    """A general index finder

    Found from the linear_interp.py template, approx. line 73
    """
    return np.floor(np.where(vec <= x)[0][-1]).astype(int)


def find_index_lin(vec: np.ndarray, x: float):
    """Problem 4a solution

    Utilises the premise that the input vector is a linear grid
    """
    step = vec[1] - vec[0]
    diff = x - vec[0]
    return np.floor(diff / step).astype(np.int_)


def find_index_4b(x: float, r_0: float, r_max: float, dim: int):
    """Problem 4b solution

    Utilises the fact that the input vector is a grid created by grid()
    """
    h = np.log(r_max / r_0 + 1) / (dim - 1)

    # This equation is obtained by solving the equation in the example code for i
    return np.floor(1/h * np.log(x/r_0 + 1)).astype(np.int_)


def test_lin():
    x = np.linspace(0, 20, 100)

    i = find_index(x, 3.5)
    print(i, x[i], x[i+1])

    i_lin = find_index_lin(x, 3.5)
    print(i_lin, x[i_lin], x[i_lin+1])


def test_4b():
    r_max = 100
    r_0 = 1e-5
    dim = 100

    g = grid(r_0, r_max, dim)

    x = grid(r_0, r_max, dim)

    i_gen = find_index(x, 5e-3)
    print(i_gen, x[i_gen], x[i_gen+1])

    i = find_index_4b(5e-3, r_0, r_max, dim)
    print(i, x[i], x[i+1])


if __name__ == "__main__":
    test_lin()
    print()
    test_4b()
