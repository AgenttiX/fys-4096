import numba
import numpy as np


@numba.njit(parallel=True)
def fun_1d(x):
    return np.sin(x) + 2*(x**2) - 5


@numba.njit(parallel=True)
def fun_2d(x, y):
    return np.sin(x*y) + x - 2*y


@numba.njit(parallel=True)
def fun_3d(x, y, z):
    return np.sin(x*y*z) + x - y*z


def cum_squares_relative_error(values: np.ndarray, estimates: np.ndarray) -> float:
    """Cumulative relative squared error

    The parameters should have equal dimensions
    :param values: actual values
    :param estimates: estimated values
    """
    if values.shape != estimates.shape:
        raise ValueError("Invalid dimensions {} and {}".format(values.shape, estimates.shape))
    return np.sqrt(np.sum((estimates - values)**2)) / values.size
