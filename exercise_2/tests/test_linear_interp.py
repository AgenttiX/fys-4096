import unittest

import numpy as np

from exercise_2 import linear_interp
from . import test_functions


class LinearInterpTest(unittest.TestCase):
    def test_1d(self):
        x = np.linspace(-10, 10, 50)
        f = test_functions.fun_1d(x)
        lin = linear_interp.LinearInterp(dims=1, f=f, x=x)

        int_x = np.linspace(-10, 10, 300)
        int_f = lin.eval1d(int_x)
        int_f_actual = test_functions.fun_1d(int_x)

        err = test_functions.cum_squares_relative_error(int_f_actual, int_f)
        print("1D linear error", err)

        self.assertAlmostEqual(err, 0, delta=0.01)

    def test_2d(self):
        x = np.linspace(-3, 3, 20)
        y = np.linspace(-3, 3, 20)
        X, Y = np.meshgrid(x, y)
        f = test_functions.fun_2d(X, Y)
        lin = linear_interp.LinearInterp(dims=2, f=f, x=x, y=y)

        int_x = np.linspace(-3, 3, 100)
        int_y = np.linspace(-3, 3, 100)
        int_f = lin.eval2d(int_x, int_y)

        int_X, int_Y = np.meshgrid(int_x, int_y)
        int_f_actual = test_functions.fun_2d(int_X, int_Y)

        err = test_functions.cum_squares_relative_error(int_f_actual, int_f)
        print("2D linear error", err)

        self.assertAlmostEqual(err, 0, delta=0.01)

    def test_3d(self):
        x = np.linspace(-3, 3, 20)
        y = np.linspace(-3, 3, 20)
        z = np.linspace(-3, 3, 20)
        X, Y, Z = np.meshgrid(x, y, z)
        f = test_functions.fun_3d(X, Y, Z)
        lin = linear_interp.LinearInterp(dims=3, f=f, x=x, y=y, z=z)

        int_x = np.linspace(-3, 3, 50)
        int_y = np.linspace(-3, 3, 50)
        int_z = np.linspace(-3, 3, 50)
        int_f = lin.eval3d(x=int_x, y=int_y, z=int_z)

        int_X, int_Y, int_Z = np.meshgrid(int_x, int_y, int_z)
        int_f_actual = test_functions.fun_3d(int_X, int_Y, int_Z)

        err = test_functions.cum_squares_relative_error(int_f_actual, int_f)
        print("3D linear error", err)

        self.assertAlmostEqual(err, 0, delta=0.01)


# TODO: make better tests
# use gradient
# estimate - actual should not be larger than the gradient (times 3x etc. because Hervanta)
# gradient is the derivative


# TODO: make presentable figures
