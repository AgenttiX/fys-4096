import unittest

import numba
import numpy as np

from exercise_2 import num_calculus


# Analytical test function definitions
@numba.njit(parallel=True)
def test_fun(x):
    """ This is the test function used in unit testing"""
    return np.exp(-x)


@numba.njit(parallel=True)
def test_fun_der(x):
    """
    This is the first derivative of the test
    function used in unit testing.
    """
    return -np.exp(-x)


@numba.njit(parallel=True)
def test_fun_der2(x):
    """
    This is the second derivative of the test
    function used in unit testing.
    """
    return np.exp(-x)


@numba.njit(parallel=True)
def test_fun2(x):
    """
    sin(x) in range [0,pi/2] is used for the integration tests.
    Should give 1 for the result.
    """
    return np.sin(x)


@numba.njit(parallel=True)
def test_fun2_int(a,b):
    """
    Integration of the test function (test_fun2).
    """
    return -np.cos(b)+np.cos(a)


class NumCalculusTest(unittest.TestCase):
    # Test routines for unit testing
    @staticmethod
    def test_first_derivative(tolerance=1.0e-3):
        """ Test routine for first derivative of f"""
        x = 0.8
        dx = 0.01
        df_estimate = num_calculus.eval_derivative(test_fun, x, dx)
        df_exact = test_fun_der(x)
        err = np.abs(df_estimate - df_exact)
        working = False
        if err < tolerance:
            # print('First derivative is OK')
            working = True
        else:
            # print('First derivative is NOT ok!!')
            raise AssertionError
        return working

    @staticmethod
    def test_second_derivative(tolerance=1.0e-3):
        """ Test routine for first derivative of f"""
        x = 0.8
        dx = 0.01
        df_estimate = num_calculus.eval_2nd_derivative(test_fun, x, dx)
        df_exact = test_fun_der2(x)
        err = np.abs(df_estimate - df_exact)
        working = False
        if err < tolerance:
            # print('Second derivative is OK')
            working = True
        else:
            # print('Second derivative is NOT ok!!')
            raise AssertionError
        return working

    @staticmethod
    def test_riemann_sum(tolerance=1.0e-2):
        """ Test routine for Riemann integration"""
        a = 0
        b = np.pi / 2
        x = np.linspace(a, b, 100)
        int_estimate = num_calculus.riemann_sum(x, test_fun2)
        int_exact = test_fun2_int(a, b)
        err = np.abs(int_estimate - int_exact)
        working = False
        if err < tolerance:
            # print('Riemann integration is OK')
            working = True
        else:
            # print('Riemann integration is NOT ok!!')
            raise AssertionError
        return working

    @staticmethod
    def test_trapezoid(tolerance=1.0e-4):
        """ Test routine for trapezoid integration"""
        a = 0
        b = np.pi / 2
        x = np.linspace(a, b, 100)
        int_estimate = num_calculus.trapezoid(x, test_fun2)
        int_exact = test_fun2_int(a, b)
        err = np.abs(int_estimate - int_exact)
        working = False
        if err < tolerance:
            # print('Trapezoid integration is OK')
            working = True
        else:
            # print('Trapezoid integration is NOT ok!!')
            raise AssertionError
        return working

    @staticmethod
    def test_simpson_integration(tolerance=1.0e-6):
        """ Test routine for uniform simpson integration"""
        a = 0
        b = np.pi / 2
        x = np.linspace(a, b, 100)
        int_estimate = num_calculus.simpson_integration(x, test_fun2)
        int_exact = test_fun2_int(a, b)
        err = np.abs(int_estimate - int_exact)
        working = False
        if err < tolerance:
            # print('Uniform simpson integration is OK')
            working = True
        else:
            # print('Uniform simpson integration is NOT ok!!')
            raise AssertionError
        return working

    @staticmethod
    def test_simpson_nonuniform(tolerance=1.0e-6):
        """ Test routine for nonuniform simpson integration"""
        a = 0
        b = np.pi / 2
        x = np.linspace(a, b, 100)
        int_estimate = num_calculus.simpson_nonuniform(x, test_fun2)
        int_exact = test_fun2_int(a, b)
        err = np.abs(int_estimate - int_exact)
        working = False
        if err < tolerance:
            # print('Nonuniform simpson integration is OK')
            working = True
        else:
            # print('Nonuniform simpson integration is NOT ok!!')
            raise AssertionError
        return working

    @staticmethod
    def test_monte_carlo_integration():
        """
        Test routine for monte carlo integration.
        Testing with 3*sigma error estimate, i.e., 99.7%
        similar integrations should be within this range.
        """
        a = 0
        b = np.pi / 2
        blocks = 100
        iters = 1000
        int_est, err_est = num_calculus.monte_carlo_integration(test_fun2, a, b, blocks, iters)
        int_exact = test_fun2_int(a, b)
        err = np.abs(int_est - int_exact)
        working = False
        if err < 3. * err_est:
            # print('Monte Carlo integration is OK')
            working = True
        else:
            # print('Monte Carlo integration is NOT ok!!')
            raise AssertionError
        return working
