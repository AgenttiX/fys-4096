"""Problem 4"""

import typing as tp

import matplotlib.pyplot as plt
import numba
import numpy as np


def analytical(d, lm=1, eps=1, l=1):
    """Analytical solution for the electric field

    At r = (L/2+d, 0)
    """
    return lm/(4*np.pi*eps) * (1/d - 1/(d+l))


@numba.njit
def field_diff(point: np.ndarray, rod_point: np.ndarray, lm=1, eps0=1) -> np.ndarray:
    """A differential field element at a point caused by the interaction with rod_point"""
    diff_vec = point - rod_point
    norm = np.linalg.norm(point - rod_point)
    return lm / (4*np.pi*eps0 * norm**3) * diff_vec


@numba.njit
def rod_array(rod_end_1: np.ndarray, rod_end_2: np.ndarray, rod_point_count: int) -> np.ndarray:
    """Create the array consisting of the rod points"""
    rod_points = np.zeros((rod_point_count, rod_end_1.size))
    for i in range(rod_end_1.size):
        rod_points[:, i] = np.linspace(rod_end_1[i], rod_end_2[i], rod_point_count)
    return rod_points


@numba.njit
def field_in_point(rod: np.ndarray, point: np.ndarray):
    """Calculate the electric field at a point"""
    diff_vecs = np.zeros(rod.shape)
    for i in range(rod.shape[0]):
        diff_vecs[i, :] = field_diff(point, rod[i, :])
    return np.sum(diff_vecs, axis=0)


@numba.njit(parallel=True)
def field_in_points(rod: np.ndarray, xy_mesh: np.ndarray):
    """Calculate the electric field in several points (mesh)"""
    uv_mesh = np.zeros_like(xy_mesh)

    for i in range(xy_mesh.shape[1]):
        for j in range(xy_mesh.shape[2]):
            uv_mesh[:, i, j] = field_in_point(rod, xy_mesh[:, i, j])
    return uv_mesh


@numba.njit(parallel=True)
def field_in_points_nomesh(rod: np.ndarray, points: np.ndarray):
    """Calculate the electric field in several points (no mesh)"""
    uv = np.zeros_like(points)
    for i in range(points.shape[1]):
        uv[:, i] = field_in_point(rod, points[:, i])
    return uv


@numba.njit
def symlog(x: tp.Union[int, float, np.ndarray]) -> tp.Union[int, float, np.ndarray]:
    """Logarithm-based scaling (symmetric)

    (Fails for values smaller than 1)
    https://stackoverflow.com/questions/24490753/logarithmic-lenghts-in-plotting-arrows-with-quiver-function-from-pyplot
    """
    return np.sign(x) * np.log(np.abs(x))


@numba.njit
def symsqrt(x: tp.Union[int, float, np.ndarray]) -> tp.Union[int, float, np.ndarray]:
    """Square root scaling (symmetric)

    Works for small values as well
    """
    return np.sign(x) * np.sqrt(np.abs(x))


def main():
    # Create the rod
    l = 1
    rod_1 = np.array([-l/2, 0])
    rod_2 = np.array([l/2, 0])
    rod = rod_array(rod_1, rod_2, 100)

    # Create the grid
    grid_size = 100
    # grid_size = 200
    x = np.linspace(-5, 5, grid_size)
    y = np.linspace(-5, 5, grid_size)

    x_mesh, y_mesh = np.meshgrid(x, y)
    xy_mesh = np.stack((x_mesh, y_mesh))

    # Compute the electric field
    uv_mesh = field_in_points(rod, xy_mesh)
    u_mesh = uv_mesh[0, :, :]
    v_mesh = uv_mesh[1, :, :]

    plt.quiver(x_mesh, y_mesh, symsqrt(u_mesh), symsqrt(v_mesh))

    # Compare analytical and numerical results
    test_min_dist = 0.1     # This has to be a non-zero value to prevent division by zero
    test_max = 5
    test_points = 100
    x_test = np.linspace(l/2 + test_min_dist, test_max, test_points)
    y_test = np.zeros_like(x_test)
    xy_test = np.stack((x_test, y_test))

    ana_test = analytical(x_test - l/2)
    uv_test = field_in_points_nomesh(rod, xy_test)

    # The difference is close to the rod point count
    # Probably the analytical result should be multiplied by the amount of points or the numerical result be divided
    # by it
    # print(np.sum(uv_test[0, :])/ np.sum(ana_test))

    plt.figure()
    plt.plot(ana_test / np.sum(ana_test), label="Analytical")
    plt.plot(uv_test[0, :] / np.sum(uv_test[0, :]), label="Numerical")
    plt.legend()
    plt.title("Analytical vs. numerical (normed)")
    plt.show()


if __name__ == "__main__":
    main()
