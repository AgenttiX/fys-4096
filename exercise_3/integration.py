"""Problem 1"""

import numpy as np
import scipy.integrate


def f(x, y):
    # As a side note, having a symmetric function for testing may be
    # rather dangerous, as some integration functions of Scipy expect
    # the integrable to be f(z, y, x) etc.
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.tplquad.html
    return (x + y) * np.exp(-(x**2 + y**2))


def main():
    d = 0.001
    x = np.arange(0, 2, d)
    y = np.arange(-2, 2, d)

    x_mesh, y_mesh = np.meshgrid(x, y)
    z = f(x_mesh, y_mesh)

    int_x = scipy.integrate.simps(z, x)
    int_xy = scipy.integrate.simps(int_x, y)

    print(int_xy)
    # The result is for d = 0.01
    # 0.864847483803314
    # d = 0.001
    # 0.8658193947299347
    # d = 0.0001
    # Traceback (most recent call last):
    #   File "/home/mika/Git/fys-4096/exercise_3/integration.py", line 33, in <module>
    #     main()
    #   File "/home/mika/Git/fys-4096/exercise_3/integration.py", line 15, in main
    #     z = f(x_mesh, y_mesh)
    #   File "/home/mika/Git/fys-4096/exercise_3/integration.py", line 6, in f
    #     return (x + y) * np.exp(-(x**2 + y**2))
    # MemoryError
    # whoopsie :)
    # (Apparently we should use an algorithm for which the memory consumption is smaller than Omega(len(x)*len(y)) )
    # Anyway, when rounded, the value for d = 0.001 is correct to up to three decimals

    # The analytically calculated value from the previous exercise is
    # -sqrt(pi)*exp(-4)*erf(2)/2 + sqrt(pi)*erf(2)/2
    # 0.8659255065387448


if __name__ == "__main__":
    main()
