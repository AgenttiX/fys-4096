"""Problem 2"""

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

# Specifying the package helps the IDE in finding the correct file
from exercise_3 import linear_interp


def f(x, y):
    return (x + y)*np.exp(-(x**2 + y**2))


def main():
    n = 40
    x = np.linspace(-2, 2, n)
    y = np.linspace(-2, 2, n)

    x_mesh, y_mesh = np.meshgrid(x, y)
    f_mesh = f(x_mesh, y_mesh)

    n_interp = 100
    x_interp = np.linspace(0, 2, n_interp)
    y_interp = np.sqrt(2) * x_interp

    # Y goes outside the original area, but since the function approaches zero, it doesn't cause trouble
    print(y_interp[-1])

    assert(x_interp.ndim == 1)
    assert(y_interp.ndim == 1)
    assert(x_interp.shape == y_interp.shape)

    # x_interp_mesh, y_interp_mesh = np.meshgrid(x_interp, y_interp)

    # It would be great if linear_interp would check the dimensions of the input arrays
    interp = linear_interp.linear_interp(x=x, y=y, f=f_mesh, dims=2)

    f_exact = f(x_interp, y_interp)

    # Highly inefficient but simple and works
    # f_interp_2d = interp.eval2d(x=x_interp, y=y_interp)
    # f_interp = np.diag(f_interp_2d)

    # We evaluate the 2D interpolation at each point separately
    # (since otherwise the method would return a 2D grid)
    f_interp = np.zeros_like(x_interp)
    for i in range(x_interp.size):
        f_interp[i] = interp.eval2d(x=x_interp[i], y=y_interp[i])

    # Old plotting (the one that was in the final tag)
    # fig = plt.figure()
    # ax1 = fig.add_subplot(121)
    # ax1.plot(x_interp, f_exact)
    # ax1.set_title("Exact")
    #
    # ax2 = fig.add_subplot(122)
    # ax2.plot(x_interp, f_interp)
    # ax2.set_title("Interpolation")
    # plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(x_interp, f_exact, label="Exact")
    ax.plot(x_interp, f_interp, label="Interpolated")
    ax.legend()
    plt.show()

    # It's visible that the interpolation isn't as accurate outside the original area

    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection="3d")
    # ax.plot_surface(x_mesh, y_mesh, f_mesh)
    # plt.show()


if __name__ == "__main__":
    main()
