"""
FYS-4096 Computational physics 

1. Add code to function 'largest_eig'
- use the power method to obtain 
  the largest eigenvalue and the 
  corresponding eigenvector of the
  provided matrix

2. Compare the results with scipy's eigs
- this is provided, but you should use
  that to validating your power method
  implementation

"""


from numpy import *
from matplotlib.pyplot import *
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps

import time
import numba

ITERS = 1000000


def largest_eigs(A: np.ndarray, tol=1e-12) -> tuple:
    """
    Simple power method code needed here
    """
    dim = A.shape[1]
    b = np.random.rand(dim)
    b = b / np.linalg.norm(b)

    for n in range(1, ITERS):
        b_old = 1.0*b
        anx = A @ b
        b = anx / np.linalg.norm(anx)
        # print(np.linalg.norm(b-b_old))
        if np.linalg.norm(b-b_old) < tol:
            break
    else:
        # Python has this weird but useful for-else syntax
        # (the else block is executed if no break statement was encountered)
        raise ArithmeticError("The result did not converge")

    # print(n)
    eig_value = np.mean(anx / b)
    # eig_value = np.sum(anx) / np.sum(b)
    eig_vector = b

    return eig_value, eig_vector


@numba.jit
def largest_eigs_opt(mat: np.ndarray, tol=1e-12) -> tuple:
    dim = mat.shape[1]
    b = np.random.rand(dim)

    for n in range(1, ITERS):
        b_old = 1.0*b
        anx = mat @ b
        b = anx / np.linalg.norm(anx)

        if np.linalg.norm(b-b_old) < tol:
            break
    else:
        raise ArithmeticError

    eig_value = np.mean(anx / b)
    eig_vector = b

    return eig_value, eig_vector


def main():
    grid = linspace(-5, 5, 100)
    grid_size = grid.shape[0]
    dx = grid[1]-grid[0]
    dx2 = dx*dx
    
    # make test matrix
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size) - 1.0/(abs(grid)+1.),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    
    # use scipy to calculate the largest eigenvalue
    # and corresponding vector
    eigs, evecs = sla.eigsh(H0, k=1, which='LA')
    
    # use your power method to calculate the same
    # start_time = time.perf_counter()
    # l, vec = largest_eigs(H0)
    # print(time.perf_counter() - start_time)

    # Merely some optimization testing
    # It turned out that the Numba version is about as fast as traditional Python
    start_time2 = time.perf_counter()
    l, vec = largest_eigs_opt(H0)
    print(time.perf_counter() - start_time2)

    # start_time3 = time.perf_counter()
    # l, vec = largest_eigs_opt(H0)
    # print(time.perf_counter() - start_time3)
    
    # see how they compare
    print('largest_eig estimate: ', l)
    print('scipy eigsh estimate: ', eigs)
    
    psi0 = evecs[:, 0]
    norm_const = simps(abs(psi0)**2, x=grid)
    psi0 = psi0/norm_const
    
    psi0_ = vec     # [:, 0]
    norm_const = simps(abs(psi0_)**2, x=grid)
    psi0_ = psi0_ / norm_const
    
    plot(grid, abs(psi0)**2, label='scipy eig. vector squared')
    plot(grid, abs(psi0_)**2, 'r--', label='largest_eig vector squared')
    legend(loc=0)
    show()


if __name__ == "__main__":
    main()
