"""Problem 3: electron density along a line"""

import time
import typing as tp

import matplotlib.pyplot as plt
import numpy as np

# from exercise_4 import read_xsf_example

# Custom xsf reader (a bit faster)
from exercise_4 import xsf
from exercise_4 import spline_class

# Custom spliner (faster by orders of magnitude, approximately two)
from exercise_4 import spline_fast


def vector_between_points(r0: np.ndarray, r1: np.ndarray, points: int) -> np.ndarray:
    """Linspace between two points in n-dimensional space"""
    if r0.ndim != r1.ndim:
        raise ValueError
    elif points < 1:
        raise ValueError

    # This is a rather hacky approach, but hey, it's a one-liner!
    # The outer product creates a matrix by multiplying two vectors
    # (compare this to inner product aka. dot product that for vectors results in a scalar)
    # To this matrix of the difference vector points we need to add the starting point, and np.newaxis is used to make
    # the dimensions match
    ret = np.transpose(np.outer(r1 - r0, np.linspace(0, 1, points)) + r0[:, np.newaxis])

    if not np.all(np.isclose(ret[0, :], r0)):
        raise ValueError(ret[0, :], r0)
    elif not np.all(np.isclose(ret[-1, :], r1)):
        raise ValueError(ret[-1, :], r1)

    return ret


def diff_lattice(lattice: np.ndarray, grid: np.ndarray) -> np.ndarray:
    """Get lattice vectors scaled by the grid size"""
    vecs = np.zeros(lattice.shape)
    for i in range(vecs.shape[0]):
        # The -1 comes from the fact that there are n-1 cells in each direction
        vecs[i, :] = lattice[i, :] / (grid[i] - 1)
    return vecs


# def grid_axes(lattice: np.ndarray, grid: np.ndarray) -> tp.List[np.ndarray]:
#     diff_vecs = diff_lattice(lattice, grid)
#     axes = []
#     for i in range(diff_vecs.ndim):
#         axes.append(np.outer(diff_vecs[i, :], np.linspace(0, 1, grid[i])))
#     return axes


def to_diff_lattice(vec: np.ndarray, lattice: np.ndarray, grid: np.ndarray) -> np.ndarray:
    """Convert a vector into the diff lattice grid coordinates"""
    # Special thanks to Alpi and Tuomo for pointing out the necessity of this transpose
    lvecs = diff_lattice(lattice, grid).transpose()
    inv_lvecs = np.linalg.inv(lvecs)

    res = np.zeros(vec.shape)
    for i in range(vec.shape[0]):
        res[i, :] = inv_lvecs @ vec[i, :]

        # Handle values that are outside the grid
        for j in range(vec.shape[1]):
            if res[i, j] < 0 or res[i, j] > grid[j]:
                # res[i, j] = res[i, j] % grid[j]
                res[i, j] = np.mod(res[i, j], grid[j])

    # Ensure that the result is in the grid
    # if np.min(res) < 0:
    #     raise Exception
    # elif np.max(res) > np.max(grid):
    #     raise Exception

    return res


def lattice_grid(grid: np.ndarray) -> tp.List[np.ndarray]:
    """Get the vectors with the lengths of the axes

    :param grid: array of grid sizes (n_x, n_y, n_z, ...)
    :return: vectors of length n_i
    """
    res = []
    for i in range(grid.shape[0]):
        res.append(np.arange(0, grid[i]))

    for i in range(grid.shape[0]):
        if res[i].size != grid[i]:
            raise Exception

    return res


def problem_3():
    path = "dft_chargedensity1.xsf"
    rho, lattice, grid, shift = xsf.read(path)

    # This line is from a vanadium atom in the middle of an edge to another vanadium atom in the middle of the
    # opposite edge
    r0 = np.array([0.1, 0.1, 2.8528])
    r1 = np.array([4.45, 4.45, 2.8528])
    points = 500
    # r = np.array([
    #     np.linspace(r0[0], r1[0], points),
    #     np.linspace(r0[1], r1[1], points),
    #     np.linspace(r0[2], r1[2], points)
    # ])
    # print(r)
    r = vector_between_points(r0, r1, points)
    print("r vector")
    print(r)
    r_lattice = to_diff_lattice(r, lattice, grid)
    print("r vector in the diff lattice coordinates")
    print(r_lattice)

    print("Axis vectors for lattice grid")
    lgrid = lattice_grid(grid)
    for vec in lgrid:
        print(vec.shape)
        print(vec)

    print("Preparing spline")
    print("This will take a while...")
    # The creation of the spline takes a long time, even on an overclocked i7
    # spline = spline_class.Spline(f=rho, dims=3, x=lgrid[0], y=lgrid[1], z=lgrid[2])

    # But we can speed it up
    spline = spline_fast.FastSpline3D(
        f=rho,
        x=lgrid[0].astype(np.float_), y=lgrid[1].astype(np.float_), z=lgrid[2].astype(np.float_)
    )

    print("Computing values")
    # Note to self: be careful in which r vector to plot!
    # (r_lattice instead of plain r)
    val = eval_spline3d(r_lattice, spline)
    print("Computing ready")

    plt.plot(val)
    plt.title("Problem 3")
    plt.show()


def eval_spline3d(vec: np.ndarray, spline: tp.Union[spline_class.Spline, spline_fast.FastSpline3D]) -> np.ndarray:
    val = np.zeros(vec.shape[0])
    for i in range(val.size):
        val[i] = spline.eval3d(
            np.array([vec[i, 0]], dtype=np.float_),
            np.array([vec[i, 1]], dtype=np.float_),
            np.array([vec[i, 2]], dtype=np.float_))
    return val


def problem_4():
    path = "dft_chargedensity2.xsf"
    rho, lattice, grid, shift = xsf.read(path)

    # This line is from an oxygen atom to another
    r0_first = np.array([-1.4466, 1.3073, 3.2115])
    r1_first = np.array([1.4361, 3.1883, 1.3542])

    # This line probably starts from a vanadium atom (one of those in the "center" of the unit cell, goes near another
    # vanadium atom (but not straight through, since that would result in a sharp dip), and ends
    # in the center of a third vanadium atom
    r0_sec = np.array([2.9996, 2.1733, 2.1462])
    r1_sec = np.array([8.7516, 2.1733, 2.1462])

    points = 500
    r_first = vector_between_points(r0_first, r1_first, points)
    r_sec = vector_between_points(r0_sec, r1_sec, points)

    r_first_lattice = to_diff_lattice(r_first, lattice, grid)
    r_sec_lattice = to_diff_lattice(r_sec, lattice, grid)

    lgrid = lattice_grid(grid)

    print("Preparing spline")
    start_time = time.perf_counter()
    # spline = spline_class.Spline(f=rho, dims=3, x=lgrid[0], y=lgrid[1], z=lgrid[2])
    spline = spline_fast.FastSpline3D(f=rho, x=lgrid[0].astype(np.float_), y=lgrid[1].astype(np.float_), z=lgrid[2].astype(np.float_))
    print(time.perf_counter() - start_time)

    print("Computing values")
    start_time = time.perf_counter()
    val_first = eval_spline3d(r_first_lattice, spline)
    val_sec = eval_spline3d(r_sec_lattice, spline)
    print(time.perf_counter() - start_time)

    plt.figure()
    plt.plot(val_first)
    plt.title("Problem 4 first")

    plt.figure()
    plt.plot(val_sec)
    plt.title("Problem 4 second")

    plt.show()


if __name__ == "__main__":
    problem_3()
    problem_4()
