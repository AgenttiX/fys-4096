"""A bit of messing around with PyQtGraph and OpenGL for problems 3 and 4"""

import matplotlib.cm
import matplotlib.colors
import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl

from exercise_4 import xsf

# import pyqtgraph.examples
# pyqtgraph.examples.run()


def convert_data(data: np.ndarray) -> np.ndarray:
    """Convert a 3D Numpy array to a format that PyQtGraph understands"""
    normalizer = matplotlib.colors.Normalize(vmin=0, vmax=np.max(data))
    cmap = matplotlib.cm.ScalarMappable(cmap="inferno", norm=normalizer)

    res = []
    for i in range(data.shape[0]):
        data_slice = data[i, :, :]
        res.append(cmap.to_rgba(data_slice))

    res = np.array(res)
    res[:, :, :, 3] = data / data.max()

    res *= 255
    res = res.astype(np.ubyte)

    if res.shape[:3] != data.shape[:3]:
        raise Exception

    return res


def show_xsf(path: str):
    """Display the electron density of an xsf file"""
    rho, lattice, grid, shift = xsf.read(path)

    app = pg.mkQApp()
    w = gl.GLViewWidget()
    w.opts["distance"] = 200
    w.show()
    w.setWindowTitle("Problem 3")

    data = convert_data(rho)

    v = gl.GLVolumeItem(data)
    v.translate(-data.shape[0] / 2, -data.shape[1] / 2, -data.shape[2] / 2)
    w.addItem(v)

    app.exec_()


def plot_3():
    path = "dft_chargedensity1.xsf"
    show_xsf(path)


def plot_4():
    path = "dft_chargedensity2.xsf"
    show_xsf(path)


if __name__ == "__main__":
    plot_3()
    plot_4()
