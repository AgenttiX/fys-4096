"""Problem 2: number of electrons"""

import numpy as np
from exercise_4 import read_xsf_example


def reciprocal_vecs(real_vectors: np.ndarray) -> np.ndarray:
    """Given the real space lattice vectors, compute the reciprocal ones"""
    # Note that np.invert() isn't related to matrix inversion
    a_inv = np.linalg.inv(real_vectors)
    return np.transpose(2*np.pi*a_inv)


def analyse(path: str):
    print("Analysing:", path)
    rho, lattice, grid, shift = read_xsf_example.read_example_xsf_density(path)

    # print(type(rho), type(lattice), type(grid), type(shift))
    # print(rho.shape)
    # print(lattice.shape)
    # print(grid.shape)
    # print(shift)
    if np.any(np.array(rho.shape) != grid):
        raise ValueError()

    volume = np.abs(np.linalg.det(lattice))
    print("Volume:", volume)

    diff_lattice = np.zeros(lattice.shape)
    for i in range(diff_lattice.shape[0]):
        # The -1 comes from the fact that there are n-1 cells in each direction
        diff_lattice[i, :] = lattice[i, :] / (grid[i]-1)
    print("Diff lattice vectors")
    print(diff_lattice)

    # Absolute value of the determinant of a matrix formed by vectors gives the volume enclosed by the vectors
    # (this also applies in higher dimensions)
    diff_lattice_volume = np.abs(np.linalg.det(diff_lattice))

    total_electrons = np.sum(rho[:-1, :-1, :-1]) * diff_lattice_volume
    print("Total electrons:", total_electrons)

    rvecs = reciprocal_vecs(lattice)
    print("Reciprocal lattice vectors")
    print(rvecs)
    print()


def main():
    path_1 = "dft_chargedensity1.xsf"
    path_2 = "dft_chargedensity2.xsf"

    analyse(path_1)
    analyse(path_2)


if __name__ == "__main__":
    main()
