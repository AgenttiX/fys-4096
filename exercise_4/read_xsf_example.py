import typing as tp
import numpy as np


# Adding the typing hint required changing the Numpy import to "import numpy as np", since
# there is a method called "any" in Numpy, and this overwrote the Python's standard "any"
# in the scope
# (this was when I added the tuple as tp.Tuple[any, any, any, any] as I wasn't yet sure on the types it contains
def read_example_xsf_density(filename: str) -> tp.Tuple[np.ndarray, np.ndarray, np.ndarray, tp.List[float]]:
    lattice = []
    density = []
    grid = []       # This isn't used, as it's later overridden with grid = np.array()...
    shift = []
    i = 0
    start_reading = False
    with open(filename, 'r') as f:
        for line in f:
            if "END_DATAGRID_3D" in line:
                start_reading = False
            if start_reading and i == 1:
                # The first row after "DATAGRID_3D_UNKNOWN" contains the matrix shape
                grid = np.array(line.split(), dtype=int)
            if start_reading and i == 2:
                shift.append(np.array(line.split(), dtype=float))
            if start_reading and i == 3:
                lattice.append(np.array(line.split(), dtype=float))
            if start_reading and i == 4:
                lattice.append(np.array(line.split(), dtype=float))
            if start_reading and i == 5:
                lattice.append(np.array(line.split(), dtype=float))
            if start_reading and i > 5:            
                density.extend(np.array(line.split(), dtype=float))
            if start_reading and i > 0:
                i = i+1
            if "DATAGRID_3D_UNKNOWN" in line:
                start_reading = True
                i = 1
    
    rho = np.zeros((grid[0], grid[1], grid[2]))
    ii = 0
    for k in range(grid[2]):
        for j in range(grid[1]):        
            for i in range(grid[0]):
                rho[i, j, k]=density[ii]
                ii += 1

    # convert density to 1/Angstrom**3 from 1/Bohr**3
    a0 = 0.52917721067
    a03 = a0*a0*a0
    rho /= a03
    return rho, np.array(lattice), grid, shift


def main():
    filename = 'dft_chargedensity1.xsf'
    rho, lattice, grid, shift = read_example_xsf_density(filename)
    print(rho.shape)
    print(lattice)
    print(grid)
    print(shift)


if __name__ == "__main__":
    main()
