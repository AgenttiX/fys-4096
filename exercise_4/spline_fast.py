"""
FAST VERSIONS
Using these the problems 3 and 4 can be computed in ~ 10 s each

-----

Cubic hermite splines in 1d, 2d, and 3d

Intentionally unfinished :)

Related to FYS-4096 Computational Physics
exercise 2-4 assignments.

By Ilkka Kylanpaa on January 2019
"""

import typing as tp

import numba
import numpy as np
import matplotlib.pyplot as plt


@numba.njit(parallel=True)
def p1(t):
    return (1. + 2. * t) * (t - 1.) ** 2


@numba.njit(parallel=True)
def p2(t):
    return t ** 2 * (3. - 2. * t)


@numba.njit(parallel=True)
def q1(t):
    return t * (t - 1.) ** 2


@numba.njit(parallel=True)
def q2(t):
    return t ** 2 * (t - 1.)


@numba.njit(parallel=True)
def init_1d_spline(x, f, h):
    # now using complete boundary conditions
    # - natural boundary conditions commented
    a = np.zeros((len(x),))
    b = np.zeros((len(x),))
    c = np.zeros((len(x),))
    d = np.zeros((len(x),))
    fx = np.zeros((len(x),))

    # a[0] = 1.0 # not needed
    b[0] = 1.0

    # natural boundary conditions
    # c[0] = 0.5
    # d[0] = 1.5*(f[1]-f[0])/(x[1]-x[0])

    # complete boundary conditions
    c[0] = 0.0
    d[0] = (f[1] - f[0]) / (x[1] - x[0])

    for i in range(1, len(x) - 1):
        d[i] = 6.0 * (h[i] / h[i - 1] - h[i - 1] / h[i]) * f[i] - 6.0 * h[i] / h[i - 1] * f[i - 1] + 6.0 * h[i - 1] / h[
            i] * f[i + 1]
        a[i] = 2.0 * h[i]
        b[i] = 4.0 * (h[i] + h[i - 1])
        c[i] = 2.0 * h[i - 1]
        # end for

    b[-1] = 1.0
    # c[-1] = 1.0 # not needed

    # natural boundary conditions
    # a[-1] = 0.5
    # d[-1] = 1.5*(f[-1]-f[-2])/(x[-1]-x[-2])

    # complete boundary conditions
    a[-1] = 0.0
    d[-1] = (f[-1] - f[-2]) / (x[-1] - x[-2])

    # solve tridiagonal eq. A*f = d
    c[0] = c[0] / b[0]
    d[0] = d[0] / b[0]
    for i in range(1, len(x) - 1):
        temp = b[i] - c[i - 1] * a[i]
        c[i] = c[i] / temp
        d[i] = (d[i] - d[i - 1] * a[i]) / temp
    # end for

    fx[-1] = d[-1]
    for i in range(len(x) - 2, -1, -1):
        fx[i] = d[i] - c[i] * fx[i + 1]
    # end for

    return fx


""" 
Add smoothing functions 

def smooth1d(x,f,factor=3):
    ...
    ...
    return ...

def smooth2d(x,y,f,factor=3):
    ...
    ...
    return ... 

def smooth3d(x,y,z,f,factor=3):
    ...
    ...
    ...
    return ...
"""


@numba.njit(parallel=True)
def _eval1d_opt(x, self_x, self_f, self_fx, self_hx) -> np.ndarray:
    N = len(self_x) - 1
    f = np.zeros((len(x),))
    ii = 0
    for val in x:
        # i = np.floor(np.where(self_x <= val)[0][-1]).astype(np.int_)
        i = int(np.floor(np.where(self_x <= val)[0][-1]))
        if i == N:
            f[ii] = self_f[i]
        else:
            t = (val - self_x[i]) / self_hx[i]
            f[ii] = self_f[i] * p1(t) + self_f[i + 1] * p2(t) + self_hx[i] * (
                    self_fx[i] * q1(t) + self_fx[i + 1] * q2(t))
        ii += 1

    return f


@numba.njit(parallel=True)
def _eval2d_opt(x, y, self_x, self_y, self_f, self_fx, self_fy, self_fxy, self_hx, self_hy) -> np.ndarray:
    Nx = len(self_x) - 1
    Ny = len(self_y) - 1
    f = np.zeros((len(x), len(y)))
    A = np.zeros((4, 4))
    ii = 0
    for valx in x:
        # i = np.floor(np.where(self_x <= valx)[0][-1]).astype(int)
        i = int(np.floor(np.where(self_x <= valx)[0][-1]))
        if i == Nx:
            i -= 1
        jj = 0
        for valy in y:
            # j = np.floor(np.where(self_y <= valy)[0][-1]).astype(int)
            j = int(np.floor(np.where(self_y <= valy)[0][-1]))
            if j == Ny:
                j -= 1
            u = (valx - self_x[i]) / self_hx[i]
            v = (valy - self_y[j]) / self_hy[j]
            pu = np.array([p1(u), p2(u), self_hx[i] * q1(u), self_hx[i] * q2(u)])
            pv = np.array([p1(v), p2(v), self_hy[j] * q1(v), self_hy[j] * q2(v)])
            A[0, :] = np.array([self_f[i, j], self_f[i, j + 1], self_fy[i, j], self_fy[i, j + 1]])
            A[1, :] = np.array([self_f[i + 1, j], self_f[i + 1, j + 1], self_fy[i + 1, j], self_fy[i + 1, j + 1]])
            A[2, :] = np.array([self_fx[i, j], self_fx[i, j + 1], self_fxy[i, j], self_fxy[i, j + 1]])
            A[3, :] = np.array([self_fx[i + 1, j], self_fx[i + 1, j + 1], self_fxy[i + 1, j], self_fxy[i + 1, j + 1]])

            f[ii, jj] = np.dot(pu, np.dot(A, pv))
            jj += 1
        ii += 1
    return f


@numba.njit(parallel=True)
def _eval3d_opt(
        x, y, z,
        self_x, self_y, self_z,
        self_f,
        self_fx, self_fy, self_fz,
        self_hx, self_hy, self_hz,
        self_fxy, self_fxz, self_fyz,
        self_fxyz) -> np.ndarray:
    Nx = len(self_x) - 1
    Ny = len(self_y) - 1
    Nz = len(self_z) - 1
    f = np.zeros((len(x), len(y), len(z)))
    A = np.zeros((4, 4))
    B = np.zeros((4, 4))
    ii = 0
    for valx in x:
        # i = np.floor(np.where(self_x <= valx)[0][-1]).astype(int)
        i = int(np.floor(np.where(self_x <= valx)[0][-1]))
        if i == Nx:
            i -= 1
        jj = 0
        for valy in y:
            # j = np.floor(np.where(self_y <= valy)[0][-1]).astype(int)
            j = int(np.floor(np.where(self_y <= valy)[0][-1]))
            if j == Ny:
                j -= 1
            kk = 0
            for valz in z:
                # k = np.floor(np.where(self_z <= valz)[0][-1]).astype(int)
                k = int(np.floor(np.where(self_z <= valz)[0][-1]))
                if k == Nz:
                    k -= 1
                u = (valx - self_x[i]) / self_hx[i]
                v = (valy - self_y[j]) / self_hy[j]
                t = (valz - self_z[k]) / self_hz[k]
                pu = np.array([p1(u), p2(u), self_hx[i] * q1(u), self_hx[i] * q2(u)])
                pv = np.array([p1(v), p2(v), self_hy[j] * q1(v), self_hy[j] * q2(v)])
                pt = np.array([p1(t), p2(t), self_hz[k] * q1(t), self_hz[k] * q2(t)])
                B[0, :] = np.array([self_f[i, j, k], self_f[i, j, k + 1], self_fz[i, j, k], self_fz[i, j, k + 1]])
                B[1, :] = np.array(
                    [self_f[i + 1, j, k], self_f[i + 1, j, k + 1], self_fz[i + 1, j, k], self_fz[i + 1, j, k + 1]])
                B[2, :] = np.array([self_fx[i, j, k], self_fx[i, j, k + 1], self_fxz[i, j, k], self_fxz[i, j, k + 1]])
                B[3, :] = np.array(
                    [self_fx[i + 1, j, k], self_fx[i + 1, j, k + 1], self_fxz[i + 1, j, k], self_fxz[i + 1, j, k + 1]])
                A[:, 0] = np.dot(B, pt)
                B[0, :] = np.array(
                    [self_f[i, j + 1, k], self_f[i, j + 1, k + 1], self_fz[i, j + 1, k], self_fz[i, j + 1, k + 1]])
                B[1, :] = np.array([self_f[i + 1, j + 1, k], self_f[i + 1, j + 1, k + 1], self_fz[i + 1, j + 1, k],
                                    self_fz[i + 1, j + 1, k + 1]])
                B[2, :] = np.array(
                    [self_fx[i, j + 1, k], self_fx[i, j + 1, k + 1], self_fxz[i, j + 1, k], self_fxz[i, j + 1, k + 1]])
                B[3, :] = np.array([self_fx[i + 1, j + 1, k], self_fx[i + 1, j + 1, k + 1], self_fxz[i + 1, j + 1, k],
                                    self_fxz[i + 1, j + 1, k + 1]])
                A[:, 1] = np.dot(B, pt)

                B[0, :] = np.array([self_fy[i, j, k], self_fy[i, j, k + 1], self_fyz[i, j, k], self_fyz[i, j, k + 1]])
                B[1, :] = np.array(
                    [self_fy[i + 1, j, k], self_fy[i + 1, j, k + 1], self_fyz[i + 1, j, k], self_fyz[i + 1, j, k + 1]])
                B[2, :] = np.array(
                    [self_fxy[i, j, k], self_fxy[i, j, k + 1], self_fxyz[i, j, k], self_fxyz[i, j, k + 1]])
                B[3, :] = np.array([self_fxy[i + 1, j, k], self_fxy[i + 1, j, k + 1], self_fxyz[i + 1, j, k],
                                    self_fxyz[i + 1, j, k + 1]])
                A[:, 2] = np.dot(B, pt)
                B[0, :] = np.array(
                    [self_fy[i, j + 1, k], self_fy[i, j + 1, k + 1], self_fyz[i, j + 1, k], self_fyz[i, j + 1, k + 1]])
                B[1, :] = np.array([self_fy[i + 1, j + 1, k], self_fy[i + 1, j + 1, k + 1], self_fyz[i + 1, j + 1, k],
                                    self_fyz[i + 1, j + 1, k + 1]])
                B[2, :] = np.array([self_fxy[i, j + 1, k], self_fxy[i, j + 1, k + 1], self_fxyz[i, j + 1, k],
                                    self_fxyz[i, j + 1, k + 1]])
                B[3, :] = np.array(
                    [self_fxy[i + 1, j + 1, k], self_fxy[i + 1, j + 1, k + 1], self_fxyz[i + 1, j + 1, k],
                     self_fxyz[i + 1, j + 1, k + 1]])
                A[:, 3] = np.dot(B, pt)

                f[ii, jj, kk] = np.dot(pu, np.dot(A, pv))
                kk += 1
            jj += 1
        ii += 1
    return f


spec3d = [
    ("x", numba.float64[:]),
    ("y", numba.float64[:]),
    ("z", numba.float64[:]),
    ("f", numba.float64[:, :, :]),
    ("hx", numba.float64[:]),
    ("hy", numba.float64[:]),
    ("hz", numba.float64[:]),
    ("fx", numba.float64[:, :, :]),
    ("fy", numba.float64[:, :, :]),
    ("fz", numba.float64[:, :, :]),
    ("fxy", numba.float64[:, :, :]),
    ("fxz", numba.float64[:, :, :]),
    ("fyz", numba.float64[:, :, :]),
    ("fxyz", numba.float64[:, :, :]),
]


# https://numba.pydata.org/numba-doc/dev/user/jitclass.html
@numba.jitclass(spec3d)
class FastSpline3D:
    def __init__(self, f: np.ndarray, x: np.ndarray, y: np.ndarray, z: np.ndarray):
        """An entirely compiled class for fast 3D splines"""
        self.x = x
        self.y = y
        self.z = z
        self.f = f
        self.hx = np.diff(x)
        self.hy = np.diff(y)
        self.hz = np.diff(z)
        self.fx = np.zeros(f.shape)
        self.fy = np.zeros(f.shape)
        self.fz = np.zeros(f.shape)
        self.fxy = np.zeros(f.shape)
        self.fxz = np.zeros(f.shape)
        self.fyz = np.zeros(f.shape)
        self.fxyz = np.zeros(f.shape)

        max_i = np.array([x.size, y.size, z.size]).max()

        for i in range(max_i):
            for j in range(max_i):
                if i < len(y) and j < len(z):
                    self.fx[:, i, j] = init_1d_spline(x, self.f[:, i, j], self.hx)
                if i < len(x) and j < len(z):
                    self.fy[i, :, j] = init_1d_spline(y, self.f[i, :, j], self.hy)
                if i < len(x) and j < len(y):
                    self.fz[i, j, :] = init_1d_spline(z, self.f[i, j, :], self.hz)

        for i in range(max_i):
            for j in range(max_i):
                if i < len(y) and j < len(z):
                    self.fxy[:, i, j] = init_1d_spline(x, self.fy[:, i, j], self.hx)
                if i < len(y) and j < len(z):
                    self.fxz[:, i, j] = init_1d_spline(x, self.fz[:, i, j], self.hx)
                if i < len(x) and j < len(z):
                    self.fyz[i, :, j] = init_1d_spline(y, self.fz[i, :, j], self.hy)

        for i in range(len(y)):
            for j in range(len(z)):
                self.fxyz[:, i, j] = init_1d_spline(x, self.fyz[:, i, j], self.hx)
        # end for

    def eval1d(self, x: np.ndarray) -> np.ndarray:
        N = len(self.x) - 1
        f = np.zeros((len(x),))
        ii = 0
        for val in x:
            # i = np.floor(np.where(self.x <= val)[0][-1]).astype(np.int_)
            i = int(np.floor(np.where(self.x <= val)[0][-1]))
            if i == N:
                f[ii] = self.f[i]
            else:
                t = (val - self.x[i]) / self.hx[i]
                f[ii] = self.f[i] * p1(t) + self.f[i + 1] * p2(t) + self.hx[i] * (
                        self.fx[i] * q1(t) + self.fx[i + 1] * q2(t))
            ii += 1

        return f

    def eval2d(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        Nx = len(self.x) - 1
        Ny = len(self.y) - 1
        f = np.zeros((len(x), len(y)))
        A = np.zeros((4, 4))
        ii = 0
        for valx in x:
            # i = np.floor(np.where(self.x <= valx)[0][-1]).astype(int)
            i = int(np.floor(np.where(self.x <= valx)[0][-1]))
            if i == Nx:
                i -= 1
            jj = 0
            for valy in y:
                # j = np.floor(np.where(self.y <= valy)[0][-1]).astype(int)
                j = int(np.floor(np.where(self.y <= valy)[0][-1]))
                if j == Ny:
                    j -= 1
                u = (valx - self.x[i]) / self.hx[i]
                v = (valy - self.y[j]) / self.hy[j]
                pu = np.array([p1(u), p2(u), self.hx[i] * q1(u), self.hx[i] * q2(u)])
                pv = np.array([p1(v), p2(v), self.hy[j] * q1(v), self.hy[j] * q2(v)])
                A[0, :] = np.array([self.f[i, j], self.f[i, j + 1], self.fy[i, j], self.fy[i, j + 1]])
                A[1, :] = np.array([self.f[i + 1, j], self.f[i + 1, j + 1], self.fy[i + 1, j], self.fy[i + 1, j + 1]])
                A[2, :] = np.array([self.fx[i, j], self.fx[i, j + 1], self.fxy[i, j], self.fxy[i, j + 1]])
                A[3, :] = np.array(
                    [self.fx[i + 1, j], self.fx[i + 1, j + 1], self.fxy[i + 1, j], self.fxy[i + 1, j + 1]])

                f[ii, jj] = np.dot(pu, np.dot(A, pv))
                jj += 1
            ii += 1
        return f

    def eval3d(self, x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
        Nx = len(self.x) - 1
        Ny = len(self.y) - 1
        Nz = len(self.z) - 1
        f = np.zeros((x.size, y.size, z.size))
        A = np.zeros((4, 4))
        B = np.zeros((4, 4))
        ii = 0
        for valx in x:
            # i = np.floor(np.where(self.x <= valx)[0][-1]).astype(int)
            i = int(np.floor(np.where(self.x <= valx)[0][-1]))
            if i == Nx:
                i -= 1
            jj = 0
            for valy in y:
                # j = np.floor(np.where(self.y <= valy)[0][-1]).astype(int)
                j = int(np.floor(np.where(self.y <= valy)[0][-1]))
                if j == Ny:
                    j -= 1
                kk = 0
                for valz in z:
                    # k = np.floor(np.where(self.z <= valz)[0][-1]).astype(int)
                    k = int(np.floor(np.where(self.z <= valz)[0][-1]))
                    if k == Nz:
                        k -= 1
                    u = (valx - self.x[i]) / self.hx[i]
                    v = (valy - self.y[j]) / self.hy[j]
                    t = (valz - self.z[k]) / self.hz[k]
                    pu = np.array([p1(u), p2(u), self.hx[i] * q1(u), self.hx[i] * q2(u)])
                    pv = np.array([p1(v), p2(v), self.hy[j] * q1(v), self.hy[j] * q2(v)])
                    pt = np.array([p1(t), p2(t), self.hz[k] * q1(t), self.hz[k] * q2(t)])
                    B[0, :] = np.array([self.f[i, j, k], self.f[i, j, k + 1], self.fz[i, j, k], self.fz[i, j, k + 1]])
                    B[1, :] = np.array(
                        [self.f[i + 1, j, k], self.f[i + 1, j, k + 1], self.fz[i + 1, j, k], self.fz[i + 1, j, k + 1]])
                    B[2, :] = np.array(
                        [self.fx[i, j, k], self.fx[i, j, k + 1], self.fxz[i, j, k], self.fxz[i, j, k + 1]])
                    B[3, :] = np.array(
                        [self.fx[i + 1, j, k], self.fx[i + 1, j, k + 1], self.fxz[i + 1, j, k],
                         self.fxz[i + 1, j, k + 1]])
                    A[:, 0] = np.dot(B, pt)
                    B[0, :] = np.array(
                        [self.f[i, j + 1, k], self.f[i, j + 1, k + 1], self.fz[i, j + 1, k], self.fz[i, j + 1, k + 1]])
                    B[1, :] = np.array([self.f[i + 1, j + 1, k], self.f[i + 1, j + 1, k + 1], self.fz[i + 1, j + 1, k],
                                        self.fz[i + 1, j + 1, k + 1]])
                    B[2, :] = np.array(
                        [self.fx[i, j + 1, k], self.fx[i, j + 1, k + 1], self.fxz[i, j + 1, k],
                         self.fxz[i, j + 1, k + 1]])
                    B[3, :] = np.array(
                        [self.fx[i + 1, j + 1, k], self.fx[i + 1, j + 1, k + 1], self.fxz[i + 1, j + 1, k],
                         self.fxz[i + 1, j + 1, k + 1]])
                    A[:, 1] = np.dot(B, pt)

                    B[0, :] = np.array(
                        [self.fy[i, j, k], self.fy[i, j, k + 1], self.fyz[i, j, k], self.fyz[i, j, k + 1]])
                    B[1, :] = np.array(
                        [self.fy[i + 1, j, k], self.fy[i + 1, j, k + 1], self.fyz[i + 1, j, k],
                         self.fyz[i + 1, j, k + 1]])
                    B[2, :] = np.array(
                        [self.fxy[i, j, k], self.fxy[i, j, k + 1], self.fxyz[i, j, k], self.fxyz[i, j, k + 1]])
                    B[3, :] = np.array([self.fxy[i + 1, j, k], self.fxy[i + 1, j, k + 1], self.fxyz[i + 1, j, k],
                                        self.fxyz[i + 1, j, k + 1]])
                    A[:, 2] = np.dot(B, pt)
                    B[0, :] = np.array(
                        [self.fy[i, j + 1, k], self.fy[i, j + 1, k + 1], self.fyz[i, j + 1, k],
                         self.fyz[i, j + 1, k + 1]])
                    B[1, :] = np.array(
                        [self.fy[i + 1, j + 1, k], self.fy[i + 1, j + 1, k + 1], self.fyz[i + 1, j + 1, k],
                         self.fyz[i + 1, j + 1, k + 1]])
                    B[2, :] = np.array([self.fxy[i, j + 1, k], self.fxy[i, j + 1, k + 1], self.fxyz[i, j + 1, k],
                                        self.fxyz[i, j + 1, k + 1]])
                    B[3, :] = np.array(
                        [self.fxy[i + 1, j + 1, k], self.fxy[i + 1, j + 1, k + 1], self.fxyz[i + 1, j + 1, k],
                         self.fxyz[i + 1, j + 1, k + 1]])
                    A[:, 3] = np.dot(B, pt)

                    f[ii, jj, kk] = np.dot(pu, np.dot(A, pv))
                    kk += 1
                jj += 1
            ii += 1
        return f


class Spline:
    def __init__(self, dims: int, f: np.ndarray, x: np.ndarray, **kwargs):
        """A partially compiled class for spline interpolation

        The evaluation itself is fast, but constructing the splining is slow, since the constructor has for
        loops in Python
        """
        self.dims = dims
        if self.dims == 1:
            self.x = x
            self.f = f
            self.hx = np.diff(self.x)
            self.fx = init_1d_spline(self.x, self.f, self.hx)
        elif self.dims == 2:
            self.x = x
            self.y = kwargs['y']
            self.f = f
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
            self.fx = np.zeros(np.shape(self.f))
            self.fy = np.zeros(np.shape(self.f))
            self.fxy = np.zeros(np.shape(self.f))
            for i in range(max([len(self.x), len(self.y)])):
                if i < len(self.y):
                    self.fx[:, i] = init_1d_spline(self.x, self.f[:, i], self.hx)
                if i < len(self.x):
                    self.fy[i, :] = init_1d_spline(self.y, self.f[i, :], self.hy)
            # end for
            for i in range(len(self.y)):
                self.fxy[:, i] = init_1d_spline(self.x, self.fy[:, i], self.hx)
            # end for
        elif self.dims == 3:
            self.x = x
            self.y = kwargs['y']
            self.z = kwargs['z']
            self.f = f
            self.hx = np.diff(self.x)
            self.hy = np.diff(self.y)
            self.hz = np.diff(self.z)
            self.fx = np.zeros(np.shape(self.f))
            self.fy = np.zeros(np.shape(self.f))
            self.fz = np.zeros(np.shape(self.f))
            self.fxy = np.zeros(np.shape(self.f))
            self.fxz = np.zeros(np.shape(self.f))
            self.fyz = np.zeros(np.shape(self.f))
            self.fxyz = np.zeros(np.shape(self.f))
            for i in range(max([len(self.x), len(self.y), len(self.z)])):
                for j in range(max([len(self.x), len(self.y), len(self.z)])):
                    if i < len(self.y) and j < len(self.z):
                        self.fx[:, i, j] = init_1d_spline(self.x, self.f[:, i, j], self.hx)
                    if i < len(self.x) and j < len(self.z):
                        self.fy[i, :, j] = init_1d_spline(self.y, self.f[i, :, j], self.hy)
                    if i < len(self.x) and j < len(self.y):
                        self.fz[i, j, :] = init_1d_spline(self.z, self.f[i, j, :], self.hz)
            # end for
            for i in range(max([len(self.x), len(self.y), len(self.z)])):
                for j in range(max([len(self.x), len(self.y), len(self.z)])):
                    if i < len(self.y) and j < len(self.z):
                        self.fxy[:, i, j] = init_1d_spline(self.x, self.fy[:, i, j], self.hx)
                    if i < len(self.y) and j < len(self.z):
                        self.fxz[:, i, j] = init_1d_spline(self.x, self.fz[:, i, j], self.hx)
                    if i < len(self.x) and j < len(self.z):
                        self.fyz[i, :, j] = init_1d_spline(self.y, self.fz[i, :, j], self.hy)
            # end for
            for i in range(len(self.y)):
                for j in range(len(self.z)):
                    self.fxyz[:, i, j] = init_1d_spline(self.x, self.fyz[:, i, j], self.hx)
            # end for
        else:
            print('Either dims is missing or specific dims is not available')
        # end if

    def eval1d(self, x: tp.Union[int, float, np.ndarray]) -> np.ndarray:
        if np.isscalar(x):
            x = np.array([x])
        return _eval1d_opt(
            x, self_x=self.x, self_f=self.f, self_fx=self.fx, self_hx=self.hx
        )

    def eval2d(self, x: tp.Union[int, float, np.ndarray], y: tp.Union[int, float, np.ndarray]) -> np.ndarray:
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        return _eval2d_opt(
            x, y,
            self_x=self.x, self_y=self.y,
            self_f=self.f, self_fx=self.fx, self_fy=self.fy, self_fxy=self.fxy,
            self_hx=self.hx, self_hy=self.hy
        )

    def eval3d(
            self,
            x: tp.Union[int, float, np.ndarray],
            y: tp.Union[int, float, np.ndarray],
            z: tp.Union[int, float, np.ndarray]) -> np.ndarray:
        if np.isscalar(x):
            x = np.array([x])
        if np.isscalar(y):
            y = np.array([y])
        if np.isscalar(z):
            z = np.array([z])
        return _eval3d_opt(
            x, y, z,
            self_x=self.x, self_y=self.y, self_z=self.z,
            self_f=self.f,
            self_fx=self.fx, self_fy=self.fy, self_fz=self.fz,
            self_hx=self.hx, self_hy=self.hy, self_hz=self.hz,
            self_fxy=self.fxy, self_fxz=self.fxz, self_fyz=self.fyz,
            self_fxyz=self.fxyz
        )


def main():
    # 1d example
    x = np.linspace(0., 2. * np.pi, 20)
    y = np.sin(x)
    spl1d = Spline(x=x, f=y, dims=1)
    xx = np.linspace(0., 2. * np.pi, 100)
    plt.figure()
    # function
    plt.plot(xx, spl1d.eval1d(xx))
    plt.plot(x, y, 'o', xx, np.sin(xx), 'r--')
    plt.title('function')

    # 2d example
    fig = plt.figure()
    ax = fig.add_subplot(121)
    x = np.linspace(0.0, 3.0, 11)
    y = np.linspace(0.0, 3.0, 11)
    X, Y = np.meshgrid(x, y)
    Z = (X + Y) * np.exp(-1.0 * (X * X + Y * Y))
    ax.pcolor(X, Y, Z)
    ax.set_title('original')

    spl2d = Spline(x=x, y=y, f=Z, dims=2)
    # plt.figure()
    ax2 = fig.add_subplot(122)
    x = np.linspace(0.0, 3.0, 51)
    y = np.linspace(0.0, 3.0, 51)
    X, Y = np.meshgrid(x, y)
    Z = spl2d.eval2d(x, y)
    ax2.pcolor(X, Y, Z)
    ax2.set_title('interpolated')

    # 3d example
    x = np.linspace(0.0, 3.0, 10)
    y = np.linspace(0.0, 3.0, 10)
    z = np.linspace(0.0, 3.0, 10)
    X, Y, Z = np.meshgrid(x, y, z)
    F = (X + Y + Z) * np.exp(-1.0 * (X * X + Y * Y + Z * Z))
    X, Y = np.meshgrid(x, y)
    fig3d = plt.figure()
    ax = fig3d.add_subplot(121)
    ax.pcolor(X, Y, F[..., int(len(z) / 2)])
    ax.set_title('original')

    spl3d = Spline(x=x, y=y, z=z, f=F, dims=3)
    x = np.linspace(0.0, 3.0, 50)
    y = np.linspace(0.0, 3.0, 50)
    z = np.linspace(0.0, 3.0, 50)
    X, Y = np.meshgrid(x, y)
    ax2 = fig3d.add_subplot(122)
    F = spl3d.eval3d(x, y, z)
    ax2.pcolor(X, Y, F[..., int(len(z) / 2)])
    ax2.set_title('interpolated')

    plt.show()


if __name__ == "__main__":
    main()
