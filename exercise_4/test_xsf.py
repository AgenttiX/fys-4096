import timeit

import numpy as np

import xsf
import read_xsf_example


def load_own_1():
    xsf.read("dft_chargedensity1.xsf")


def load_ilkka_1():
    read_xsf_example.read_example_xsf_density("dft_chargedensity1.xsf")


def test_xsf():
    """Compare the xsf reading methods"""

    path1 = "dft_chargedensity1.xsf"
    path2 = "dft_chargedensity2.xsf"

    own1 = xsf.read(path1)
    own2 = xsf.read(path2)

    ilkka1 = read_xsf_example.read_example_xsf_density(path1)
    ilkka2 = read_xsf_example.read_example_xsf_density(path2)

    # print(own1[0].shape, ilkka1[0].shape)
    # print(own1[0])
    # print()
    # print("---")
    # print()
    # print(ilkka1[0])

    print(own1[1])
    print(ilkka1[1])

    print(own1[2])
    print(ilkka1[2])

    print(own1[3])
    print(ilkka1[3])

    # Seems close enough to zero
    print(np.mean(own1[0] - ilkka1[0]))
    print(np.mean(own2[0] - ilkka2[0]))


def test_perf():
    """Test the performance of the xsf reading methods"""

    count = 100
    print(timeit.timeit("load_own_1()", setup="from __main__ import load_own_1", number=count))
    print(timeit.timeit("load_ilkka_1()", setup="from __main__ import load_ilkka_1", number=count))

    # Results for 100 iterations
    # Own: 25,5 s
    # Course code: 134,9 s


if __name__ == "__main__":
    test_xsf()
    # test_perf()
