import numpy as np
import pandas.io.parsers


def read(path: str):
    """A faster reader for xsf files"""
    primvec_start = 0
    primvec_end = 0
    # primcoord_start = 0
    # primcoord_end = 0

    lattice = []
    grid = None
    datagrid_start = 0
    datagrid_end = None
    shift = None

    with open(path, "r") as file:
        for i, line in enumerate(file):
            stripped: str = line.strip()

            # Having a multi-level if-else reduces the total amount of comparisons that have to be made per row
            if datagrid_start == 0:
                if stripped == "PRIMVEC":
                    primvec_start = i
                elif stripped == "PRIMCOORD":
                    primvec_end = i-1
                    # primcoord_start = i
                # elif stripped == "BEGIN_BLOCK_DATAGRID_3D":
                #     primcoord_end = i - 1
                elif stripped == "DATAGRID_3D_UNKNOWN":
                    datagrid_start = i
                elif primvec_start != 0 and primvec_end == 0:
                    lattice.append(stripped.split())
            else:
                if i == datagrid_start + 1:
                    grid = np.array(stripped.split(), dtype=np.int_)
                elif i == datagrid_start + 2:
                    shift = np.array(stripped.split(), dtype=np.float_)
                # elif i < datagrid_start + 6:
                    # shift.append(stripped.split())
                elif stripped == "END_DATAGRID_3D":
                    datagrid_end = i-1

    lattice = np.array(lattice, dtype=np.float_)
    shift = np.array(shift, dtype=np.float_)

    datagrid_data_start = datagrid_start + 6

    # Using Pandas enables us to do the row processing in C instead of Python
    rho: np.ndarray = pandas.io.parsers.read_csv(
        path,
        skiprows=datagrid_data_start,
        nrows=datagrid_end - datagrid_data_start + 1,
        dtype=np.float_,
        header=None,
        skipinitialspace=True,
        delimiter=" ",
        engine="c"
    ).values

    # Numpy operations are faster than for loops
    point_count = grid.prod()
    rho = rho.flatten()[:point_count]   # Flatten the array and strip the nonexisting points from the end
    rho = rho.reshape(grid, order="F")

    # convert density to 1/Angstrom**3 from 1/Bohr**3
    a0 = 0.52917721067
    a03 = a0**3
    rho /= a03

    return rho, lattice, grid, shift


def __test():
    rho, lattice, grid, shift = read("dft_chargedensity1.xsf")
    print(rho.shape)

    read("dft_chargedensity2.xsf")


if __name__ == "__main__":
    __test()
