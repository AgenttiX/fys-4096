"""Problem 4: Two capacitor plates"""


# import os
# os.environ["NUMBA_WARNINGS"] = "1"

import matplotlib.figure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numba
import numpy as np


# I'm again spamming Numba everywhere for learning purposes
@numba.njit
def find_nearest_ind(array: np.ndarray, value) -> int:
    # https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
    return (np.abs(array - value)).argmin()


@numba.njit
def jacobi_update(phi: np.ndarray, skipper: callable = (lambda i, j: False)) -> np.ndarray:
    """Reusing code from the previous problem with added skipper

    The default skipper skips nothing
    """
    phi_new = phi.copy()
    for i in range(1, phi.shape[0]-1):
        for j in range(1, phi.shape[1]-1):
            if not skipper(i, j):
                phi_new[i, j] = 0.25*(phi[i+1, j] + phi[i-1, j] + phi[i, j+1] + phi[i, j-1])

    return phi_new


@numba.njit
def solver(phi: np.ndarray, updater: callable, skipper: callable, tol: float = 1e-6, max_iter: int = 100000):
    """Reusing code from the previous problem with added skipper"""
    conv_i = max_iter
    for i in range(max_iter):
        phi_old = phi
        phi = updater(phi, skipper)
        if np.mean(np.abs(phi - phi_old)) < tol:
            # print("Converged after", i)
            conv_i = i
            break
    else:
        pass
        # print("Did not converge")
    return phi, conv_i


def main():
    l_max = 1
    step = 0.1

    plate_dist = 0.3
    plate_len = 1
    plate_potential_left = -1
    plate_potential_right = 1

    x = y = np.arange(-l_max, l_max, step)
    x_mesh, y_mesh = np.meshgrid(x, y)

    plate_x_left_ind = find_nearest_ind(x, -plate_dist)
    plate_x_right_ind = find_nearest_ind(x, plate_dist)
    plate_y_min_ind = find_nearest_ind(y, -plate_len/2)
    plate_y_max_ind = find_nearest_ind(y, plate_len/2)

    phi0 = np.zeros((x.size, y.size))

    # Set plate potentials
    # (I presume that the potential is negative for the blue plate and positive for the red)
    phi0[plate_x_left_ind, plate_y_min_ind:plate_y_max_ind+1] = plate_potential_left
    phi0[plate_x_right_ind, plate_y_min_ind:plate_y_max_ind+1] = plate_potential_right

    @numba.njit
    def is_plate(i: int, j: int) -> bool:
        if (i == plate_x_left_ind or i == plate_x_right_ind) and plate_y_min_ind < j < plate_y_max_ind:
            return True
        return False

    # I learned from Alpi (the guy sitting next to me on the computer lab sessions)
    # that _ is a good way to capture unused return values.
    # https://stackoverflow.com/questions/431866/ignore-python-multiple-return-value/431871#431871
    phi, _ = solver(phi0, updater=jacobi_update, skipper=is_plate)

    gradient = np.gradient(phi)

    # Plotting
    fig1: matplotlib.figure.Figure = plt.figure()

    ax1 = fig1.add_subplot(121, projection="3d")
    ax1.plot_wireframe(x_mesh, y_mesh, phi0, rstride=1, cstride=1)
    ax1.set_title("Initial")

    ax2 = fig1.add_subplot(122, projection="3d")
    ax2.plot_wireframe(x_mesh, y_mesh, phi, rstride=1, cstride=1)
    ax2.set_title("Jacobi")

    fig2: matplotlib.figure.Figure = plt.figure()
    plt.plot([-plate_dist, -plate_dist], [-plate_len/2, plate_len/2], "b")
    plt.plot([plate_dist, plate_dist], [-plate_len/2, plate_len/2], "r")

    # The indexing is different for np.gradient and plt.quiver
    plt.quiver(y_mesh, x_mesh, -1*gradient[0], -1*gradient[1])

    plt.show()


if __name__ == "__main__":
    main()
