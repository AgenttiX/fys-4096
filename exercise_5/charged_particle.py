"""Problem 2: trajectory of a charged particle influenced by both electric and magnetic field"""

import matplotlib.figure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numba
import numpy as np

import runge_kutta


@numba.njit
def e_field(r, t):
    """Electric field

    Takes the time parameter as well so that it can be made time-dependent
    """
    return 0.05 * np.array([1, 0, 0])


@numba.njit
def b_field(r, t):
    """Magnetic field density

    Takes the time parameter as well so that it can be made time-dependent
    """
    return 4 * np.array([0, 1, 0])


# @numba.njit
def particle(x: np.ndarray, t: float, qm: float, E: callable, B: callable):
    r = x[:3]
    v = x[3:]

    # Acceleration
    acc = qm * (E(r, t) + np.cross(v, B(r, t)))

    return np.concatenate((v, acc))


def main():
    # t0 = 0.0
    v0 = np.array([0.1, 0.1, 0.1])
    r0 = np.array([0.0, 0.0, 0.0])
    qm = 1
    t = np.linspace(0, 5, 100)
    dt = t[1] - t[0]

    qm = 1
    sol = np.zeros((t.size, 6))
    # a = np.zeros((t.size, 3))

    x = np.concatenate((r0, v0))
    for i in range(t.size):
        sol[i, :] = x
        x, tp = runge_kutta.runge_kutta4(x, t[i], dt, particle, args=(qm, e_field, b_field))

    r_sol = sol[:, :3]
    v_sol = sol[:, 3:]

    print("Coordinates")
    print(r_sol[-1, :])
    print("Distance (abs(r))")
    print(np.linalg.norm(r_sol[-1, :]))
    print("Velocity")
    print(v_sol[-1, :])
    print("Speed")
    print(np.linalg.norm(v_sol[-1, :]))

    # For some reason PyCharm cannot determine the type of the output of plt.figure()
    # without the explicit type definition
    fig: matplotlib.figure.Figure = plt.figure()
    fig.suptitle("Problem 2: charged particle")

    ax1 = fig.add_subplot(121, projection="3d")
    ax1.plot(r_sol[:, 0], r_sol[:, 1], r_sol[:, 2])
    ax1.set_title("position")

    ax2 = fig.add_subplot(122, projection="3d")
    ax2.plot(v_sol[:, 0], v_sol[:, 1], v_sol[:, 2])
    ax2.set_title("velocity")

    plt.show()


if __name__ == "__main__":
    main()
