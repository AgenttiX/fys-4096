"""Problem 3: Poisson equation and relaxation methods"""

import os
os.environ["NUMBA_WARNINGS"] = "1"

import matplotlib.figure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numba
import numpy as np


# Sorry for spamming Numba everywhere. I'm merely testing where it works and where it doesn't.
@numba.njit
def jacobi_update(phi: np.ndarray) -> np.ndarray:
    phi_new = phi.copy()
    for i in range(1, phi.shape[0]-1):
        for j in range(1, phi.shape[1]-1):
            phi_new[i, j] = 0.25*(phi[i+1, j] + phi[i-1, j] + phi[i, j+1] + phi[i, j-1])    # + rho

    return phi_new


@numba.njit
def gauss_seidel_update(phi: np.ndarray) -> np.ndarray:
    phi_new = phi.copy()
    for i in range(1, phi.shape[0]-1):
        for j in range(1, phi.shape[1]-1):
            phi_new[i, j] = 0.25*(phi[i+1, j] + phi_new[i-1, j] + phi[i, j+1] + phi_new[i, j-1])

    return phi_new


@numba.njit
def sor_update(phi: np.ndarray, omega: float = 1.8) -> np.ndarray:
    phi_new = phi.copy()
    for i in range(1, phi.shape[0]-1):
        for j in range(1, phi.shape[1]-1):
            phi_new[i, j] = (1-omega)*phi[i, j] + omega/4*(phi[i+1, j] + phi_new[i-1, j] + phi[i, j+1] + phi_new[9, j-1])

    return phi_new


@numba.njit
def solver(phi: np.ndarray, updater: callable, tol: float = 1e-6, max_iter: int = 100000):
    conv_i = max_iter
    for i in range(max_iter):
        phi_old = phi
        phi = updater(phi)
        if np.mean(np.abs(phi - phi_old)) < tol:
            # print("Converged after", i)
            conv_i = i
            break
    else:
        pass
        # print("Did not converge")
    return phi, conv_i


def main():
    # Phi is NxN
    omega = 1.8

    l_max = 1
    size = 10
    x = y = np.linspace(0, l_max, size)
    x_mesh, y_mesh = np.meshgrid(x, y)
    h = l_max / size
    phi0 = np.zeros((size, size))
    phi0[:, -1] = np.ones(size)

    phi_jacobi, i = solver(phi0, updater=jacobi_update)
    phi_gs, i = solver(phi0, updater=gauss_seidel_update)
    phi_sor, i = solver(phi0, updater=sor_update)

    # Plotting
    fig1: matplotlib.figure.Figure = plt.figure()

    ax1 = fig1.add_subplot(221, projection="3d")
    ax1.plot_wireframe(x_mesh, y_mesh, phi0, rstride=1, cstride=1)
    ax1.set_title("Initial")

    ax2 = fig1.add_subplot(222, projection="3d")
    ax2.plot_wireframe(x_mesh, y_mesh, phi_jacobi, rstride=1, cstride=1)
    ax2.set_title("Jacobi")

    ax3 = fig1.add_subplot(223, projection="3d")
    ax3.plot_wireframe(x_mesh, y_mesh, phi_gs, rstride=1, cstride=1)
    ax3.set_title("Gauss-Seidel")

    ax4 = fig1.add_subplot(224, projection="3d")
    ax4.plot_wireframe(x_mesh, y_mesh, phi_sor, rstride=1, cstride=1)
    ax4.set_title("SOR")

    # Convergence analysis
    tols = np.logspace(-2, -12, num=30)
    # print(tols)
    conv_jacobi = np.zeros_like(tols)
    conv_gs = np.zeros_like(tols)
    conv_sor = np.zeros_like(tols)

    for i in range(tols.size):
        rho, conv_jacobi[i] = solver(phi0, jacobi_update, tol=tols[i])
        rho, conv_gs[i] = solver(phi0, gauss_seidel_update, tol=tols[i])
        rho, conv_sor[i] = solver(phi0, sor_update, tol=tols[i])

    fig2: matplotlib.figure.Figure = plt.figure()

    tols_axis = np.log10(tols)
    plt.plot(tols_axis, conv_jacobi, label="Jacobi")
    plt.plot(tols_axis, conv_gs, label="Gauss-Seidel")
    plt.plot(tols_axis, conv_sor, label="SOR")
    plt.legend()
    plt.title("Convergence properties")
    plt.xlabel("Tolerance (10^-x)")
    plt.ylabel("Iterations required for convergence")

    plt.show()


if __name__ == "__main__":
    main()
