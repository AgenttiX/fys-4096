"""Problem 1"""

import typing as tp

import numpy as np
# Please note that star-importing pyplot includes "import numpy as np", and therefore
# the Numpy import above is overwritten.
# In general it would be a better practice to have "import matplotlib.pyplot as plt"
# from matplotlib.pyplot import *
import matplotlib.pyplot as plt

from scipy.integrate import odeint
from scipy.integrate import solve_ivp


def runge_kutta4(
        x: float,
        t: float,
        dt: float,
        func: callable,
        args: tp.Union[tuple, list] = ()) -> tp.Tuple[float, float]:
    """
    Fourth order Runge-Kutta for solving ODEs
    dx/dt = f(x,t)

    x = state vector at time t
    t = time
    dt = time step

    func = function on the right-hand side,
    i.e., dx/dt = func(x,t;params)
    
    kwargs = possible parameters for the function
             given as args=(a,b,c,...)

    Need to complete the routine below
    where it reads '...' !!!!!!!
    
    See FYS-4096 lecture notes.
    """
    # F1 = F2 = F3 = F4 = 0.0*x
    # if 'args' in kwargs:
    #     args = kwargs['args']
    #     F1 = func(x, t, *args)
    #     F2 = ...
    #     F3 = ...
    #     F4 = ...
    # else:
    #     F1 = func(x,t)
    #     F2 = ...
    #     F3 = ...
    #     F4 = ...

    f1 = func(x, t, *args)
    f2 = func(x + dt/2*f1, t + dt/2, *args)
    f3 = func(x + dt/2*f2, t + dt/2, *args)
    f4 = func(x + dt*f3, t + dt, *args)

    return x + dt/6*(f1 + 2*f2 + 2*f3 + f4), t+dt


def pend(y: tp.List[float], t: float, b: float, c: float) -> np.ndarray:
    """The equation of motion for a pendulum"""
    theta, omega = y
    # The angle changes by the angular velocity, and the angular velocity changes
    # according to the loss term b and the gravity term c
    dydt = [omega, -b*omega - c*np.sin(theta)]
    return np.array(dydt)


def pend_ivp(t: float, y: tp.List[float]):
    """A pendulum function in the format that scipy.integrate.solve_ivp understands"""
    b = 0.25
    c = 5.0
    return pend(y, t, b, c)


# The functions below are mostly initialization and plotting

def odeint_test(ax):
    """Solve the motion of a pendulum using scipy.integrate.odeint"""
    b = 0.25
    c = 5.0
    y0 = [np.pi - 0.1, 0.0]
    t = np.linspace(0, 10, 101)
    sol = odeint(pend, y0, t, args=(b, c))
    ax.plot(t, sol[:, 0], 'b', label='theta(t)')
    ax.plot(t, sol[:, 1], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()


def runge_kutta_test(ax):
    """Solve the motion of a pendulum using the Runge-Kutta coded above"""
    b = 0.25
    c = 5.0    
    y0 = [np.pi - 0.1, 0.0]
    t = np.linspace(0, 10, 101)
    dt = t[1]-t[0]  # timestep
    sol = []
    x = 1.0*np.array(y0)
    for i in range(len(t)):
        sol.append(x)
        # Solve the movement of the pendulum using a predetermined time and the previous position
        x, tp = runge_kutta4(x, t[i], dt, pend, args=(b, c))
    sol = np.array(sol)
    ax.plot(t, sol[:, 0], 'b', label='theta(t)')
    ax.plot(t, sol[:, 1], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()


def solve_ivp_test(ax):
    """Solve the motion of a pendulum using scipy.integrate.solve_ivp"""
    b = 0.25
    c = 5.0
    y0 = [np.pi - 0.1, 0.0]
    t = np.linspace(0, 10, 101)
    sol = solve_ivp(pend_ivp, (0, 10), y0, t_eval=t)
    ax.plot(sol.t, sol.y[0, :], 'b', label='theta(t)')
    ax.plot(sol.t, sol.y[1, :], 'g', label='omega(t)')
    ax.legend(loc='best')
    ax.set_xlabel('t')
    ax.grid()


def main():
    """Compute the motion of a pendulum using three different methods and plot the solutions"""
    fig = plt.figure()
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132)
    ax3 = fig.add_subplot(133)
    solve_ivp_test(ax1)
    ax1.set_title('solve_ivp')
    odeint_test(ax2)
    ax2.set_title('odeint')
    runge_kutta_test(ax3)
    ax3.set_title('own Runge-Kutta 4')
    plt.show()
    

if __name__ == "__main__":
    main()
