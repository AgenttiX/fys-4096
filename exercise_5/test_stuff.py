import numpy as np


# Leap frog method
# (used in molecular dynamics etc.)

def v(v_prev2, dt, f_prev, m):
    return v_prev2 + 2*dt/m*f_prev


def r(r_prev2, v_prev):
    return r_prev2*0    # TODO


def num_der2(f: callable, x, h):
    return (f(x+h) + f(x-h) - 2*f(x))/(h**2)


def verlet(r_prev, r_prev2, f_prev, dt, m):
    return 2*r_prev - r_prev2 + dt**2/m*f_prev


def rk4(x: float, t: float, dt: float, f: callable):
    """Fourth-order Runge-Kutta"""
    f1 = f(x, t)
    f2 = f(x + dt/2*f1, t+dt/2)
    f3 = f(x + dt/2*f2, t+dt/2)
    f4 = f(x+dt*f3, t+dt)

    return x + dt/6 * (f1 + 2*f2 + 2*f3 + f4)
