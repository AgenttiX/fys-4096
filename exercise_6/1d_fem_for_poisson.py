"""Exercise 6 problem 2: 1D FEM for Poisson equation"""


import matplotlib.pyplot as plt
import numba
import numpy as np

from exercise_6 import num_calculus


@numba.njit
def phi_solution(x):
    return np.sin(np.pi * x)


@numba.njit
def rho(x, eps0=1):
    return eps0 * np.pi * np.sin(np.pi * x)


def a_mat(h: float, ndim: int):
    diag_vec = np.repeat(2/h, ndim)
    offset_vec = np.repeat(-1/h, ndim-1)

    diag_vec[[0, -1]] = 1
    offset_vec[[0, -1]] = 0

    return np.diag(diag_vec) + np.diag(offset_vec, k=1) + np.diag(offset_vec, k=-1)


@numba.njit
def b_vec(x: np.ndarray, h: float):
    b = np.zeros(x.shape)

    for i in range(1, x.size-1):
        b[i] = np.pi/h * (x[i-1] + x[i+1] - 2*x[i])*np.cos(np.pi*x[i]) + 1/h*(2*np.sin(np.pi*x[i]) - np.sin(np.pi*x[i-1]) - np.sin(np.pi*x[i+1]))

    return b

# def hat_func(x: float, xi: float, h: float) -> float:
#     if xi-h < x <= xi:
#         return (x - xi + h) / h
#     elif xi < x < xi+h:
#         return (xi + h - x) / h
#     else:
#         return 0


@numba.njit
def hat_func(x: np.ndarray, i: float, x_vec: np.ndarray, h: float) -> np.ndarray:
    ret = np.zeros(x.size)

    for j in range(x.size):
        if x_vec[i-1] < x[j] < x_vec[i+1]:
            if x[j] < x_vec[i]:
                ret[j] = (x[j] - x_vec[i-1]) / h
            else:
                ret[j] = (x_vec[i+1] - x[j]) / h
        else:
            ret[j] = 0

    return ret


def a_mat_int(x_vec: np.ndarray):
    a = np.zeros((x_vec.size, x_vec.size))
    h = x_vec[1] - x_vec[0]

    # The multiplier for the size is important, otherwise the result oscillates
    x_vec_int = np.linspace(x_vec[0], x_vec[-1], x_vec.size * 10)

    der_h = 0.01*h

    # The code below works but is horribly inefficient due to the amount
    # of function calls and for loops:)

    for i in range(1, a.shape[0]-1):
        for j in range(1, a.shape[1]-1):
            def u_i(x):
                return hat_func(x, i=i, x_vec=x_vec, h=h)

            def u_j(x):
                return hat_func(x, i=j, x_vec=x_vec, h=h)

            def u_i_der(x):
                return num_calculus.eval_derivative(u_i, x, der_h)

            def u_j_der(x):
                return num_calculus.eval_derivative(u_j, x, der_h)

            def integrable(x):
                return u_i_der(x) * u_j_der(x)

            a[i, j] = num_calculus.simpson_integration(x_vec_int, integrable)

    a[0, 0] = 1
    a[-1, -1] = 1
    return a


def b_vec_int(x_vec: np.ndarray, eps0: float = 1):
    b = np.zeros(x_vec.shape)
    h = x_vec[1] - x_vec[0]

    for i in range(b.size):
        def w_i(x):
            return hat_func(x, i=i, x_vec=x_vec, h=h)

        def integrable(x):
            return w_i(x) * rho(x)

        b[i] = num_calculus.simpson_integration(x_vec, integrable)

    return 1/eps0 * b


def main():
    h = 0.01
    x = np.arange(0, 1+h, h)
    # print(x)

    a = a_mat(h, x.size)
    # print(a)
    b = b_vec(x, h)
    # print(b)

    sol = np.linalg.solve(a, b)
    # sol = b @ np.linalg.inv(a)
    # print(sol)

    a_int = a_mat_int(x)
    # print(a_int.round(2))
    b_int = b_vec_int(x)

    # sol_int = np.linalg.lstsq(a_int, b_int)
    sol_int = b @ np.linalg.inv(a_int)

    plt.plot(x, phi_solution(x), label="Solution")
    plt.plot(x, sol, label="Solved")
    plt.plot(x, sol_int, label="Solved (with custom u-funcs)")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()


# Note: hattufunktion voi myöhemmin korvata polynomilla tmv. :)
