"""Problem 3 part 1: Poisson equation"""

from fenics import *

# Create mesh and define function space
# 8x8 rectangles on the unit square [0, 1] x [0, 1]
mesh = UnitSquareMesh(8, 8)
# P = standard Lagrange family of elements. Equivalent to "Lagrange"
# P1 = triangle with nodes at the vertices
V = FunctionSpace(mesh, 'P', 1)

# Define boundary condition
# The condition is given in C++ syntax as it will later be included to some C++ code and then compiled
u_D = Expression('1 + x[0]*x[0] + 2*x[1]*x[1]', degree=2)


def boundary(x, on_boundary):
    """Where the boundary condition should be applied"""
    return on_boundary


bc = DirichletBC(V, u_D, boundary)


# Define variational problem
# Trial and test functions have the common space V
u = TrialFunction(V)
v = TestFunction(V)
f = Constant(-6.0)
a = dot(grad(u), grad(v))*dx
L = f*v*dx

# Compute solution
u = Function(V)
solve(a == L, u, bc)

# Plot solution
u.rename('u', 'solution')
plot(u)
plot(mesh)

# Save solution to file in VTK format
vtkfile = File('poisson.pvd')
vtkfile << u

# Compute error in L2 norm
error_L2 = errornorm(u_D, u, 'L2')

# Compute maximum error at vertices
vertex_values_u_D = u_D.compute_vertex_values(mesh)
vertex_values_u = u.compute_vertex_values(mesh)
import numpy as np
error_max = np.max(np.abs(vertex_values_u_D - vertex_values_u))

# Print errors
print('error_L2  =', error_L2)
print('error_max =', error_max)

# Hold plot
# interactive() has been removed from FEniCS
# interactive()

import matplotlib.pyplot as plt

# print(type(u))

plt.show()

# plot(u, interactive=True)
