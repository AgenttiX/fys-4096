import h5py
import matplotlib.pyplot as plt
import numpy as np


def main():
    f0 = h5py.File("s0.hdf5")
    f3 = h5py.File("s3.hdf5")

    x = np.array(f0["grid"])

    s0_label = "$S = 0$"
    s3_label = "$S = 3$"

    plt.figure()
    plt.plot(x, f0["Vhartree"], label=s0_label)
    plt.plot(x, f3["Vhartree"], label=s3_label)
    plt.legend()
    plt.title("$V_{Hartree}$")

    plt.figure()
    plt.plot(x, f0["density"], label=s0_label)
    plt.plot(x, f3["density"], label=s3_label)
    plt.legend()
    plt.title("Electron density")

    plt.figure()
    f0_orbs = np.array(f0["orbitals"])
    for i in range(f0_orbs.shape[0]):
        plt.plot(x, f0_orbs[i, :], label=i)
    plt.legend()
    plt.title("$S=0$ orbitals")

    plt.figure()
    f3_orbs = np.array(f3["orbitals"])
    for i in range(f3_orbs.shape[0]):
        plt.plot(x, f3_orbs[i, :], label=i)
    plt.legend()
    plt.title("$S=3$ orbitals")

    # print("S=0 iterations", f0.attrs["iterations"])
    # print("S=3 iterations", f3.attrs["iterations"])

    plt.show()

    # NOTES
    # For all values below, threshold = 1e-8

    # -----
    # With interaction
    # -----

    # S = 0
    # 9 iterations
    # Total energy      18.313695475870492
    # Kinetic energy    3.594711076337028
    # Potential energy  14.718984399533463
    #
    # Density integral  5.999997845849101

    # S = 3
    # 9 iterations
    # Total energy      26.057931718650877
    # Kinetic energy    8.432507255195237
    # Potential energy  17.62542446345564
    #
    # Density integral  5.999940370153629

    # -----
    # Without interaction (ee_coef = [0, 1]) only a single iteration is required
    # -----

    # S = 0
    # Total energy      8.994906747160657
    # Kinetic energy    4.502047882854594
    # Potential energy  4.492858864306063
    #
    # Density integral  5.999999692844874

    # S = 3
    # Total energy      18.01605610784922
    # Kinetic energy    9.199299330428484
    # Potential energy  8.816756777420736
    #
    # Density integral  5.999971960833587


if __name__ == "__main__":
    main()
