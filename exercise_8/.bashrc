# .bashrc
#
# Runs every time a new bash instance is run

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Default CSC settings for bash users.
. /homeappl/appl_common/profile/bash_profile.default
. /homeappl/appl_common/profile/bash_profile.optional

# User specific aliases and functions

# Added for FYS-4096
export PATH="/homeappl/home/student058/appl_taito/qe-6.3/bin:$PATH"
export PATH="/homeappl/home/student058/appl_taito/qmcpack/nexus/bin:$PATH"

export PYTHONPATH="/homeappl/home/student058/appl_taito/qmcpack/nexus/lib:$PYTHONPATH"
