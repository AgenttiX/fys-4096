#! /usr/bin/env python

from nexus import settings, run_project
from nexus import generate_physical_system
from nexus import generate_pwscf
from machine_configs import get_taito_configs

settings(
    pseudo_dir="./pseudopotentials",
    results="",
    status_only=0,
    generate_only=1,
    sleep=3,
    machine="taito"
)

jobs = get_taito_configs()

dia16 = generate_physical_system(
    units="A",
    axes=[[1.785, 1.785, 0.],
          [0., 1.785, 1.785],
          [1.785, 0., 1.785]],
    elem=["C", "C"],
    pos=[[0., 0., 0.],
         [0.8925, 0.8925, 0.8925]],
    tiling=(2, 2, 2),
    kgrid=(1, 1, 1),
    kshift=(0, 0, 0),
    C=4
)

scfs = []
cutoffs = [50, 100, 150, 200, 250, 300]

for i in range(len(cutoffs)):
    path = "scf-%d" % i

    scf = generate_pwscf(
        identifier="scf",
        path=path,
        job=jobs["scf"],
        input_type="generic",
        calculation="scf",
        input_dft="lda",
        # ecutwfc=200,
        ecutwfc=cutoffs[i],
        conv_thr=1e-8,
        nosym=True,
        wf_collect=True,
        system=dia16,
        # kgrid=(1, 1, 1),
        kgrid=(2, 2, 2),
        pseudos=["C.BFD.upf"],
    )

    scfs.append(scf)

run_project(scfs)
