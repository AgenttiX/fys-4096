#! /usr/bin/env python
# import os
from __future__ import print_function
from nexus import settings, job, run_project, obj
from nexus import generate_physical_system
from nexus import generate_pwscf
from machine_configs import get_taito_configs

print("Setting up custom workflow")

settings(
    pseudo_dir="./pseudopotentials",
    results="",
    # Enable this to see only a brief output of the status
    status_only=0,
    # Don't disable this before testing that the workflow works
    generate_only=1,
    sleep=3,
    machine="taito",
)

jobs = get_taito_configs()

cubic_box_size = [10.0]
x = 1.0 * cubic_box_size[0]
d_eq = 1.2074  # nuclei separation in Angstrom
d_sep = 0.1
point_count = 7
d_min = d_eq - (point_count-1)//2 * d_sep
scfs = []
d_vals = []

for i in range(point_count):
    d = d_min + i*d_sep
    d_vals.append(d)

    O2 = generate_physical_system(
        units="A",
        axes=[[x, 0.0, 0.0],
              [0., x, 0.0],
              [0., 0., x]],
        elem=["O", "O"],
        pos=[[x / 2 - d / 2, x / 2, x / 2],
             [x / 2 + d / 2, x / 2, x / 2]],
        net_spin=0,
        tiling=(1, 1, 1),
        kgrid=(1, 1, 1),  # scf kgrid given below to enable symmetries
        kshift=(0, 0, 0),
        O=6,
    )

    name = "scf-%d" % i

    scf = generate_pwscf(
        identifier="scf",
        path=name,
        job=jobs["scf"],
        input_type="generic",
        calculation="scf",
        input_dft="lda",
        ecutwfc=200,
        conv_thr=1e-8,
        nosym=True,
        wf_collect=True,
        system=O2,
        kgrid=(1, 1, 1),
        pseudos=["O.BFD.upf"],
    )
    scfs.append(scf)

print("d values")
print(d_vals)
print()

print("About to run this")
print(scfs)
print()

print("Now running")
print("------")
print()

run_project(scfs)
