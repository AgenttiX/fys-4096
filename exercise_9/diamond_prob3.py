#! /usr/bin/env python

"""
There appears to have been some bug here, since the execution was not completed in a meaningful time despite the time
limit having been set to 60 min

elapsed time 26707.9 s  memory 68.09 MB
elapsed time 26711.0 s  memory 68.09 MB
elapsed time 26714.2 s  memory 68.09 MB
elapsed time 26717.3 s  memory 68.09 MB
elapsed time 26720.5 s  memory 68.09 MB

After this, it appears that I can no longer get CPU time for the simulations even though I deleted the old jobs from
the queue
[student058@taito-login3 ~]$ squeue -u $USER -o '%.13i  %.25j  %.8T  %.10M  %.9l  %3C  %m'
        JOBID                       NAME     STATE        TIME  TIME_LIMI  CPU  MIN_MEMORY
     42029801                        scf   PENDING        0:00    1:00:00  24   2G

Apparently lowering the CPU count to 4 helped, and the job was executed successfully

[student058@taito-login3 vmc]$ qmca -e 5 -q ev *scalar.dat
                            LocalEnergy               Variance           ratio
vmc  series 0  -87.879806 +/- 0.056597   17.257757 +/- 1.260796   0.1964

From qmca --help
- e stands for equilibration length in blocks
- q stands for quantities to analyze. However, the quantity "ev" is not listed in the abbreviations.

The struct files are the same, since the output of scf is converted using conv and then passed on to vmc.
In other words, the same structure is first analyzed using scf and then vmc.
"""

from nexus import settings, job, run_project
from nexus import generate_physical_system
from nexus import generate_pwscf
from nexus import generate_pw2qmcpack
from nexus import generate_qmcpack, vmc
from machine_configs import get_taito_configs

settings(
    pseudo_dir='../pseudopotentials',
    status_only=0,
    generate_only=0,
    sleep=3,
    machine='taito'
)

jobs = get_taito_configs()

dia16 = generate_physical_system(
    units='A',
    axes=[[1.785, 1.785, 0.],
          [0., 1.785, 1.785],
          [1.785, 0., 1.785]],
    elem=['C', 'C'],
    pos=[[0., 0., 0.],
         [0.8925, 0.8925, 0.8925]],
    tiling=(2, 2, 2),
    kgrid=(1, 1, 1),
    kshift=(0, 0, 0),
    C=4
)

scf = generate_pwscf(
    identifier='scf',
    path='diamond/scf',
    # job=job(cores=16, app='pw.x'),
    job=jobs["scf_pw.x"],
    input_type='generic',
    calculation='scf',
    input_dft='lda',
    ecutwfc=200,
    conv_thr=1e-8,
    nosym=True,
    wf_collect=True,
    system=dia16,
    pseudos=['C.BFD.upf'],
)

conv = generate_pw2qmcpack(
    identifier='conv',
    path='diamond/scf',
    # job=job(cores=1, app='pw2qmcpack.x'),
    job=jobs["scf_pw2qmcpack.x"],
    write_psir=False,
    dependencies=(scf, 'orbitals'),
)

qmc = generate_qmcpack(
    identifier='vmc',
    path='diamond/vmc',
    # job=job(cores=16, threads=4, app='qmcpack'),
    job=jobs["scf_qmcpack"],
    input_type='basic',
    system=dia16,
    pseudos=['C.BFD.xml'],
    jastrows=[],
    calculations=[
        vmc(
            walkers=1,
            warmupsteps=20,
            blocks=200,
            steps=10,
            substeps=2,
            timestep=.4
        )
    ],
    dependencies=(conv, 'orbitals'),
)

run_project(scf, conv, qmc)
