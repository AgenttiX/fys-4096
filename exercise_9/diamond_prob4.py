#! /usr/bin/env python

"""
There appears to be some bug here, since the execution was not completed in a meaningful time despite the time
limit having been set to 60 min

elapsed time 25811.2 s  memory 68.17 MB
elapsed time 25814.4 s  memory 68.17 MB
elapsed time 25817.6 s  memory 68.17 MB
elapsed time 25820.7 s  memory 68.17 MB
elapsed time 25823.9 s  memory 68.17 MB

I lowered the CPU count for the first parts, and now they run properly, but the script gets stuck at opt,
which according to the exercise description requires more cores
[student058@taito-login3 ~]$ squeue -u $USER -o '%.13i  %.25j  %.8T  %.10M  %.9l  %3C  %m'
        JOBID                       NAME     STATE        TIME  TIME_LIMI  CPU  MIN_MEMORY
     42030268                        opt   PENDING        0:00    1:00:00  24   2G

"""

from nexus import settings, job, run_project
from nexus import generate_physical_system
from nexus import generate_pwscf
from nexus import generate_pw2qmcpack
from nexus import generate_qmcpack, vmc, dmc
from optim_params import *
from machine_configs import get_taito_configs

settings(
    pseudo_dir='../pseudopotentials',
    status_only=0,
    generate_only=0,
    sleep=3,
    machine='taito'
)

jobs = get_taito_configs()

dia16 = generate_physical_system(
    units='A',
    axes=[[1.785, 1.785, 0.],
          [0., 1.785, 1.785],
          [1.785, 0., 1.785]],
    elem=['C', 'C'],
    pos=[[0., 0., 0.],
         [0.8925, 0.8925, 0.8925]],
    tiling=(2, 2, 2),
    kgrid=(1, 1, 1),
    kshift=(0, 0, 0),
    C=4
)

scf = generate_pwscf(
    identifier='scf',
    path='diamond/scf',
    # job=job(cores=12, app='pw.x'),
    job=jobs["scf_pw.x"],
    input_type='generic',
    calculation='scf',
    input_dft='lda',
    ecutwfc=200,
    conv_thr=1e-8,
    nosym=True,
    wf_collect=True,
    system=dia16,
    pseudos=['C.BFD.upf'],
)

conv = generate_pw2qmcpack(
    identifier='conv',
    path='diamond/scf',
    # job=job(cores=1, app='pw2qmcpack.x'),
    job=jobs["scf_pw2qmcpack.x"],
    write_psir=False,
    dependencies=(scf, 'orbitals'),
)

qmc = generate_qmcpack(
    identifier='vmc',
    path='diamond/vmc',
    # job=job(cores=12, threads=2, app='qmcpack_cpu_comp_SoA'),
    job=jobs["scf_qmcpack"],
    input_type='basic',
    system=dia16,
    pseudos=['C.BFD.xml'],
    jastrows=[],
    calculations=[
        vmc(
            walkers=1,
            warmupsteps=20,
            blocks=200,
            steps=10,
            substeps=2,
            timestep=.4
        )
    ],
    dependencies=(conv, 'orbitals'),
)

optims = getOptims()

optJ2 = generate_qmcpack(
    path='diamond/optJ2',
    spin_polarized=False,
    identifier='opt',
    # job=job(cores=12, threads=2, app='qmcpack_cpu_comp_SoA'),
    job=jobs["scf_qmcpack_large"],
    pseudos=['C.BFD.xml'],
    system=dia16,
    input_type='basic',
    twistnum=0,
    corrections=[],
    jastrows=[('J1', 'bspline', 6),
              ('J2', 'bspline', 6)],
    calculations=optims,
    dependencies=(conv, 'orbitals')
)

dmc_run = generate_qmcpack(
    path='diamond/dmc',
    spin_polarized=False,
    identifier='qmc',
    # job=job(cores=12, threads=2, app='qmcpack_cpu_comp_SoA'),
    job=jobs["scf_qmcpack_large"],
    pseudos=['C.BFD.xml'],
    system=dia16,
    input_type='basic',
    estimators=[],
    corrections=[],
    jastrows=[],
    calculations=[
        vmc(
            timestep=0.3,
            warmupsteps=10,
            blocks=80,
            steps=5,
            substeps=3,
            # samplesperthread = 10,
            samples=2048,
        ),
        dmc(
            timestep=0.01,
            warmupsteps=10,
            blocks=80,
            steps=5,
            nonlocalmoves=True,
        ),
        dmc(
            timestep=0.005,
            warmupsteps=50,
            blocks=80,
            steps=5,
            nonlocalmoves=True,
        ),
    ],
    dependencies=[(conv, 'orbitals'),
                  (optJ2, 'jastrow')]
)

run_project(scf, conv, qmc, optJ2, dmc_run)
