#!/usr/bin/env bash

module purge
module load gcc
module load openmpi
module load openblas
module load hdf5-serial
module load mkl
module load python
