#! /usr/bin/env python

from __future__ import print_function
from nexus import job


def general_configs(machine):
    if machine == 'taito':
        jobs = get_taito_configs()
    else:
        print('Using taito as default machine')
        jobs = get_taito_configs()
    return jobs


def get_taito_configs():
    pw_presub = '''
    module purge
    module load gcc
    module load openmpi
    module load openblas
    module load hdf5-serial
    '''

    qmcpack_presub = """
    module purge
    module load gcc/5.4.0
    module load mkl/11.3.2
    module load intelmpi/5.1.3
    module load hdf5-par/1.8.18
    module load fftw/3.3.6
    module load boost/1.63
    module load cmake/3.9.0
    """

    scf_pw = job(cores=4, minutes=60, user_env=False, presub=pw_presub, app="pw.x")
    scf_qmcpack = job(cores=4, minutes=60, user_env=False, presub=qmcpack_presub, app="qmcpack_taito_cpu_comp_SoA")
    scf_qmcpack_large = job(nodes=1, threads=4, minutes=60, user_env=False, presub=qmcpack_presub, app="qmcpack_taito_cpu_comp_SoA")
    scf_pw2qmcpack = job(cores=1, minutes=60, user_env=False, presub=pw_presub, app="pw2qmcpack.x")

    # 24 processes (1 node = 24 processors at taito)
    # scf  = job(nodes=1,hours=1,user_env=False,presub=scf_presub,app=qe)

    jobs = {
        "scf_pw.x": scf_pw,
        "scf_qmcpack": scf_qmcpack,
        "scf_qmcpack_large": scf_qmcpack_large,
        "scf_pw2qmcpack.x": scf_pw2qmcpack
    }

    return jobs
