"""
Simple VMC and DMC code for FYS-4096 course at TUT

- Fill in more comments based on lecture slides
- Follow assignment instructions  


By Ilkka Kylanpaa


Exercise answers
- VMC is significantly faster, as less layers of loops are required
    - Also, I was able to JIT the VMC, but not the DMC
- The Jastrow factor used is the simplest Pade form
- Some optimization has already been implemented on top of the template code, but there is still room for more
    - The software could be implemented using a lower-level programming language (and such speedup has been attempted
    here using Numba)
    - The values for some functios such as the Jastrow factor could be precomputed and then splined etc. when needed
    - The computations could be parallelised
        - For example VMC could be computed so that the previous walker position is used only up to a certain
        amount of iterations, and then all future walkers would start from that position, enabling parallelism
- In VMC the wave function coefficients can be extracted from the walker parameters, and could probably be calculated
    by averaging the walker parameters over the latest iterations (latest 50 % for example?)
        - At least in this example case
"""

import os
import time
import typing as tp

# Show compilation warnings
os.environ["NUMBA_WARNINGS"] = "1"

# from numpy import *
# from matplotlib.pyplot import *
import numba
import numpy as np
import matplotlib.pyplot as plt


@numba.jitclass([
    ('Ne', numba.int64),
    ('Re', numba.float64[:, :]),
    ('spins', numba.int64[:]),
    ('Nn', numba.int64),
    ('Rn', numba.float64[:, :]),
    ('Zn', numba.float64[:]),
    # ('dim', numba.int64),
    ('sys_dim', numba.int64)
])
class Walker:
    """ A random walker """
    def __init__(
            self,
            Ne: int,
            Re: np.ndarray,
            spins: np.ndarray,
            Nn: int,
            Rn: np.ndarray,
            Zn: np.ndarray,
            dim: int):
        """ A random walker

        :param Ne: number of electrons
        :param Re: positions of the electrons
        :param spins: spins for the electrons
        :param Nn: number of nuclei
        :param Rn: positions of the nuclei
        :param Zn: charges of the nuclei
        :param dim: dimensionality of the system
        """
        self.Ne = Ne
        self.Re = Re
        self.spins = spins
        self.Nn = Nn
        self.Rn = Rn
        self.Zn = Zn
        self.sys_dim = dim

    def w_copy(self) -> "Walker":
        return Walker(self.Ne,
                      self.Re.copy(),
                      self.spins.copy(),
                      self.Nn,
                      self.Rn.copy(),
                      self.Zn.copy(),
                      self.sys_dim)


@numba.njit
def vmc_run(n_blocks: int, n_iters: int, delta: float, walkers_in: tp.List[Walker], n_target: int) \
        -> tp.Tuple[tp.List[Walker], np.ndarray, np.ndarray]:
    """ Run variational Monte Carlo with the given walkers

    :param n_blocks: number of Monte Carlo integration blocks
    :param n_iters: number of iterations for each block
    :param delta: amount of electron movement in a timestep
    :param walkers_in: the walkers in the start
    :param n_target: target walker count
    :return: walkers, energy, accepted move counts
    """
    Eb = np.zeros((n_blocks,))
    accept = np.zeros((n_blocks,))

    vmc_walker = walkers_in[0]  # just one walker needed
    walkers_out = []
    for i in range(n_blocks):
        for j in range(n_iters):
            # moving only electrons
            for k in range(vmc_walker.Ne):
                # "part 1"
                # the .copy() is necessary when walker.Re is an array, not a list of arrays
                R = vmc_walker.Re[k].copy()
                Psi_R: float = wfs(vmc_walker)

                # move the electron by a random
                vmc_walker.Re[k] = R + delta * (np.random.rand(vmc_walker.sys_dim) - 0.5)

                # calculate wave function at the new position
                # "part 2"
                Psi_Rp: float = wfs(vmc_walker)

                # calculate the sampling probability A(R -> R')
                # "part 3"
                A_RtoRp = min((Psi_Rp / Psi_R) ** 2, 1.0)

                # Metropolis
                # "parts 4 and 5"
                if A_RtoRp > np.random.rand():
                    accept[i] += 1.0 / vmc_walker.Ne
                else:
                    vmc_walker.Re[k] = R
                # end if
            # end for
            Eb[i] += E_local(vmc_walker)
        # end for
        if len(walkers_out) < n_target:
            walkers_out.append(vmc_walker.w_copy())
        Eb[i] /= n_iters
        accept[i] /= n_iters
        # String formatting is not supported in Numba
        # print('Block {0}/{1}'.format(i + 1, n_blocks))
        # print('    E   = {0:.5f}'.format(Eb[i]))
        # print('    Acc = {0:.5f}'.format(accept[i]))

        if i % 10 == 0:
            print("Block", i+1, "/", n_blocks, "(", round(i/n_blocks * 100), "% )")
            print("    E   =", round(Eb[i], 5))
            print("    Acc =", round(accept[i], 5))

    return walkers_out, Eb, accept


# Warning! This jitting does not work well. Please see the comments in delete_walkers()
@numba.njit
def dmc_run(n_blocks: int, n_iters: int, walkers: tp.List[Walker], tau: float, E_T: float, n_target: int) \
        -> tp.Tuple[tp.List[Walker], np.ndarray, np.ndarray]:
    """ Run diffusion Monte Carlo with the given walkers

    :param n_blocks: number of Monte Carlo integration blocks
    :param n_iters: number of iterations for each block
    :param walkers: walkers to start with
    :param tau: TODO
    :param E_T: TODO
    :param n_target: target walker count
    :return: walkers, energy, acceptances
    """

    max_walkers = 2 * n_target
    lW = len(walkers)
    # Ensure that there are at least as many walkers as desired
    while len(walkers) < n_target:
        # Add a copy of a randomly selected walker
        walkers.append(walkers[max(1, int(lW * np.random.rand()))].w_copy())

    Eb = np.zeros((n_blocks,))
    accept = np.zeros((n_blocks,))
    acc_count = np.zeros((n_blocks,))

    obs_interval = 5
    mass = 1

    for i in range(n_blocks):
        EbCount = 0
        for j in range(n_iters):
            w_size: int = len(walkers)
            i_dead: tp.List[int] = []
            # For each walker
            for k in range(w_size):
                Acc = 0.0
                E_L_R = 0.0
                E_L_Rp = 0.0

                # For each electron in the walker
                for walker_np in range(walkers[k].Ne):
                    R = walkers[k].Re[walker_np].copy()
                    Psi_R = wfs(walkers[k])
                    DriftPsi_R = 2 * gradient(walkers[k], walker_np) / Psi_R * tau / 2 / mass
                    E_L_R = E_local(walkers[k])

                    DeltaR = np.random.randn(walkers[k].sys_dim)
                    logGf = -0.5 * np.dot(DeltaR, DeltaR)

                    # Set new walker position
                    walkers[k].Re[walker_np] = R + DriftPsi_R + DeltaR * np.sqrt(tau / mass)

                    Psi_Rp = wfs(walkers[k])
                    DriftPsi_Rp = 2 * gradient(walkers[k], walker_np) / Psi_Rp * tau / 2 / mass
                    E_L_Rp = E_local(walkers[k])

                    DeltaR = R - walkers[k].Re[walker_np] - DriftPsi_Rp
                    logGb = -np.dot(DeltaR, DeltaR) / 2 / tau * mass

                    # Transition probability A(R -> R')
                    # Equation 29
                    A_RtoRp = min(1, (Psi_Rp / Psi_R) ** 2 * np.exp(logGb - logGf))

                    if A_RtoRp > np.random.rand():
                        Acc += 1.0 / walkers[k].Ne
                        accept[i] += 1
                    else:
                        walkers[k].Re[walker_np] = R

                    acc_count[i] += 1

                tau_eff = Acc * tau
                # Branching term
                GB = np.exp(-(0.5 * (E_L_R + E_L_Rp) - E_T) * tau_eff)
                MB = int(np.floor(GB + np.random.rand()))

                if MB > 1:
                    for n in range(MB - 1):
                        if len(walkers) < max_walkers:
                            walkers.append(walkers[k].w_copy())
                elif MB == 0:
                    i_dead.append(k)

            walkers = delete_walkers(walkers, i_dead)

            # Calculate observables every now and then
            if j % obs_interval == 0:
                EL = observable_E(walkers)
                Eb[i] += EL
                EbCount += 1
                E_T += 0.01 / tau * np.log(n_target / len(walkers))

        Nw = len(walkers)
        dNw = n_target - Nw
        for kk in range(abs(dNw)):
            ind = int(np.floor(len(walkers) * np.random.rand()))
            if dNw > 0:
                walkers.append(walkers[ind].w_copy())
            elif dNw < 0:
                walkers = delete_walkers(walkers, [ind])

        Eb[i] /= EbCount
        accept[i] /= acc_count[i]
        # String formatting is not supported in Numba
        # print('Block {0}/{1}'.format(i + 1, n_blocks))
        # print('    E   = {0:.5f}'.format(Eb[i]))
        # print('    Acc = {0:.5f}'.format(accept[i]))
        if i % 10 == 0:
            print("Block", i+1, "/", n_blocks, "(", round(i/n_blocks * 100), "% )")
            print("    E   =", Eb[i])
            print("    Acc =", accept[i])
        # print("post-processed")

    return walkers, Eb, accept


@numba.njit
def delete_walkers(walkers: tp.List[Walker], i_dead: tp.List[int]) -> tp.List[Walker]:
    """ Delete dead walkers from the given list

    Used only for diffusion Monte Carlo
    """
    if len(i_dead) > 0:
        # If all walkers are dead
        if len(walkers) == len(i_dead):
            walkers = [walkers[0]]
        else:
            # Remove dead in reverse order so that the loop indexing won't go awry
            i_dead.sort(reverse=True)
            for i in range(len(i_dead)):
                # del walkers[i_dead[i]]
                walkers.pop(i_dead[i])

    # For some reason, this reduces the probability of a segfault significantly
    broken_walkers = []
    for i, walker in enumerate(walkers):
        if walker.Ne < 0:
            broken_walkers.append(walker)
    if broken_walkers:
        print("Broken walkers")
        for walker in broken_walkers:
            print(walker.Ne)
            print(walker.Nn)
        raise RuntimeError

    return walkers


@numba.njit
def H_1s(r1: np.ndarray, r2: np.ndarray) -> np.ndarray:
    # exp(-sqrt(delta_r**2))
    return np.exp(-np.sqrt(np.sum((r1 - r2) ** 2)))


@numba.njit
def wfs(walker: Walker) -> float:
    """ Wave function optimization for VMC using Jastrow function """
    # H2 approx
    f = H_1s(walker.Re[0], walker.Rn[0]) + H_1s(walker.Re[0], walker.Rn[1])
    f *= (H_1s(walker.Re[1], walker.Rn[0]) + H_1s(walker.Re[1], walker.Rn[1]))

    # J = Jastrow function
    J = 0.0
    # Jastrow e-e
    for i in range(walker.Ne - 1):
        for j in range(i + 1, walker.Ne):
            # Electron separation
            r = np.sqrt(np.sum((walker.Re[i] - walker.Re[j]) ** 2))
            # Equation 14 and the Pade form
            if walker.spins[i] == walker.spins[j]:
                # HERE IS THE JASTROW FORM USED
                # Pade form for unlike spins
                J += 0.25 * r / (1.0 + 1.0 * r)
            else:
                # Pade form for like spins
                J += 0.5 * r / (1.0 + 1.0 * r)

    # Jastrow e-Ion
    for i in range(walker.Ne):
        for j in range(walker.Nn):
            # Electron-ion separation
            r = np.sqrt(np.sum((walker.Re[i] - walker.Rn[j]) ** 2))
            # Equation 13
            J -= walker.Zn[j] * r / (1.0 + 100.0 * r)

    # Equation 10
    return f * np.exp(J)


@numba.njit
def potential(walker: Walker) -> float:
    """ Calculate the potential energy of a walker

    Sum the energies of each e-e, e-ion and ion-ion interaction together
    """
    V = 0.0
    r_cut = 1.0e-4
    # e-e
    for i in range(walker.Ne - 1):
        for j in range(i + 1, walker.Ne):
            r = np.sqrt(np.sum((walker.Re[i] - walker.Re[j]) ** 2))
            V += 1.0 / max(r_cut, r)

    # e-Ion
    for i in range(walker.Ne):
        for j in range(walker.Nn):
            r = np.sqrt(np.sum((walker.Re[i] - walker.Rn[j]) ** 2))
            V -= walker.Zn[j] / max(r_cut, r)

    # Ion-Ion
    for i in range(walker.Nn - 1):
        for j in range(i + 1, walker.Nn):
            r = np.sqrt(np.sum((walker.Rn[i] - walker.Rn[j]) ** 2))
            V += 1.0 / max(r_cut, r)
    return V


@numba.njit
def local_kinetic(walker: Walker) -> float:
    """ Calculate the local kinetic energy of all electrons in the walker"""
    # laplacian -0.5 \nabla^2 \Psi / \Psi
    h = 0.001
    h2 = h * h
    K = 0.0
    Psi_R = wfs(walker)
    for i in range(walker.Ne):
        for j in range(walker.sys_dim):
            Y = walker.Re[i][j]
            walker.Re[i][j] -= h
            wfs1 = wfs(walker)
            walker.Re[i][j] += 2.0 * h
            wfs2 = wfs(walker)
            K -= 0.5 * (wfs1 + wfs2 - 2.0 * Psi_R) / h2
            walker.Re[i][j] = Y
    return K / Psi_R


@numba.njit
def gradient(walker: Walker, particle: int) -> np.ndarray:
    h = 0.001
    dPsi = np.zeros(shape=walker.Re[particle].shape)
    for i in range(walker.sys_dim):
        Y = walker.Re[particle][i]
        walker.Re[particle][i] -= h
        wfs1 = wfs(walker)
        walker.Re[particle][i] += 2.0 * h
        wfs2 = wfs(walker)
        dPsi[i] = (wfs2 - wfs1) / 2 / h
        walker.Re[particle][i] = Y
    return dPsi


@numba.njit
def E_local(walker: Walker):
    """ The local energy of a walker is the sum of its kinetic and potential energies """
    return local_kinetic(walker) + potential(walker)


@numba.njit
def observable_E(walkers: tp.List[Walker]) -> float:
    """ Calculate the average energy of given walkers """
    E = 0.0
    num_walkers = len(walkers)
    for i in range(num_walkers):
        E += E_local(walkers[i])
    E /= num_walkers
    return E


def plot(walkers, Eb, acc, name):
    std_mean = np.std(Eb)       # / np.sqrt(Eb.size)
    e_mean = np.mean(Eb)

    print(name)
    print("Mean acceptance", np.mean(acc))
    print("E", e_mean)
    print("std(E)", std_mean)

    plt.figure()
    plt.plot(Eb)
    plt.axhline(e_mean, color="k")
    plt.axhline(e_mean + std_mean, color="k", ls=":")
    plt.axhline(e_mean - std_mean, color="k", ls=":")
    plt.title("Energy ({})".format(name))
    plt.xlabel("i")
    plt.ylabel("E")

    plt.figure()
    plt.plot(acc)
    plt.title("Acceptance ({})".format(name))
    plt.xlabel("i")
    plt.ylabel("Acceptance")
    plt.show()


def main():
    walkers = [
        Walker(
            Ne=2,
            Re=np.array([[0.5, 0, 0], [-0.5, 0, 0]]),
            # Re=[np.array([0.5, 0, 0]), np.array([-0.5, 0, 0])],
            spins=np.array([0, 1]),
            # spins=[0, 1],
            Nn=2,
            # The nuclei are spaced 0.7 + 0.7 = 1.4 a0 apart
            Rn=np.array([[-0.7, 0, 0], [0.7, 0, 0]]),
            # Rn=[np.array([-0.7, 0, 0]), np.array([0.7, 0, 0])],
            Zn=np.array([1.0, 1.0]),
            # Zn=[1.0, 1.0],
            dim=3
        )
    ]

    # vmc_only = True
    vmc_only = False

    n_target = 100
    # Reducing the time step helps in getting the acceptance ratio close to 50 %
    # vmc_time_step = 10.0
    vmc_time_step = 2.5

    vmc_start_time = time.perf_counter()
    # walkers, Eb, acc = vmc_run(n_blocks=100, n_iters=50, delta=vmc_time_step, walkers_in=walkers, n_target=n_target)
    walkers, Eb, acc = vmc_run(n_blocks=1000, n_iters=1000, delta=vmc_time_step, walkers_in=walkers, n_target=n_target)
    print("VMC took", time.perf_counter() - vmc_start_time, "s")
    plot(walkers, Eb, acc, "VMC")

    print("VMC ready. Calculating DMC")
    if not vmc_only:
        dmc_start_time = time.perf_counter()
        # walkers, Eb_dmc, accept_dmc = dmc_run(n_blocks=10, n_iters=10, walkers=walkers, tau=0.05, E_T=np.mean(Eb), n_target=n_target)
        # walkers, Eb_dmc, accept_dmc = dmc_run(n_blocks=30, n_iters=30, walkers=walkers, tau=0.05, E_T=np.mean(Eb), n_target=n_target)
        walkers, Eb_dmc, accept_dmc = dmc_run(n_blocks=50, n_iters=50, walkers=walkers, tau=0.05, E_T=np.mean(Eb), n_target=n_target)
        print("DMC took", time.perf_counter() - dmc_start_time, "s")

        plot(walkers, Eb_dmc, accept_dmc, "DMC")


if __name__ == "__main__":
    main()
