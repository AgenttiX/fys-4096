import numpy as np


def mid_points(nmax: int, a0: np.ndarray, a1: np.ndarray, a2: np.ndarray) -> np.ndarray:
    """

    :param nmax:
    :param a0: lattice vector
    :param a1: lattice vector
    :param a2: lattice vector
    :return:
    """
    points = []
    for n0 in range(-nmax, nmax+1):
        for n1 in range(-nmax, nmax+1):
            for n2 in range(-nmax, nmax+1):
                if not (n0 == n1 == n2 == 0):
                    points.append(n0*a0/2 + n1*a1/2 + n2*a2/2)
    return np.array(points)


def intersecting_line():
    # TODO

    def line(s: float):
        pass

    return line


def trim(vertex_points: list):
    for point in vertex_points:
        pass

    # TODO
