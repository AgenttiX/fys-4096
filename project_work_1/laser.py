"""Laser equations and functions

Based on the book:
Principles of Lasers, 5th ed., Orazio Svelto
"""

import typing as tp

import numba
import numpy as np
import scipy.optimize


@numba.njit
def runge_kutta4(
        x: np.ndarray,
        dt: float,
        func: callable,
        rp: float,
        rp_next: float,
        b: float,
        va: float,
        tau: float,
        tau_c: float,
        tau_c_next: float
) -> tp.Tuple[np.ndarray, float]:

    # The changes of external variables are approximated to be linear
    rp_half = (rp + rp_next) / 2
    tau_c_half = (tau_c + tau_c_next) / 2

    f1 = func(x, rp=rp, b=b, va=va, tau=tau, tau_c=tau_c)
    f2 = func(x + dt / 2 * f1, rp=rp_half, b=b, va=va, tau=tau, tau_c=tau_c_half)
    f3 = func(x + dt / 2 * f2, rp=rp_half, b=b, va=va, tau=tau, tau_c=tau_c_half)
    f4 = func(x + dt * f3, rp=rp_next, b=b, va=va, tau=tau, tau_c=tau_c_next)

    x_new = x + dt / 6 * (f1 + 2 * f2 + 2 * f3 + f4)

    # Population N2 cannot be smaller than zero
    x_new[0] = max(0, x_new[0])

    # There always has to be at least one photon in the cavity for stimulated emission to occur
    # This is provided by spontaneous emission
    x_new[1] = max(1, x_new[1])

    return x_new


@numba.njit
def rate_equations(x: np.ndarray, rp: float, b: float, va: float, tau: float, tau_c: float):
    """Laser rate equations (four-level laser)

    Page 256, equations 7.2.1 a and b
    """
    n = x[0]
    phi = x[1]

    return np.array([
        # dN2 / dt
        rp - b * phi * n - n / tau,

        # Using equation 7.2.16b to account for spontaneous emission in the correct mode
        # (b * va * n - 1 / tau_c) * phi

        # dphi / dt
        # Using equation 7.2.2
        va*b*(phi+1)*n - phi/tau_c
    ])


@numba.njit
def laser_solver(
        n0: float, phi0: float,
        t: np.ndarray, rp: np.ndarray,
        b: float, va: float,
        tau: float, tau_c: np.ndarray) -> tp.Tuple[np.ndarray, np.ndarray]:
    """Solve the rate equations at each moment t[i]

    Numba is used, since without it the simulation would be painfully slow
    """
    x_vec = np.zeros((2, t.size))
    x = np.array([n0, phi0])

    for i in range(t.size-1):
        x = runge_kutta4(
            x=x,
            dt=t[i+1] - t[i],
            func=rate_equations,
            rp=rp[i],
            rp_next=rp[i+1],
            b=b,
            va=va,
            tau=tau,
            tau_c=tau_c[i],
            tau_c_next=tau_c[i+1]
        )

        x_vec[:, i] = x

    # Last point is computed separately
    x = runge_kutta4(
        x=x,
        dt=t[-1] - t[-2],
        func=rate_equations,
        rp=rp[-1],
        rp_next=rp[-1],
        b=b,
        va=va,
        tau=tau,
        tau_c=tau_c[-1],
        tau_c_next=tau_c[-1]
    )
    x_vec[:, -1] = x

    # N, phi
    return x_vec[0, :], x_vec[1, :]


# Support functions

def critical_inversion(b, va, tau_c):
    """Equation 7.3.2"""
    return 1/(b*va*tau_c)


def critical_pump_rate(b, va, tau_c, tau):
    """Equations 7.3.3 and 7.3.2"""
    return 1/(b*va*tau_c*tau)


def inversion_at_peak_sim(n: np.ndarray, phi: np.ndarray):
    peak_ind = np.argmax(phi)
    return n[peak_ind]


def __inversion_utilization_func(nu_e, x):
    return nu_e * x + np.log(1 - nu_e)


def inversion_utilization(ni: float, n_peak: float):
    """nu_e, in equation 8.4.19"""
    x = ni/n_peak
    start_estimates = np.array([0.5])

    sol = scipy.optimize.fsolve(__inversion_utilization_func, x0=start_estimates, args=(x, ))
    return sol[0]


def inversion_utilization_sim(n_pulse: np.ndarray):
    """Notes for equation 8.4.19"""
    ni = np.max(n_pulse)
    nf = np.min(n_pulse)
    return (ni - nf) / ni


def log_losses(r1, r2, li):
    """Logarithmic single-pass cavity losses

    Equation 7.2.8
    """
    return 0.5*(-np.log(r1) - np.log(r2)) - np.log(1 - li)


def peak_photons(va, ni, n_peak):
    """Equation 8.4.14"""
    x = ni/n_peak
    return va*n_peak*(x - np.log(x) - 1)


def photon_lifetime(le, gamma, c=2.99792458e8):
    """Photon lifetime in the cavity"""
    return le / (gamma*c)


def pulse_delay(tau_c, ni, n_peak, phi_p):
    """Equation 8.4.23"""
    return tau_c / (ni/n_peak - 1) * np.log(phi_p/10)


def pulse_delay_sim(t: np.ndarray, n: np.ndarray):
    """Calculate the pulse delay from the simulated data of a single pulse"""
    peak_ind = np.argmax(n)
    return t[peak_ind] - t[0]


def pulse_length(tau_c, nu_e, ni, n_peak):
    """Equation 8.4.21"""
    x = ni/n_peak
    return tau_c * x * nu_e / (x - np.log(x) - 1)


def pulse_length_corrected(tau_c, nu_e, ni, np):
    """Including the 9% correction mentioned in the text"""
    return (1-0.09) * pulse_length(tau_c, nu_e, ni, np)


def pulse_length_sim(t: np.ndarray, phi: np.ndarray):
    """Calculate pulse length from the simulated data of a single pulse"""
    peak_phi = np.max(phi)
    peak_times = t[phi > peak_phi / 2]

    return peak_times[-1] - peak_times[0]


def rel_osc_cond(tau_c, tau, x):
    """Check whether relaxation oscillations can occur

    There is probably a bug here. Should the comparison be the other way?
    Equation 8.2.16
    x = Rp/Rcp
    """
    return tau_c/tau > 4*(x-1)/x**2


def rel_osc_damp_time(tau, x):
    """Damping time of relaxation oscillations

    Equation 8.2.14
    """
    return 2*tau / x


def rel_osc_freq(tau_c, tau, x):
    """Angular frequency of relaxation oscillations

    Equation 8.2.15
    """
    return np.sqrt((x-1)/(tau_c*tau))
