"""A laser simulator featuring both Q-switching and relaxation oscillations

Created by Mika Mäki, 2019

General information:

https://www.rp-photonics.com/q_switching.html
https://www.rp-photonics.com/relaxation_oscillations.html

The pumping (electrical or another laser) is turned on at t=0. This results in the growth of the population inversion,
until the laser starts lasing (at phi > 1). However, phi doesn't settle to its equilibrium value immediately, but
instead experiences so-called relaxation oscillations.

At a later time a periodic modulator is added to the cavity with both minimum and maximum losses > 0. Lasing will stop
for a while as the population inversion increases to overcome the minimum losses. Once the population inversion is high
enough, there will be lasing during the periods of low losses, and this lasing rapidly depletes the population
inversion, resulting in pulses.

The values given by the simulation differ somewhat from the values given by the significantly approximated equations
of the book. However, with a single manual correction accounting for spontaneous emission etc. during the pulse buildup,
the values can be made to have equal orders of magnitude.


An exercise question this simulation could be the answer to:
Solve the laser rate equations numerically using a method of your choice, e.g. Runge-Kutta.
a) Include a stepwise change in the pump rate and show, that relaxation oscillations result directly from the equations,
and that the oscillation period converges to the value qiven by the equations in literature.
b) Add a modulator into the cavity by changing the photon lifetime periodically and show, that this results
in Q-switched pulsed operation of the laser, where the peak power of the pulses is higher than the CW power of the laser
with the given pump rate.

In the case of Q-switching, compare the pulse characteristics to the values given by formulas in literature.
Draw figures of both cases and show visually, that the Q-switched pulses are asymmetrical.
"""

# import matplotlib.pyplot as plt
import numpy as np
import pyqtgraph as pg
import scipy.signal

from project_work_1 import laser


def main():
    # Laser startup (count of steps and total time)
    n_steps_build = int(1e5)
    t_build_len = 5e-4

    # Pulsing
    n_steps_pulsing = int(8e5)
    t_pulsing_len = 4e-4

    # Time vectors
    t_build = np.linspace(0, t_build_len, n_steps_build)
    t_pulsing = t_build[-1] + (t_build[-1] - t_build[-2]) + np.linspace(0, t_pulsing_len, n_steps_pulsing)
    t = np.concatenate((t_build, t_pulsing))

    # Cavity parameters
    # Effective length (m)
    le = 0.01
    # Einstein B coefficient
    b = 1
    # Volume of the active medium (m^3)
    va = 0.01 ** 3
    # Spontaneous emission lifetime (s)
    # Example 8.1, page 316
    tau = 230e-6
    # Pumping (times the critical pumping)
    rp_mult = 1.5
    # Mirror reflectivities
    r1 = 1
    r2 = 0.99
    # Modulator parameters
    frequency = 5e4
    omega = 2*np.pi*frequency
    max_loss = 0.1
    min_loss = 0.001

    tau_p = 1/frequency
    print("Pulse repetition rate:", tau_p)

    dt_pulsing = t[-1] - t[-2]
    pulse_len_ind = int(tau_p / dt_pulsing)
    last_pulse_start = t.size - pulse_len_ind

    # Modulator (aka. internal) losses
    li_build = np.zeros_like(t_build)
    li_pulsing = max_loss - (max_loss-min_loss)*0.5*(scipy.signal.square(omega*t_pulsing) + 1)
    li = np.concatenate((li_build, li_pulsing))

    if np.max(li) >= 1:
        raise ValueError
    elif np.min(li) < 0:
        raise ValueError
    # print(np.max(li), np.min(li))

    # Logarithmic cavity losses
    gamma = laser.log_losses(r1, r2, li)
    gamma0 = laser.log_losses(r1, r2, 0)
    gamma_pulse_peak = laser.log_losses(r1, r2, min_loss)
    print("gamma:", gamma)

    # Photon lifetimes in the cavity
    tau_c = laser.photon_lifetime(le, gamma)
    tau_c0 = laser.photon_lifetime(le, gamma0)
    tau_c_pulse_peak = laser.photon_lifetime(le, gamma_pulse_peak)
    print("tau_c:", tau_c)

    rcp = laser.critical_pump_rate(b, va, tau_c0, tau)
    # Just a multiplier for easy control of Rp relative to Rcp
    rp = np.ones((t.size,)) * rp_mult * rcp

    print()

    nc = laser.critical_inversion(b, va, tau_c0)
    n_peak = laser.critical_inversion(b, va, tau_c_pulse_peak)

    # This does not account for spontaneous emission and thereby gives a too large value
    # ni = n_peak + rp[-1] * tau_p/2
    # A manual correction is necessary
    # ni = n_peak + rp[-1] * tau_p/3
    ni = 1.82e14

    print("Equations from the book")
    print("Nc: {:.3e} (before the modulator is added)".format(nc))
    x = rp[1]/rcp
    # rel_osc_cond = laser.rel_osc_cond(tau_c0, tau, x)
    # print("Relaxation oscillation condition:", rel_osc_cond)
    rel_osc_damp = laser.rel_osc_damp_time(tau, x)
    print("Relaxation oscillation damping time: {:.3e}".format(rel_osc_damp))
    rel_osc_freq = laser.rel_osc_freq(tau_c0, tau, x)
    print("Relaxation oscillation frequency: {:.3e}".format(rel_osc_freq))
    rel_osc_period = 2*np.pi / rel_osc_freq
    print("Relaxation oscillation period: {:.3e}".format(rel_osc_period))

    print()
    print("Ni: {:.3e}".format(ni))
    print("Np: {:.3e}".format(n_peak))

    nu_e = laser.inversion_utilization(ni, n_peak)
    print("Inversion utilization: {:.3e}".format(nu_e))
    dtaup = laser.pulse_length(tau_c_pulse_peak, nu_e, ni, n_peak)
    dtaup_corr = laser.pulse_length_corrected(tau_c_pulse_peak, nu_e, ni, n_peak)
    print("Pulse length: {:.3e}".format(dtaup))
    print("Pulse length (corrected): {:.3e}".format(dtaup_corr))

    phi_peak = laser.peak_photons(va, ni, n_peak)
    print("Peak photon count: {:.3e}".format(phi_peak))

    tau_d = laser.pulse_delay(tau_c_pulse_peak, ni, n_peak, phi_peak)
    print("Pulse delay: {:.3e}".format(tau_d))

    print()

    # Simulation

    n, phi = laser.laser_solver(
        n0=0.0, phi0=0.0,
        t=t, rp=rp,
        b=b, va=va,
        tau=tau, tau_c=tau_c
    )

    print()

    rel_osc_end_ind_cutout = 10 # Just to be sure
    nc_sim = n[t_build.size - rel_osc_end_ind_cutout]

    # Analysis of the last pulse

    last_pulse_t = t[last_pulse_start:]
    last_pulse_n = n[last_pulse_start:]
    last_pulse_phi = phi[last_pulse_start:]
    last_pulse_tau_c = tau_c[last_pulse_start:]

    print("Simulation")
    print("Nc: {:.3e} (before the modulator is added)".format(nc_sim))

    rel_osc_start_ind = np.argmax(n > nc_sim)
    rel_osc_end_ind = t_build.size - rel_osc_end_ind_cutout
    rel_osc_t = t[rel_osc_start_ind:rel_osc_end_ind]
    rel_osc_phi = phi[rel_osc_start_ind:rel_osc_end_ind]
    rel_osc_phi_grad = np.gradient(rel_osc_phi)
    rel_osc_phi_grad_zeros = np.where(np.diff(np.sign(rel_osc_phi_grad)))[0]
    rel_osc_phi_grad_zeros_times = rel_osc_t[rel_osc_phi_grad_zeros]
    rel_osc_periods_sim = np.gradient(rel_osc_phi_grad_zeros_times) * 2
    print("Rel osc grad zeros times", rel_osc_phi_grad_zeros_times)
    print("Rel osc periods", rel_osc_periods_sim)
    # rel_osc_period_sim = rel_osc_phi_grad_zeros_times[2] - rel_osc_phi_grad_zeros_times[0]
    # print("Relaxation oscillation period: {:.3e}".format(rel_osc_period_sim))
    print("Relaxation oscillation period (visual): {:.3e}".format(0.0002702 - 0.000259))

    print()
    print("Ni: {:.3e}".format(np.max(last_pulse_n)))
    print("Np: {:.3e}".format(laser.inversion_at_peak_sim(last_pulse_n, last_pulse_phi)))
    print("Inversion utilization: {:.3e}".format(laser.inversion_utilization_sim(last_pulse_n)))
    print("Pulse length: {:.3e}".format(laser.pulse_length_sim(last_pulse_t, last_pulse_phi)))
    print("Peak photon count: {:.3e}".format(np.max(last_pulse_phi)))
    print("Pulse delay: {:.3e}".format(laser.pulse_delay_sim(last_pulse_t, last_pulse_n)))

    # Plotting

    # PyQtGraph is used, since it enables linking the axes of the figures
    app = pg.mkQApp()
    pg.setConfigOptions(antialias=True, background="w", foreground="k")

    win = pg.GraphicsWindow(title="Relaxation oscillations and Q-switching")

    bpen = pg.mkPen("b")
    rpen = pg.mkPen("r")
    # kpen = pg.mkPen("k")

    plot_n = win.addPlot(title="Population inversion")
    win.nextRow()
    plot_phi = win.addPlot(title="Photon count in the cavity")
    win.nextRow()
    plot_tau_c = win.addPlot(title="Photon lifetime in the cavity")

    plot_phi.setXLink(plot_n)
    plot_tau_c.setXLink(plot_n)

    plot_n.plot(t, n, pen=bpen)
    plot_n.plot([t[0], t[-1]], [nc, nc], pen=rpen)
    plot_n.plot([t[0], t[-1]], [n_peak, n_peak], pen=rpen)

    plot_phi.plot(t, phi, pen=bpen)
    plot_tau_c.plot(t, tau_c, pen=bpen)

    win2 = pg.GraphicsWindow(title="A single pulse")

    plot_pulse_n = win2.addPlot(title="Population inversion")
    win2.nextRow()
    plot_pulse_phi = win2.addPlot(title="Photon count in the cavity")
    win2.nextRow()
    plot_pulse_tau_c = win2.addPlot(title="Photon lifetime in the cavity")

    plot_pulse_phi.setXLink(plot_pulse_n)
    plot_pulse_tau_c.setXLink(plot_pulse_n)

    plot_pulse_n.plot(last_pulse_t, last_pulse_n, pen=bpen)
    plot_pulse_phi.plot(last_pulse_t, last_pulse_phi, pen=bpen)
    plot_pulse_tau_c.plot(last_pulse_t, last_pulse_tau_c, pen=bpen)

    win3 = pg.GraphicsWindow(title="Relaxation oscillations")

    plot_rel_osc_periods = win3.addPlot(title="Periods")
    plot_rel_osc_periods.plot(rel_osc_periods_sim, pen=bpen)
    plot_rel_osc_periods.plot(
        [0, rel_osc_periods_sim.size - 1],
        [rel_osc_period, rel_osc_period],
        pen=rpen
    )

    # fig = plt.figure()
    # ax1 = fig.add_subplot(411)
    # ax2 = fig.add_subplot(412)
    # ax3 = fig.add_subplot(413)
    # ax4 = fig.add_subplot(414)
    #
    # ax1.plot(t, rp, label="$R_p$")
    # ax2.plot(t, n, label="$N_2$")
    # ax2.plot([t[0], t[-1]], [nc, nc], label="$N_c(0)$")
    # ax3.plot(t, phi, label="$\phi$")
    # ax4.plot(t, tau_c, label=r"$\tau_c$")
    #
    # ax1.legend()
    # ax2.legend()
    # ax3.legend()
    # ax4.legend()
    #
    # plt.show()

    app.exec_()


if __name__ == "__main__":
    main()
