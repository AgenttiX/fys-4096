"""A laser simulator for relaxation oscillations

This got incorporated to the q_switching.py and exists merely as a reference when something breaks in q_switching.py
"""

import matplotlib.pyplot as plt
import numpy as np

from project_work_1 import laser


def main():
    n_steps = int(1e5)
    t_max = 1e-3

    le = 0.01
    b = 1
    va = 0.01 ** 3

    r1 = 1
    r2 = 0.99
    li = 0
    gamma = laser.log_losses(r1, r2, li)
    print("gamma:", gamma)

    # Example 8.1, page 316
    tau = 230e-6

    tau_c = laser.photon_lifetime(le, gamma)
    print("tau_c:", tau_c)
    tau_c_vec = np.ones((n_steps,)) * tau_c

    t = np.linspace(0, t_max, n_steps)

    rp = np.zeros(t.size, )
    rp[n_steps // 10: - n_steps // 5] = 2 * laser.critical_pump_rate(b, va, tau_c, tau)

    nc = laser.critical_inversion(b, va, tau_c)

    n, phi = laser.laser_solver(
        n0=0.0, phi0=0.0,
        t=t, rp=rp,
        b=b, va=va,
        tau=tau, tau_c=tau_c_vec
    )

    fig = plt.figure()
    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)

    ax1.plot(t, rp, label="$R_p$")
    ax2.plot(t, n, label="$N_2$")
    ax2.plot([t[0], t[-1]], [nc, nc], label="$N_c$")
    ax3.plot(t, phi, label="$\phi$")

    ax1.legend()
    ax2.legend()
    ax3.legend()

    plt.show()


if __name__ == "__main__":
    main()
