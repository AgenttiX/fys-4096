"""The warm up exercise of the project work"""

import time
# import sys

import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate


def func(x, y):
    return (x + y) * np.exp(-0.5 * np.sqrt(x**2 + y**2))


def main():
    start_time = time.perf_counter()

    lattice = np.array([
        [1.1, 0],
        [0.5, 1]
    ])

    volume = np.linalg.det(lattice)
    a_vec = lattice[0, :]
    b_vec = lattice[1, :]

    # Warning! These values are really heavy on the memory
    n_a = 20000
    n_b = 20000

    # Old way: these for loops are way too slow
    # coords = np.zeros((2, n_a, n_b))
    #
    # print("Creating coordinates")
    # for ia in range(n_a):
    #     for ib in range(n_b):
    #         coords[:, ia, ib] = ia/n_a*a_vec + ib/n_b*b_vec

    a_coords = np.outer(a_vec, np.linspace(0, 1, n_a))
    b_coords = np.outer(b_vec, np.linspace(0, 1, n_b))

    # Construct coordinate meshgrids
    x_coords = np.add.outer(a_coords[0, :], b_coords[0, :])
    y_coords = np.add.outer(a_coords[1, :], b_coords[1, :])

    # print(sys.getsizeof(x_coords))
    # print(sys.getsizeof(y_coords))

    print("Computing function values")
    # The function has to be called only once
    # However, this takes an absurd amount of RAM (up to 20 GB)
    f_vals = func(x_coords, y_coords)

    # Old way
    # f_vals = func(coords[0, ...], coords[1, ...])

    # The integration is done in the (a, b) space, and converted to
    # (x, y) space afterwards by multiplying by the volume
    print("Integrating")
    f_vals_int = scipy.integrate.simps(f_vals)
    print("Integrating for the second axis")
    f_vals_int2 = scipy.integrate.simps(f_vals_int)

    # The resulting value is quite close, but not exact. Probably the difference is caused by inaccuracies introduced
    # when computing the integral in the (a, b) space.
    result = f_vals_int2 / (n_a*n_b) * volume

    print(result)
    print("Total time:", time.perf_counter() - start_time)

    res = 100
    plt.contourf(x_coords[::res, ::res], y_coords[::res, ::res], f_vals[::res, ::res])
    plt.show()

    # Program output:
    # Computing function values
    # Integrating
    # Integrating for the second axis
    # 0.8248049596997243
    # Total time: 23.810200744999747
    #
    # Process finished with exit code 0


if __name__ == "__main__":
    main()
