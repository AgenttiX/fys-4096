import ctypes
import typing as tp

import numba
import numba.extending

import scipy.special.cython_special
import scipy.special


def find_function(name: str) -> None:
    capi = scipy.special.cython_special.__pyx_capi__
    for cname, cvalue in capi.items():
        if name in cname:
            print(cname)
            print(cvalue)


def njit(module: str, func: str, functype: tuple, numba_kwargs: tp.Dict[str, any] = None) -> callable:
    addr = numba.extending.get_cython_function_address(module, func)
    ct_functype = ctypes.CFUNCTYPE(*functype)
    func = ct_functype(addr)

    if numba_kwargs is not None:
        @numba.njit(**numba_kwargs)
        def jitted(*jitted_args):
            return func(*jitted_args)
    else:
        @numba.njit
        def jitted(*jitted_args):
            return func(*jitted_args)

    return jitted


def main():
    a = scipy.special.cython_special.erf(0.2)
    print(type(scipy.special.cython_special.erf))
    print(a)

    # "One caveat is that if your function uses Cython’s fused types, then the function’s name will be mangled.
    # To find out the mangled name of your function you can check the extension module’s __pyx_capi__ attribute."
    addr = numba.extending.get_cython_function_address("scipy.special.cython_special", "__pyx_fuse_1erf")
    functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double)
    func = functype(addr)

    @numba.vectorize("float64(float64)")
    def vectorized(x):
        return func(x)

    @numba.njit
    def jitted(x):
        return vectorized(x)

    print(jitted(0.2))

    jit2 = njit("scipy.special.cython_special", "__pyx_fuse_1erf", (ctypes.c_double, ctypes.c_double))
    print(jit2(0.2))


if __name__ == "__main__":
    main()
