""" Analysis tools for existing PIMC data """

import os.path
import typing as tp

import matplotlib.pyplot as plt
import numpy as np

from project_work_2 import pimc_back


def cut_vecs(*vecs, cut_size: float = 0.1):
    """ Cut the equilibration time from the start of each vector """
    out = []
    size = vecs[0].shape[0]
    cut = int(size * cut_size)
    for vec in vecs:
        out.append(vec[cut:, ...])
    return tuple(out)


def plot_set(ax_e, ax_acc, runs: tp.List[pimc_back.Run], title: str):
    """ Plot observables """
    for run in runs:
        ax_e.plot(run.energy, label="{:.0f} K".format(run.temperature()))
    ax_e.set_xlabel("Block")
    ax_e.set_ylabel("E")
    ax_e.set_title("Energy ({})".format(title))
    ax_e.legend()

    for run in runs:
        ax_acc.plot(run.accept, label="{:.0f} K".format(run.temperature()))
    ax_acc.set_xlabel("Block")
    ax_acc.set_ylabel("Acceptance")
    ax_acc.set_title("Acceptance ({})".format(title))
    ax_acc.legend()


def plot_set_temp(ax_e, ax_acc, runs: tp.List[pimc_back.Run], title: str, cut: float = 0.5):
    """ Plot temperature dependence """
    e_means = np.zeros((len(runs),))
    e_stds = np.zeros_like(e_means)
    acc_means = np.zeros_like(e_means)
    acc_stds = np.zeros_like(e_means)
    temps = np.array([run.temperature() for run in runs])

    for i, run in enumerate(runs):
        e_cut, acc_cut = cut_vecs(run.energy, run.accept, cut_size=cut)
        e_means[i] = np.mean(e_cut)
        e_stds[i] = np.std(e_cut) / np.sqrt(acc_cut.size)
        acc_means[i] = np.mean(acc_cut)
        acc_stds[i] = np.std(acc_cut) / np.sqrt(acc_cut.size)

    ax_e.errorbar(
        temps, e_means,
        yerr=e_stds,
        fmt="o-",
        capsize=5,
        ecolor="black"
    )
    ax_e.set_xlabel("T (K)")
    ax_e.set_ylabel("E")
    ax_e.set_title("Energy ({})".format(title))

    ax_acc.errorbar(
        temps, acc_means,
        yerr=acc_stds,
        fmt="o-",
        capsize=5,
        ecolor="black"
    )
    ax_acc.set_xlabel("T (K)")
    ax_acc.set_ylabel("Acecptance")
    ax_acc.set_title("Acceptance ({})".format(title))


def stats(run: pimc_back.Run):
    """ Print the statistics of a run """
    e_cut, acc_cut = cut_vecs(run.energy, run.accept)

    e_mean = np.mean(e_cut)
    e_std = np.std(e_cut) / np.sqrt(e_cut.size)
    acc_mean = np.mean(acc_cut)
    acc_std = np.std(acc_cut) / np.sqrt(acc_cut.size)

    print("Temperature: {} K".format(run.temperature()))
    print("Energy: {:.3f} +/- {:.3f}".format(e_mean, e_std))
    print("Acceptance: {:.3f} +/- {:.3f}".format(acc_mean, acc_std))


def multi():
    path = os.path.dirname(os.path.abspath(__file__))
    folder = os.path.join(path, "multi_temp")
    folder_classical = os.path.join(folder, "classical")
    folder_quantum = os.path.join(folder, "quantum")

    temps = [300, 500, 700, 1000, 1500, 2000, 3000, 4000]

    runs_classical = [pimc_back.Run.from_file("{}/temp_{}_classical.hdf5".format(folder_classical, temp)) for temp in temps]
    runs_quantum = [pimc_back.Run.from_file("{}/temp_{}_quantum.hdf5".format(folder_quantum, temp)) for temp in temps]

    fig, axes = plt.subplots(2, 2)
    plot_set(axes[0][0], axes[0][1], runs_classical, "classical")
    plot_set(axes[1][0], axes[1][1], runs_quantum, "quantum")
    fig.suptitle("Observables")

    fig2, axes2 = plt.subplots(2, 2)
    plot_set_temp(axes2[0][0], axes2[0][1], runs_classical, "classical")
    plot_set_temp(axes2[1][0], axes2[1][1], runs_quantum, "quantum")
    fig2.suptitle("Temperature dependence")

    n_atoms = runs_classical[0].walker.Ne
    m_hydrogen_kg = 1.008 * 1.660539040e-27
    a0_m = 5.29e-11
    density = pimc_back.density(m_atom=m_hydrogen_kg, n_atoms=n_atoms, cell_size=runs_classical[0].cell_size * a0_m)
    print("Density: {:.3f} kg/m^3 (g/l)".format(density))

    print()
    print("Classical")
    for run in runs_classical:
        stats(run)

    print()
    print("Quantum")
    for run in runs_quantum:
        stats(run)


if __name__ == "__main__":
    multi()
    plt.show()
