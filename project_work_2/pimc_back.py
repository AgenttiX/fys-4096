""" Backend for PIMC simulations """

import typing as tp

import h5py
import numba
import numpy as np

numba_kwargs = {
    # "parallel": True,
    # "cache": True
}


@numba.jitclass([
    ("Ne", numba.int64),
    ("Re", numba.float64[:, :]),
    ("Me", numba.float64[:]),
    ("spins", numba.int64[:]),
    ("Nn", numba.int64),
    ("Rn", numba.float64[:, :]),
    ("Zn", numba.float64[:]),
    ("tau", numba.float64),
    ("sys_dim", numba.int64),
    # ("dim", numba.int64)
])
class Walker:
    """ A Monte-Carlo walker

    The state of the particles at some moment in imaginary time
    """
    def __init__(
            self,
            # Ne: int,
            Re: np.ndarray,
            Me: np.ndarray,
            spins: np.ndarray,
            # Nn: int,
            Rn: np.ndarray,
            Zn: np.ndarray,
            # tau: float,
            # dim: int
            ):
        self.Ne = Re.shape[0]
        self.Re = Re
        self.Me = Me
        self.spins = spins
        self.Nn = Rn.shape[0]
        self.Rn = Rn
        self.Zn = Zn
        # self.tau = tau
        self.sys_dim = Re.shape[1]

    def w_copy(self) -> "Walker":
        return Walker(
            # self.Ne,
            self.Re.copy(),
            self.Me,
            self.spins.copy(),
            # self.Nn,
            self.Rn.copy(),
            self.Zn.copy(),
            # self.tau,
            # self.sys_dim
        )

    def test_validity(self):
        if self.Ne < 1:
            raise ValueError
        elif self.sys_dim < 1:
            raise ValueError
        elif self.Re.shape != (self.Ne, self.sys_dim):
            raise ValueError
        elif self.Me.shape != (self.Ne, ):
            raise ValueError
        elif self.spins.shape != (self.Ne, ):
            raise ValueError
        elif self.Nn < 0:
            raise ValueError
        # elif self.tau <= 0:
        #     raise ValueError


class Run:
    """ A simulation run

     Includes both input and output of the simulation
     """
    def __init__(
            self, walker: Walker,
            n_walkers: int, n_iters: int, n_blocks: int, tau: float,
            potential: tp.Union[callable, str],
            obs_interval: int = 5, cell_size: float = 0.0,
            from_file: bool = False):

        self.is_from_file = from_file
        self.file: tp.Optional[h5py.File] = None

        walker.test_validity()
        self.walker = walker.w_copy()
        self.walkers: tp.Optional[tp.List[Walker]] = None
        self.walkers_out: tp.Optional[tp.List[Walker]] = None
        self.n_walkers = n_walkers

        self.n_iters = n_iters
        self.n_blocks = n_blocks
        self.potential = potential
        self.obs_interval = obs_interval
        self.cell_size = cell_size

        self.tau = tau
        self.sys_dim = self.walker.sys_dim

        self.energy: tp.Optional[np.ndarray] = None
        self.accept: tp.Optional[np.ndarray] = None
        self.positions: tp.Optional[np.ndarray] = None

    def __del__(self):
        if self.file is not None:
            self.file.close()

    @classmethod
    def from_file(cls, path: str) -> "Run":
        """ Load data from a file """
        with h5py.File(path, "r") as f:
            walker = f["walker"]
            run = Run(
                walker=Walker(
                    # Ne=walker.attrs["Ne"],
                    Re=np.array(walker["Re"]),
                    Me=np.array(walker["Me"]),
                    spins=np.array(walker["spins"]),
                    # Nn=walker.attrs["Nn"],
                    Rn=np.array(walker["Rn"]),
                    Zn=np.array(walker["Zn"]),
                    # tau=f.attrs["tau"]
                ),
                n_walkers=f.attrs["n_walkers"],
                n_blocks=f.attrs["n_blocks"],
                n_iters=f.attrs["n_iters"],
                tau=f.attrs["tau"],
                obs_interval=f.attrs["obs_interval"],
                cell_size=f.attrs["cell_size"],
                potential=f.attrs["potential"],
                from_file=True
            )
            run.set_results(
                energy=np.array(f["energy"]),
                accept=np.array(f["acceptance"]),
                positions=np.array(f["positions"])
            )
        return run

    def generate_walkers(self) -> None:
        if self.walkers is None:
            self.walkers = []
            for i in range(self.n_walkers):
                self.walkers.append(self.walker.w_copy())

    def run(self) -> None:
        if self.walkers is None:
            self.generate_walkers()
        self.walkers_out, self.energy, self.accept, self.positions = pimc(
            n_blocks=self.n_blocks,
            n_iters=self.n_iters,
            tau=self.tau,
            walkers=self.walkers,
            potential=self.potential,
            obs_interval=self.obs_interval,
            cell_size=self.cell_size
        )

    def run_and_save(self, path: str) -> None:
        self.generate_walkers()
        self.save_config(path)
        self.run()
        self.save_results()

    def save_config(self, path: str) -> None:
        if self.file is not None:
            raise Exception("This run should not yet be associated with a file")

        self.file = h5py.File(path, "w")
        f = self.file   # Shorter
        f.attrs["n_blocks"] = self.n_blocks
        f.attrs["n_iters"] = self.n_iters
        f.attrs["obs_interval"] = self.obs_interval
        f.attrs["cell_size"] = self.cell_size
        f.attrs["potential"] = str(self.potential)

        n_walkers = self.n_walkers
        f.attrs["n_walkers"] = n_walkers
        f.attrs["sys_dim"] = self.sys_dim
        f.attrs["tau"] = self.tau

        walker = f.create_group("walker")
        walker.attrs["Ne"] = self.walker.Ne
        Re = walker.create_dataset("Re", data=self.walker.Re)
        Me = walker.create_dataset("Me", data=self.walker.Me)
        walker.create_dataset("spins", data=self.walker.spins)
        walker.attrs["Nn"] = self.walker.Nn
        Rn = walker.create_dataset("Rn", data=self.walker.Rn)
        Zn = walker.create_dataset("Zn", data=self.walker.Zn)
        Re.attrs["info"] = "Starting positions of electrons/atoms"
        Me.attrs["info"] = "Masses of the electrons/atoms"
        Rn.attrs["info"] = "Starting positions of nuclei"
        Zn.attrs["info"] = "Proton numbers of the nuclei"

        f.attrs["temperature"] = temp_kelvin(n_walkers, self.tau)

    def set_results(self, energy: np.ndarray, accept: np.ndarray, positions: np.ndarray) -> None:
        self.energy = energy
        self.accept = accept
        self.positions = positions

    def save_results(self) -> None:
        if self.file is None:
            raise Exception("This run should already be associated with a file")

        self.file.create_dataset("energy", data=self.energy)
        self.file.create_dataset("acceptance", data=self.accept)
        self.file.create_dataset("positions", data=self.positions)

    def temperature(self) -> float:
        return temp_kelvin(self.n_walkers, self.tau)


@numba.njit(**numba_kwargs)
def kinetic_action(r1: np.ndarray, r2: np.ndarray, tau: float, lambda1: float) -> float:
    """ The latter part of slideset 10 equation 15 """
    return np.sum((r1 - r2) ** 2) / (lambda1 * tau * 4)


@numba.njit(**numba_kwargs)
def potential_action(
        walkers: tp.List[Walker],
        time_slice1: int, time_slice2: int,
        tau: float, potential: callable) -> float:
    """ Slideset 10 equation 16 (without the error term) """
    return 0.5 * tau * (potential(walkers[time_slice1]) + potential(walkers[time_slice2]))


@numba.njit(**numba_kwargs)
def bisection(
        r0: float, r1: float, r2: float,
        sys_dim: int, sigma: float, sigma2: float, cell_size: float) -> tp.Tuple[np.ndarray, float, float]:
    """ Bisection sampling using normal distribution"""
    r02_ave = (r0 + r2) / 2
    # NOPE Equation 15 (the former part substracts away)
    # Ilkka's PhD thesis, equation 2.23
    log_S_Rp_R = -np.sum((r1 - r02_ave) ** 2) / (2 * sigma2)
    # Rp is moved from r02_ave by a Gaussian random value
    Rp = r02_ave + np.random.randn(sys_dim) * sigma
    if cell_size != 0.0:
        # If the particle goes outside the cell, move it to the opposite side
        cell_max = cell_size / 2
        Rp = (Rp + cell_max) % cell_size - cell_max
    # Ilkka's PhD thesis, equation 2.22
    log_S_R_Rp = -np.sum((Rp - r02_ave) ** 2) / (2 * sigma2)

    return Rp, log_S_R_Rp, log_S_Rp_R


@numba.njit
def pimc(
        n_blocks: int, n_iters: int, tau: float,
        walkers: tp.List[Walker],
        potential: tp.Callable[[Walker], float],
        obs_interval: int,
        cell_size: float) -> tp.Tuple[tp.List[Walker], np.ndarray, np.ndarray, np.ndarray]:
    M = len(walkers)
    Ne = walkers[0].Ne * 1
    sys_dim = 1 * walkers[0].sys_dim
    # tau = 1.0 * walkers[0].tau

    # lambda1 = 0.5
    lambda1 = 1/(2*walkers[0].Me[0])

    Eb = np.zeros((n_blocks,))
    accept = np.zeros((n_blocks,))
    acc_count = np.zeros((n_blocks,))
    sigma2 = lambda1 * tau
    sigma = np.sqrt(sigma2)

    positions = np.zeros((n_blocks, M*Ne, sys_dim))

    for i in range(n_blocks):
        EbCount = 0
        for j in range(n_iters):
            # Walker and particle indices
            time_slice0 = int(np.random.rand() * M)
            time_slice1 = (time_slice0 + 1) % M
            time_slice2 = (time_slice1 + 1) % M
            ptcl_index = int(np.random.rand() * Ne)

            # Positions of the particle at the given times
            r0 = walkers[time_slice0].Re[ptcl_index]
            r1 = 1.0 * walkers[time_slice1].Re[ptcl_index]
            r2 = walkers[time_slice2].Re[ptcl_index]

            KineticActionOld = kinetic_action(r0, r1, tau, lambda1) + \
                               kinetic_action(r1, r2, tau, lambda1)
            PotentialActionOld = potential_action(walkers, time_slice0, time_slice1, tau, potential) + \
                                 potential_action(walkers, time_slice1, time_slice2, tau, potential)

            # bisection sampling
            Rp, log_S_R_Rp, log_S_Rp_R = bisection(r0, r1, r2, sys_dim, sigma, sigma2, cell_size)

            walkers[time_slice1].Re[ptcl_index] = 1.0 * Rp
            KineticActionNew = kinetic_action(r0, Rp, tau, lambda1) + \
                               kinetic_action(Rp, r2, tau, lambda1)
            PotentialActionNew = potential_action(walkers, time_slice0, time_slice1, tau, potential) + \
                                 potential_action(walkers, time_slice1, time_slice2, tau, potential)

            deltaK = KineticActionNew - KineticActionOld
            deltaU = PotentialActionNew - PotentialActionOld
            # print('delta K', deltaK)
            # print('delta logS', log_S_R_Rp-log_S_Rp_R)
            # print('np.exp(dS-dK)', np.exp(log_S_Rp_R-log_S_R_Rp-deltaK))
            # print('deltaU', deltaU)
            q_R_Rp = np.exp(log_S_Rp_R - log_S_R_Rp - deltaK - deltaU)
            A_RtoRp = min(1.0, q_R_Rp)
            if A_RtoRp > np.random.rand():
                accept[i] += 1.0
            else:
                walkers[time_slice1].Re[ptcl_index] = 1.0 * r1
            acc_count[i] += 1
            if j % obs_interval == 0:
                E_kin, E_pot = energy(walkers, potential, tau)
                # print(E_kin,E_pot)
                Eb[i] += E_kin + E_pot
                EbCount += 1

        Eb[i] /= EbCount
        accept[i] /= acc_count[i]
        # print('Block {0}/{1}'.format(i + 1, n_blocks))
        # print('    E   = {0:.5f}'.format(Eb[i]))
        # print('    Acc = {0:.5f}'.format(accept[i]))
        if i % 10 == 0:
            print("Block", i+1, "/", n_blocks)
            print("    E   =", Eb[i])
            print("    Acc =", accept[i])

        for i_w, walker in enumerate(walkers):
            positions[i, i_w*Ne:(i_w+1)*Ne, :] = walker.Re

    return walkers, Eb, accept, positions


@numba.njit(**numba_kwargs)
def energy(walkers: tp.List[Walker], potential: callable, tau: float):
    """ Compute average walker energy"""
    M = len(walkers)
    d = 1.0 * walkers[0].sys_dim
    # tau = walkers[0].tau
    lambda1 = 0.5
    U = 0.0
    K = 0.0
    for i in range(M):
        U += potential(walkers[i])
        for j in range(walkers[i].Ne):
            if i < M - 1:
                K += d / 2 / tau - np.sum((walkers[i].Re[j] - walkers[i + 1].Re[j]) ** 2) / 4 / lambda1 / tau ** 2
            else:
                K += d / 2 / tau - np.sum((walkers[i].Re[j] - walkers[0].Re[j]) ** 2) / 4 / lambda1 / tau ** 2
    return K / M, U / M


def temp_kelvin(n_walkers: int, tau: float) -> float:
    kb = 3.116801e-6    # Hartree / Kelvin
    return 1 / (kb * n_walkers * tau)


def get_tau(n_walkers: int, temperature: float) -> float:
    kb = 3.116801e-6
    return 1 / (kb * n_walkers * temperature)


def gen_random_pos(count: int, dim: int, max_dist: float):
    return np.random.uniform(low=-max_dist, high=max_dist, size=(count, dim))


def run_multi(configs: tp.List[Run]):
    """ Run multiple simulations in parallel """

    n_blocks = np.array([c.n_blocks for c in configs])
    if not np.all(n_blocks == n_blocks[0]):
        raise ValueError("Block counts should be same")
    sys_dims = np.array([c.sys_dim for c in configs])
    if not np.all(sys_dims == sys_dims[0]):
        raise ValueError("Sys dims should be same")
    n_es = np.array([c.walker.Ne for c in configs])
    if not np.all(n_es == n_es[0]):
        raise ValueError("Electron/atom counts should be same")
    n_walkers = np.array([c.n_walkers for c in configs])
    if not np.all(n_walkers == n_walkers[0]):
        raise ValueError("Walker counts should be same")

    n_iters = np.array([c.n_iters for c in configs])
    taus = np.array([c.tau for c in configs])
    walkers = [c.walker for c in configs]
    potentials = [c.potential for c in configs]
    obs_intervals = np.array([c.obs_interval for c in configs])
    cell_sizes = np.array([c.cell_size for c in configs])

    ener, accept, positions = run_multi_core(
        n_blocks=n_blocks[0],
        n_iters=n_iters,
        n_walkers=n_walkers[0],
        taus=taus,
        walkers=walkers,
        potentials=potentials,
        obs_intervals=obs_intervals,
        cell_sizes=cell_sizes
    )

    for i in range(len(configs)):
        configs[i].set_results(
            energy=ener[i, ...],
            accept=accept[i, ...],
            positions=positions[i, ...]
        )


@numba.njit(parallel=True)
def run_multi_core(
        n_blocks: int, n_iters: np.ndarray, n_walkers: int, taus: np.ndarray,
        walkers: tp.List[Walker], potentials: tp.List[callable], obs_intervals: np.ndarray, cell_sizes: np.ndarray) \
        -> tp.Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """ Backend for parallel simulation """
    n_sims = len(walkers)
    ener = np.zeros((n_sims, n_blocks))
    accept = np.zeros((n_sims, n_blocks))
    positions = np.zeros((n_sims, n_blocks, n_walkers*walkers[0].Ne, walkers[0].sys_dim))

    for i in numba.prange(n_sims):
        walkers_out, ener[i, ...], accept[i, ...], positions[i, ...] = run_with_copy(n_blocks, n_iters[i], n_walkers, taus[i], walkers[i], potentials[i], obs_intervals[i], cell_sizes[i])

    return ener, accept, positions


@numba.njit
def run_with_copy(
        n_blocks: int, n_iters: int, n_walkers: int, tau: float,
        walker: Walker, potential: callable, obs_interval: int, cell_size: float):
    walkers_copy = []
    for i in range(n_walkers):
        walkers_copy.append(walker.w_copy())
    return pimc(n_blocks, n_iters, tau, walkers_copy, potential, obs_interval, cell_size)


def density(m_atom: float, n_atoms: int, cell_size: float):
    volume = cell_size**3
    mass = m_atom * n_atoms
    return mass / volume
