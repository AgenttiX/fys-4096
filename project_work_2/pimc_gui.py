""" Graphical user interface for analysing path integral Monte Carlo data

For enabling OpenGL in Python please see
http://www.pyqtgraph.org/documentation/3dgraphics/index.html
or just install the package "pyopengl" using pip
"""

import threading
# import time
import typing as tp

import matplotlib.pyplot as plt
import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl
# import PyQt5 as qt

from project_work_2.pimc_back import Run

app = pg.mkQApp()


class PimcGui:
    def __init__(
            self, run: Run,
            title: str = "Path integral Monte Carlo",
            win_size: tp.Tuple[int, int] = (1200, 700)):

        self.__run = run
        self.__positions = run.positions
        self.__ne = run.walker.Ne

        self.__fps = 10
        self.__video_ind = self.__positions.shape[0]
        self.__video_timer = pg.QtCore.QTimer()

        pg.setConfigOptions(
            antialias=True,
            background="w",
            foreground="k"
        )

        win = pg.GraphicsWindow()
        win.resize(*win_size)
        win.setWindowTitle(title)

        layout = pg.QtGui.QGridLayout()
        lo_plots = pg.QtGui.QGridLayout()
        lo_controls = pg.QtGui.QGridLayout()
        layout.addLayout(lo_plots, 0, 1)
        layout.addLayout(lo_controls, 1, 0)
        win.setLayout(layout)

        self.__view = gl.GLViewWidget()

        plot_e = pg.PlotWidget(title="Energy")
        plot_e.getPlotItem().plot(run.energy, pen="b")
        plot_acc = pg.PlotWidget(title="Acceptance")
        plot_acc.getPlotItem().plot(run.accept, pen="b")
        plot_acc.setXLink(plot_e)

        layout.addWidget(self.__view, 0, 0)
        lo_plots.addWidget(plot_e, 0, 0)
        lo_plots.addWidget(plot_acc, 1, 0)

        # self.__view.sizeHint = lambda: pg.QtCore.QSize(200, 200)
        # plot_e.sizeHint = lambda: pg.QtCore.QSize(100, 100)
        # plot_acc.sizeHint = lambda: pg.QtCore.QSize(100, 100)

        # self.__view.setSizePolicy(plot_e.sizePolicy())

        # size_policy_type = type(self.__view.sizePolicy())
        # self.__view.setSizePolicy(size_policy_type())

        # plot_e.setSizePolicy(self.__view.sizePolicy())
        # plot_acc.setSizePolicy(self.__view.sizePolicy())

        self.__walker_dots = None
        self.init_3d()
        self.update_3d(self.__video_ind)

        self.__sel_e = pg.InfiniteLine(movable=True)
        self.__sel_acc = pg.InfiniteLine(movable=True)
        self.__sel_e.setValue(self.__video_ind)
        self.__sel_acc.setValue(self.__video_ind)
        self.__sel_e.sigPositionChangeFinished.connect(self.update_by_sel_e)
        self.__sel_acc.sigPositionChangeFinished.connect(self.update_by_sel_acc)
        plot_e.addItem(self.__sel_e)
        plot_acc.addItem(self.__sel_acc)

        self.__input_fps = pg.SpinBox(value=self.__fps, int=True, minStep=1, step=1)
        self.__input_fps.setMinimum(1)
        # self.__input_fps.setSizePolicy(self.__view.sizePolicy())
        lo_controls.addWidget(self.__input_fps, 0, 1)

        fps_text = pg.QtGui.QLabel("FPS")
        lo_controls.addWidget(fps_text, 0, 0)

        self.__anime_button = pg.QtGui.QPushButton("Animate")
        # self.__anime_button.sizeHint = lambda: pg.QtCore.QSize(10, 10)
        # self.__anime_button.setSizePolicy(self.__view.sizePolicy())
        self.__anime_button.clicked.connect(self.animate)
        lo_controls.addWidget(self.__anime_button, 1, 0)

        # print(self.__view.sizePolicy())
        # print(self.__view.sizeHint())
        # policy_type = type(self.__view.sizePolicy())
        # hint_type = type(self.__view.sizeHint())
        # print(policy_type)
        # print(hint_type)
        # self.set_size_policy(self.__view, [plot_e, plot_acc, self.__input_fps, self.__anime_button])

        win.show()
        app.exec_()

    # def set_size_policy(self, source, targets):
    #     policy = source.sizePolicy()
    #     hint = lambda: pg.QtCore.QSize(100, 100)
    #     source.sizeHint = hint
    #
    #     for target in targets:
    #         target.setSizePolicy(policy)
    #         target.sizeHint = hint

    def animate(self):
        if self.__video_timer.isActive():
            self.__video_timer.stop()
            return

        self.__fps = self.__input_fps.value()
        if self.__video_ind >= self.__positions.shape[0] - 1:
            self.__video_ind = 0
        else:
            self.__video_ind = self.__sel_e.value()

        interval = int(round(1000 / self.__fps))
        self.__video_timer.setInterval(interval)
        print(self.__video_timer.interval())
        self.__video_timer.timeout.connect(self.animate_engine)
        self.__video_timer.start()

    def animate_engine(self):
        i = self.__video_ind
        if i >= self.__positions.shape[0] - 1:
            self.__video_timer.stop()

        self.__sel_e.setValue(i)
        self.__sel_acc.setValue(i)
        self.update_3d(i)
        self.__video_ind += 1

    def update_by_sel_e(self):
        self.__video_timer.stop()
        pos = self.__sel_e.value()
        self.__sel_acc.setValue(pos)
        self.update_3d(pos)

    def update_by_sel_acc(self):
        self.__video_timer.stop()
        pos = self.__sel_acc.value()
        self.__sel_e.setValue(pos)
        self.update_3d(pos)

    def init_3d(self):
        self.__view.opts["distance"] = self.__run.cell_size
        grid_x = gl.GLGridItem()
        grid_y = gl.GLGridItem()
        grid_z = gl.GLGridItem()
        grid_x.rotate(90, 0, 1, 0)
        grid_y.rotate(90, 1, 0, 0)
        self.__view.addItem(grid_x)
        self.__view.addItem(grid_y)
        self.__view.addItem(grid_z)

        cls = self.__run.cell_size
        border = gl.GLGridItem()
        border.setSize(x=cls, y=cls, z=cls)
        border.setSpacing(x=10, y=10, z=10)
        self.__view.addItem(border)

        self.__walker_dots = gl.GLScatterPlotItem()
        self.__view.addItem(self.__walker_dots)

    def update_3d(self, time_index: int):
        time_index = int(round(time_index))
        if time_index < 0:
            time_index = 0
        elif time_index >= self.__positions.shape[0]:
            time_index = self.__positions.shape[0] - 1

        ne = self.__ne
        n_walkers = self.__run.n_walkers
        # dim = self.__positions.shape[2]

        count = self.__positions.shape[1]
        pos = self.__positions[time_index]

        size = np.ones((count,)) * 10
        color = np.repeat(np.array([[0.5, 0.5, 0.5, 0.5]]), count, axis=0)
        color[:ne, :] = np.repeat(np.array([[1, 0, 0, 1]], dtype=np.float_), ne, axis=0)

        g_start = None
        if n_walkers == 2:
            g_start = n_walkers * ne
        elif n_walkers >= 3:
            g_start = (n_walkers // 3) * ne
            b_start = g_start * 2
            color[b_start:b_start + ne, :] = np.repeat(np.array([[0, 0, 1, 1]], dtype=np.float_), ne, axis=0)

        if g_start is not None:
            color[g_start:g_start + ne, :] = np.repeat(np.array([[0, 1, 0, 1]], dtype=np.float_), ne, axis=0)

        self.__view.removeItem(self.__walker_dots)
        self.__walker_dots = gl.GLScatterPlotItem(pos=pos, size=size, color=color)
        self.__view.addItem(self.__walker_dots)


def examples():
    import pyqtgraph.examples
    pyqtgraph.examples.run()


def plot_obs(energy: np.ndarray, accept: np.ndarray):
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.plot(energy)
    ax1.set_xlabel("Block")
    ax1.set_ylabel("E")
    ax1.set_title("Energy")
    ax2.plot(accept)
    ax2.set_xlabel("Block")
    ax2.set_ylabel("Acceptance")
    ax2.set_title("Acceptance")


def show_qt(threaded: bool = False):
    if threaded:
        thr = threading.Thread(target=app.exec_)
        thr.run()
    else:
        app.exec_()


def show_mpl():
    plt.show()


def main():
    import os.path
    # You can change the path here!
    # name = "multi_temp_long/classical/temp_300_classical.hdf5"
    # name = "multi_temp_long/quantum/temp_4000_quantum.hdf5"
    name = "multi_temp_long/classical/temp_700_classical.hdf5"
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), name)

    run = Run.from_file(path)
    print("Temperature", run.temperature())
    print("Tau", run.tau)
    PimcGui(run)
    # show_qt()


if __name__ == "__main__":
    # examples()
    main()
