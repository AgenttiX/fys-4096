""" Functions for PIMC """

import numba
import numpy as np

from project_work_2.pimc_back import Walker

numba_kwargs = {
    # "parallel": True
    # "cache": True,
}


def potential_morse(De: float, a: float, re: float):
    """ Generate a Morse potential function with the given parameters

    :param De: dissociation energy
    :param a: sqrt(k/(2*De) where k is the force constant
    :param re: bond length
    :return: potential function
    """
    @numba.njit(**numba_kwargs)
    def potential(walker: Walker) -> float:
        V = 0.0

        for i in range(walker.Ne - 1):
            for j in range(i+1, walker.Ne):
                r = np.sqrt(np.sum((walker.Re[i, :] - walker.Re[j, :]) ** 2))
                V += De * (1 - np.exp(-a*(r-re)))**2 - De
        return V

    return potential


@numba.njit(**numba_kwargs)
def coulomb_qd(walker: Walker) -> float:
    """ Example Coulomb potential for electrons in a quantum dot """
    V = 0.0
    r_cut = 1.0e-12
    # e-e
    for i in range(walker.Ne - 1):
        for j in range(i + 1, walker.Ne):
            r = np.sqrt(np.sum((walker.Re[i] - walker.Re[j]) ** 2))
            V += 1.0 / max(r_cut, r)

    Vext = external_potential_qd(walker)

    return V + Vext


@numba.njit(**numba_kwargs)
def external_potential_qd(walker: Walker) -> float:
    """ Example potential of a quantum dot"""
    V = 0.0

    for i in range(walker.Ne):
        V += 0.5 * np.sum(walker.Re[i] ** 2)

    """
    r_cut = 1.0e-12
    # e-Ion
    sigma = 0.05
    for i in range(walker.Ne):
        for j in range(walker.Nn):
            r = max(r_cut,np.sqrt(np.sum((walker.Re[i]-walker.Rn[j])**2)))
            #V -= walker.Zn[j]/max(r_cut,r)
            V -= walker.Zn[j]*erf(r/np.sqrt(2.0*sigma))/r

    # Ion-Ion
    for i in range(walker.Nn-1):
        for j in range(i+1,walker.Nn):
            r = np.sqrt(np.sum((walker.Rn[i]-walker.Rn[j])**2))
            V += walker.Zn[i]*walker.Zn[j]/max(r_cut,r)
    """

    return V
