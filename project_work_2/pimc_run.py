""""
Configuration for path integral Monte Carlo simulations

This should be used similarly as the Nexus workflow files
"""

import os.path

import numpy as np
import matplotlib.pyplot as plt

from project_work_2 import pimc_back, pimc_lib


def example():
    """ The original example by Ilkka

    For testing if the code has bugs
    Correct should be something like:
    PIMC total energy: 2.93373 +/- 0.08641
    Variance to energy ratio: 0.39516
    """

    """
    # For H2
    walkers.append(Walker(Ne=2,
                          Re=[np.array([0.5,0,0]),np.array([-0.5,0,0])],
                          spins=[0,1],
                          Nn=2,
                          Rn=[np.array([-0.7,0,0]),np.array([0.7,0,0])],
                          Zn=[1.0,1.0],
                          tau = 0.5,
                          dim=3))
    """

    M = 100

    # For 2D quantum dot
    run = pimc_back.Run(
        walker=pimc_back.Walker(
            # Ne=2,
            Re=np.array([[0.5, 0], [-0.5, 0]]),
            Me=np.array([1.0, 1.0]),
            spins=np.array([0, 1]),
            # Nn=2,  # not used
            Rn=np.array([[-0.7, 0], [0.7, 0]]),  # not used
            Zn=np.array([1.0, 1.0]),  # not used
            # tau=0.1,
            # dim=2
        ),
        n_walkers=M,
        n_blocks=200,
        n_iters=100,
        potential=pimc_lib.coulomb_qd,
        tau=0.1,
    )
    run.run()

    Eb = run.energy
    # Acc = run.accept

    plt.plot(Eb)
    Eb = Eb[20:]
    print('PIMC total energy: {0:.5f} +/- {1:0.5f}'.format(np.mean(Eb), np.std(Eb) / np.sqrt(len(Eb))))
    print('Variance to energy ratio: {0:.5f}'.format(np.abs(np.std(Eb) / np.mean(Eb))))
    plt.show()


def testing():
    electron_mass = 5.48579909070e-4   # u
    hydrogen_mass = 1.008
    mass_ratio = hydrogen_mass / electron_mass
    print("Mass ratio of H and e:", mass_ratio)

    cell_size = 50
    n_walkers = 8
    temperature = 300
    start_pos = 10
    tau = pimc_back.get_tau(n_walkers, temperature)
    temp2 = pimc_back.temp_kelvin(n_walkers, tau)
    print("Temperature {:.2f} K".format(temp2))

    ne = 30
    walker = pimc_back.Walker(
        # Ne=ne,
        Re=pimc_back.gen_random_pos(ne, 3, cell_size/2),
        Me=np.array([mass_ratio]*ne),
        spins=np.array([0]*ne),
        # Nn=0,
        Rn=np.array([[]]),
        Zn=np.array([]),
        # tau=tau,
        # dim=3
    )

    a0 = 1
    potential = pimc_lib.potential_morse(
        De=0.1745,  # Hartree
        re=1.40*a0,
        a=1.0282/a0
    )

    run = pimc_back.Run(
        walker=walker,
        n_walkers=n_walkers,
        n_blocks=2000,
        n_iters=30,
        potential=potential,
        cell_size=cell_size,
        tau=tau
    )
    run.run_and_save("testing.hdf5")


def multi_temp():
    """ Run both classical and quantum simulations for several temperatures """
    electron_mass = 5.48579909070e-4  # u
    hydrogen_mass = 1.008
    mass_ratio = hydrogen_mass / electron_mass
    print("Mass ratio of H and e:", mass_ratio)

    # The long run was calculated with this value
    # n_blocks = 50000
    n_blocks = 2000
    n_iters = 50
    cell_size = 50
    n_walkers = 8
    temperatures = np.array([300, 500, 700, 1000, 1500, 2000, 3000, 4000])

    start_pos = 10
    # re = np.array([
    #     [-1, -1, -1],
    #     [-1, -1, 1],
    #     [-1, 1, -1],
    #     [-1, 1, 1],
    #     [1, -1, -1],
    #     [1, -1, 1],
    #     [1, 1, -1],
    #     [1, 1, 1]
    # ], dtype=np.float_) * start_pos
    re = np.array([
        [-1, -1, -1],
        [-1.1, -1.1, -1.1],
        [-1, 1, 1],
        [-1.1, 1.1, 1.1],
        [1, -1, -1],
        [1.1, -1.1, -1.1],
        [1, 1, 1],
        [1.1, 1.1, 1.1],
    ]) * start_pos

    walker = pimc_back.Walker(
        Re=re,
        Me=np.array([mass_ratio]*8),
        spins=np.array([0]*8),
        Rn=np.array([[]]),
        Zn=np.array([]),
    )

    n_atoms = walker.Ne
    m_hydrogen_kg = 1.008 * 1.660539040e-27
    a0_m = 5.29e-11
    density = pimc_back.density(m_atom=m_hydrogen_kg, n_atoms=n_atoms, cell_size=cell_size * a0_m)
    print("Density: {:.3f} kg/m^3 (g/l)".format(density))

    a0 = 1
    potential = pimc_lib.potential_morse(
        De=0.1745,  # Hartree
        re=1.40 * a0,
        a=1.0282 / a0
    )

    runs_classical = []
    for i in range(temperatures.size):
        runs_classical.append(pimc_back.Run(
            walker=walker,
            n_walkers=1,
            n_iters=n_iters,
            n_blocks=n_blocks,
            tau=pimc_back.get_tau(1, temperatures[i]),
            potential=potential,
            cell_size=cell_size
        ))

    runs_quantum = []
    for i in range(temperatures.size):
        runs_quantum.append(pimc_back.Run(
            walker=walker,
            n_walkers=n_walkers,
            n_iters=n_iters,
            n_blocks=n_blocks,
            tau=pimc_back.get_tau(n_walkers, temperatures[i]),
            potential=potential,
            cell_size=cell_size
        ))

    path = os.path.dirname(os.path.abspath(__file__))
    path_folder = os.path.join(path, "multi_temp")
    path_classical = os.path.join(path_folder, "classical")
    path_quantum = os.path.join(path_folder, "quantum")
    if not os.path.exists(path_folder):
        os.mkdir(path_folder)
    if not os.path.exists(path_classical):
        os.mkdir(path_classical)
    if not os.path.exists(path_quantum):
        os.mkdir(path_quantum)

    for i in range(len(runs_classical)):
        runs_classical[i].save_config(path=os.path.join(path_classical, "temp_{}_classical.hdf5".format(temperatures[i])))
        runs_quantum[i].save_config(path=os.path.join(path_quantum, "temp_{}_quantum.hdf5".format(temperatures[i])))

    print("Running classical")
    pimc_back.run_multi(runs_classical)
    print("Running quantum")
    pimc_back.run_multi(runs_quantum)

    for i in range(len(runs_classical)):
        runs_classical[i].save_results()
        runs_quantum[i].save_results()

    print("Ready")


if __name__ == "__main__":
    # example()
    # testing()
    multi_temp()
