"""
Cubic hermite splines in 1d, 2d, and 3d

Related to FYS-4096 Computational Physics
exercise 2-5 assignments.

By Ilkka Kylanpaa on January 2019
Edited by Mika Mäki on April 2019

Now properly parallelised!
"""

import os
os.environ["NUMBA_WARNINGS"] = "1"

import numba
import numpy as np
import matplotlib.pyplot as plt


# Both parallelism and caching cannot be enabled at the same time due to the limitations of Numba
CACHE = False
PARALLEL = True


# Distance functions

@numba.njit(cache=CACHE)
def p1(t: float) -> float:
    return (1. + 2. * t) * (t - 1.) ** 2


@numba.njit(cache=CACHE)
def p2(t: float) -> float:
    return t ** 2 * (3. - 2. * t)


@numba.njit(cache=CACHE)
def q1(t: float) -> float:
    return t * (t - 1.) ** 2


@numba.njit(cache=CACHE)
def q2(t: float) -> float:
    return t ** 2 * (t - 1.)


# Initialization

@numba.njit(parallel=PARALLEL, cache=CACHE)
def init_1d_spline(x: np.ndarray, f: np.ndarray, h: np.ndarray) -> np.ndarray:
    # now using complete boundary conditions
    # - natural boundary conditions commented
    a = np.zeros((len(x),))
    b = np.zeros((len(x),))
    c = np.zeros((len(x),))
    d = np.zeros((len(x),))
    fx = np.zeros((len(x),))

    # a[0]=1.0 # not needed
    b[0] = 1.0

    # natural boundary conditions
    # c[0]=0.5
    # d[0]=1.5*(f[1]-f[0])/(x[1]-x[0])

    # complete boundary conditions
    c[0] = 0.0
    d[0] = (f[1] - f[0]) / (x[1] - x[0])

    for i in numba.prange(1, len(x)-1):
        d[i] = 6.0 * (h[i] / h[i-1] - h[i-1] / h[i]) * f[i] \
               - 6.0 * h[i] / h[i-1] * f[i-1] \
               + 6.0 * h[i-1] / h[i] * f[i+1]
        a[i] = 2.0 * h[i]
        b[i] = 4.0 * (h[i] + h[i-1])
        c[i] = 2.0 * h[i-1]

    b[-1] = 1.0
    # c[-1]=1.0 # not needed

    # natural boundary conditions
    # a[-1]=0.5
    # d[-1]=1.5*(f[-1]-f[-2])/(x[-1]-x[-2])

    # complete boundary conditions
    a[-1] = 0.0
    d[-1] = (f[-1] - f[-2]) / (x[-1] - x[-2])

    # solve tridiagonal eq. A*f=d
    c[0] = c[0] / b[0]
    d[0] = d[0] / b[0]
    for i in range(1, len(x) - 1):
        temp = b[i] - c[i-1] * a[i]
        c[i] = c[i] / temp
        d[i] = (d[i] - d[i-1] * a[i]) / temp
    # end for

    fx[-1] = d[-1]
    for i in range(len(x) - 2, -1, -1):
        fx[i] = d[i] - c[i] * fx[i+1]
    # end for

    return fx


@numba.njit(parallel=PARALLEL, cache=CACHE)
def init_2d_spline(x: np.ndarray, y: np.ndarray, f: np.ndarray):
    hx = np.diff(x)
    hy = np.diff(y)
    fx = np.zeros(f.shape)
    fy = np.zeros(f.shape)
    fxy = np.zeros(f.shape)
    max_i = np.array([x.size, y.size]).max()
    for i in numba.prange(max_i):
        if i < len(y):
            fx[:, i] = init_1d_spline(x, f[:, i], hx)
        if i < len(x):
            fy[i, :] = init_1d_spline(y, f[i, :], hy)

    for i in range(len(y)):
        fxy[:, i] = init_1d_spline(x, fy[:, i], hx)

    return hx, hy, fx, fy, fxy


@numba.njit(parallel=PARALLEL, cache=CACHE)
def init_3d_spline(x: np.ndarray, y: np.ndarray, z: np.ndarray, f: np.ndarray):
    hx = np.diff(x)
    hy = np.diff(y)
    hz = np.diff(z)
    fx = np.zeros(f.shape)
    fy = np.zeros(f.shape)
    fz = np.zeros(f.shape)
    fxy = np.zeros(f.shape)
    fxz = np.zeros(f.shape)
    fyz = np.zeros(f.shape)
    fxyz = np.zeros(f.shape)
    max_i = np.array([x.size, y.size, z.size]).max()

    for i in numba.prange(max_i):
        for j in range(max_i):
            if i < len(y) and j < len(z):
                fx[:, i, j] = init_1d_spline(x, f[:, i, j], hx)
            if i < len(x) and j < len(z):
                fy[i, :, j] = init_1d_spline(y, f[i, :, j], hy)
            if i < len(x) and j < len(y):
                fz[i, j, :] = init_1d_spline(z, f[i, j, :], hz)

    for i in numba.prange(max_i):
        for j in range(max_i):
            if i < len(y) and j < len(z):
                fxy[:, i, j] = init_1d_spline(x, fy[:, i, j], hx)
            if i < len(y) and j < len(z):
                fxz[:, i, j] = init_1d_spline(x, fz[:, i, j], hx)
            if i < len(x) and j < len(z):
                fyz[i, :, j] = init_1d_spline(y, fz[i, :, j], hy)

    for i in numba.prange(len(y)):
        for j in range(len(z)):
            fxyz[:, i, j] = init_1d_spline(x, fyz[:, i, j], hx)

    return hx, hy, hz, fx, fy, fz, fxy, fxz, fyz, fxyz


""" 
Add smoothing functions 

def smooth1d(x,f,factor=3):
    ...
    ...
    return ...

def smooth2d(x,y,f,factor=3):
    ...
    ...
    return ... 

def smooth3d(x,y,z,f,factor=3):
    ...
    ...
    ...
    return ...
"""


# Evaluation

@numba.njit(parallel=PARALLEL, cache=CACHE)
def eval1d(
        x: np.ndarray,
        self_x: np.ndarray, self_hx: np.ndarray, self_f: np.ndarray, self_fx: np.ndarray) -> np.ndarray:
    # if np.isscalar(x):
    #     x = np.array([x])
    N = len(self_x) - 1
    f = np.zeros((len(x),))
    for ii in numba.prange(len(x)):
        val = x[ii]
        # i = np.floor(np.where(self_x <= val)[0][-1]).astype(int)
        i = int(np.floor(np.where(self_x <= val)[0][-1]))
        if i == N:
            f[ii] = self_f[i]
        else:
            t = (val - self_x[i]) / self_hx[i]
            f[ii] = self_f[i] * p1(t) + self_f[i + 1] * p2(t) + self_hx[i] * (
                    self_fx[i] * q1(t) + self_fx[i + 1] * q2(t))

    return f


@numba.njit(parallel=PARALLEL, cache=CACHE)
def eval2d(
        x: np.ndarray, y: np.ndarray,
        self_x: np.ndarray, self_y: np.ndarray,
        self_hx: np.ndarray, self_hy: np.ndarray,
        self_f: np.ndarray,
        self_fx: np.ndarray, self_fy: np.ndarray,
        self_fxy: np.ndarray) -> np.ndarray:
    # if np.isscalar(x):
    #     x = np.array([x])
    # if np.isscalar(y):
    #     y = np.array([y])
    Nx = len(self_x) - 1
    Ny = len(self_y) - 1
    f = np.zeros((len(x), len(y)))
    # A = np.zeros((4, 4))
    for ii in numba.prange(len(x)):
        valx = x[ii]
        # i = np.floor(np.where(self_x <= valx)[0][-1]).astype(int)
        i = int(np.floor(np.where(self_x <= valx)[0][-1]))
        if i == Nx:
            i -= 1
        A = np.zeros((4, 4))
        for jj, valy in enumerate(y):
            # j = np.floor(np.where(self_y <= valy)[0][-1]).astype(int)
            j = int(np.floor(np.where(self_y <= valy)[0][-1]))
            if j == Ny:
                j -= 1

            u = (valx - self_x[i]) / self_hx[i]
            v = (valy - self_y[j]) / self_hy[j]
            pu = np.array([p1(u), p2(u), self_hx[i] * q1(u), self_hx[i] * q2(u)])
            pv = np.array([p1(v), p2(v), self_hy[j] * q1(v), self_hy[j] * q2(v)])
            A[0, :] = np.array([self_f[i, j], self_f[i, j+1], self_fy[i, j], self_fy[i, j+1]])
            A[1, :] = np.array([self_f[i+1, j], self_f[i+1, j+1], self_fy[i+1, j], self_fy[i+1, j+1]])
            A[2, :] = np.array([self_fx[i, j], self_fx[i, j+1], self_fxy[i, j], self_fxy[i, j+1]])
            A[3, :] = np.array([self_fx[i+1, j], self_fx[i+1, j+1], self_fxy[i+1, j], self_fxy[i+1, j+1]])

            f[ii, jj] = np.dot(pu, np.dot(A, pv))

            # f[ii, jj] = eval_2d_point(
            #     valx, valy, i, j,
            #     self_x, self_y, self_hx, self_hy,
            #     self_f, self_fx, self_fy, self_fxy
            # )

    return f


@numba.njit(parallel=PARALLEL, cache=CACHE)
def eval3d(
        x: np.ndarray, y: np.ndarray, z: np.ndarray,
        self_x: np.ndarray, self_y: np.ndarray, self_z: np.ndarray,
        self_hx: np.ndarray, self_hy: np.ndarray, self_hz: np.ndarray,
        self_f: np.ndarray,
        self_fx: np.ndarray, self_fy: np.ndarray, self_fz: np.ndarray,
        self_fxy: np.ndarray, self_fxz: np.ndarray, self_fyz: np.ndarray,
        self_fxyz: np.ndarray) -> np.ndarray:
    # if np.isscalar(x):
    #     x = np.array([x])
    # if np.isscalar(y):
    #     y = np.array([y])
    # if np.isscalar(z):
    #     z = np.array([z])
    Nx = len(self_x) - 1
    Ny = len(self_y) - 1
    Nz = len(self_z) - 1
    f = np.zeros((len(x), len(y), len(z)))
    for ii in numba.prange(len(x)):
        valx = x[ii]
        # i = np.floor(np.where(self_x <= valx)[0][-1]).astype(int)
        i = int(np.floor(np.where(self_x <= valx)[0][-1]))
        if i == Nx:
            i -= 1

        A = np.zeros((4, 4))
        B = np.zeros((4, 4))
        for jj, valy in enumerate(y):
            # j = np.floor(np.where(self_y <= valy)[0][-1]).astype(int)
            j = int(np.floor(np.where(self_y <= valy)[0][-1]))
            if j == Ny:
                j -= 1
            for kk, valz in enumerate(z):
                # k = np.floor(np.where(self_z <= valz)[0][-1]).astype(int)
                k = int(np.floor(np.where(self_z <= valz)[0][-1]))
                if k == Nz:
                    k -= 1

                u = (valx - self_x[i]) / self_hx[i]
                v = (valy - self_y[j]) / self_hy[j]
                t = (valz - self_z[k]) / self_hz[k]
                pu = np.array([p1(u), p2(u), self_hx[i] * q1(u), self_hx[i] * q2(u)])
                pv = np.array([p1(v), p2(v), self_hy[j] * q1(v), self_hy[j] * q2(v)])
                pt = np.array([p1(t), p2(t), self_hz[k] * q1(t), self_hz[k] * q2(t)])

                B[0, :] = np.array([self_f[i, j, k], self_f[i, j, k+1], self_fz[i, j, k], self_fz[i, j, k+1]])
                B[1, :] = np.array(
                    [self_f[i+1, j, k], self_f[i+1, j, k+1], self_fz[i+1, j, k], self_fz[i+1, j, k+1]])
                B[2, :] = np.array([self_fx[i, j, k], self_fx[i, j, k+1], self_fxz[i, j, k], self_fxz[i, j, k+1]])
                B[3, :] = np.array([
                    self_fx[i+1, j, k], self_fx[i+1, j, k+1], self_fxz[i+1, j, k], self_fxz[i+1, j, k+1]])
                A[:, 0] = np.dot(B, pt)
                B[0, :] = np.array(
                    [self_f[i, j+1, k], self_f[i, j+1, k+1], self_fz[i, j+1, k], self_fz[i, j+1, k+1]])
                B[1, :] = np.array(
                    [self_f[i+1, j+1, k], self_f[i+1, j+1, k+1], self_fz[i+1, j+1, k], self_fz[i+1, j+1, k+1]])
                B[2, :] = np.array(
                    [self_fx[i, j+1, k], self_fx[i, j+1, k+1], self_fxz[i, j+1, k], self_fxz[i, j+1, k+1]])
                B[3, :] = np.array(
                    [self_fx[i+1, j+1, k], self_fx[i+1, j+1, k+1], self_fxz[i+1, j+1, k], self_fxz[i+1, j+1, k+1]])
                A[:, 1] = np.dot(B, pt)

                B[0, :] = np.array([self_fy[i, j, k], self_fy[i, j, k+1], self_fyz[i, j, k], self_fyz[i, j, k+1]])
                B[1, :] = np.array(
                    [self_fy[i+1, j, k], self_fy[i+1, j, k+1], self_fyz[i+1, j, k], self_fyz[i+1, j, k+1]])
                B[2, :] = np.array(
                    [self_fxy[i, j, k], self_fxy[i, j, k+1], self_fxyz[i, j, k], self_fxyz[i, j, k+1]])
                B[3, :] = np.array(
                    [self_fxy[i+1, j, k], self_fxy[i+1, j, k+1], self_fxyz[i+1, j, k], self_fxyz[i+1, j, k+1]])
                A[:, 2] = np.dot(B, pt)
                B[0, :] = np.array(
                    [self_fy[i, j+1, k], self_fy[i, j+1, k+1], self_fyz[i, j+1, k], self_fyz[i, j+1, k+1]])
                B[1, :] = np.array(
                    [self_fy[i+1, j+1, k], self_fy[i+1, j+1, k+1], self_fyz[i+1, j+1, k], self_fyz[i+1, j+1, k+1]])
                B[2, :] = np.array(
                    [self_fxy[i, j+1, k], self_fxy[i, j+1, k+1], self_fxyz[i, j+1, k], self_fxyz[i, j+1, k+1]])
                B[3, :] = np.array(
                    [self_fxy[i+1, j+1, k], self_fxy[i+1, j+1, k+1], self_fxyz[i+1, j+1, k], self_fxyz[i+1, j+1, k+1]])
                A[:, 3] = np.dot(B, pt)

                f[ii, jj, kk] = np.dot(pu, np.dot(A, pv))

                # Slower
                # f[ii, jj, kk] = f[ii] = eval_3d_point(
                #     valx, valy, valz,
                #     i, j, k,
                #     self_x, self_y, self_z,
                #     self_hx, self_hy, self_hz,
                #     self_f, self_fx, self_fy, self_fz,
                #     self_fxy, self_fxz, self_fyz, self_fxyz
                # )
    return f


# Evaluations in smaller dimensions

@numba.njit(parallel=PARALLEL, cache=CACHE)
def eval_2d_point(
        valx, valy, i, j,
        self_x: np.ndarray, self_y: np.ndarray,
        self_hx: np.ndarray, self_hy: np.ndarray,
        self_f: np.ndarray,
        self_fx: np.ndarray, self_fy: np.ndarray,
        self_fxy: np.ndarray):
    """ Do not use! Calling this is much slower than inlining! """
    A = np.zeros((4, 4))
    u = (valx - self_x[i]) / self_hx[i]
    v = (valy - self_y[j]) / self_hy[j]
    pu = np.array([p1(u), p2(u), self_hx[i] * q1(u), self_hx[i] * q2(u)])
    pv = np.array([p1(v), p2(v), self_hy[j] * q1(v), self_hy[j] * q2(v)])
    A[0, :] = np.array([self_f[i, j], self_f[i, j+1], self_fy[i, j], self_fy[i, j+1]])
    A[1, :] = np.array([self_f[i+1, j], self_f[i+1, j+1], self_fy[i+1, j], self_fy[i+1, j+1]])
    A[2, :] = np.array([self_fx[i, j], self_fx[i, j+1], self_fxy[i, j], self_fxy[i, j+1]])
    A[3, :] = np.array([self_fx[i+1, j], self_fx[i+1, j+1], self_fxy[i+1, j], self_fxy[i+1, j+1]])

    return np.dot(pu, np.dot(A, pv))


@numba.njit(parallel=PARALLEL, cache=CACHE)
def eval_3d_point(
        valx: float, valy: float, valz: float,
        i: int, j: int, k: int,
        self_x: np.ndarray, self_y: np.ndarray, self_z: np.ndarray,
        self_hx: np.ndarray, self_hy: np.ndarray, self_hz: np.ndarray,
        self_f: np.ndarray,
        self_fx: np.ndarray, self_fy: np.ndarray, self_fz: np.ndarray,
        self_fxy: np.ndarray, self_fxz: np.ndarray, self_fyz: np.ndarray,
        self_fxyz: np.ndarray):
    """ Do not use! Calling this is much slower than inlining! """
    u = (valx - self_x[i]) / self_hx[i]
    v = (valy - self_y[j]) / self_hy[j]
    t = (valz - self_z[k]) / self_hz[k]
    pu = np.array([p1(u), p2(u), self_hx[i] * q1(u), self_hx[i] * q2(u)])
    pv = np.array([p1(v), p2(v), self_hy[j] * q1(v), self_hy[j] * q2(v)])
    pt = np.array([p1(t), p2(t), self_hz[k] * q1(t), self_hz[k] * q2(t)])
    A = np.zeros((4, 4))
    B = np.zeros((4, 4))
    B[0, :] = np.array([self_f[i, j, k], self_f[i, j, k+1], self_fz[i, j, k], self_fz[i, j, k+1]])
    B[1, :] = np.array(
        [self_f[i+1, j, k], self_f[i+1, j, k+1], self_fz[i+1, j, k], self_fz[i+1, j, k+1]])
    B[2, :] = np.array([self_fx[i, j, k], self_fx[i, j, k+1], self_fxz[i, j, k], self_fxz[i, j, k+1]])
    B[3, :] = np.array([
        self_fx[i+1, j, k], self_fx[i+1, j, k+1], self_fxz[i+1, j, k], self_fxz[i+1, j, k+1]])
    A[:, 0] = np.dot(B, pt)
    B[0, :] = np.array(
        [self_f[i, j+1, k], self_f[i, j+1, k+1], self_fz[i, j+1, k], self_fz[i, j+1, k+1]])
    B[1, :] = np.array(
        [self_f[i+1, j+1, k], self_f[i+1, j+1, k+1], self_fz[i+1, j+1, k], self_fz[i+1, j+1, k+1]])
    B[2, :] = np.array(
        [self_fx[i, j+1, k], self_fx[i, j+1, k+1], self_fxz[i, j+1, k], self_fxz[i, j+1, k+1]])
    B[3, :] = np.array([self_fx[i+1, j+1, k], self_fx[i+1, j+1, k+1], self_fxz[i+1, j+1, k], self_fxz[i+1, j+1, k+1]])
    A[:, 1] = np.dot(B, pt)

    B[0, :] = np.array([self_fy[i, j, k], self_fy[i, j, k+1], self_fyz[i, j, k], self_fyz[i, j, k+1]])
    B[1, :] = np.array(
        [self_fy[i+1, j, k], self_fy[i+1, j, k+1], self_fyz[i+1, j, k], self_fyz[i+1, j, k+1]])
    B[2, :] = np.array(
        [self_fxy[i, j, k], self_fxy[i, j, k+1], self_fxyz[i, j, k], self_fxyz[i, j, k+1]])
    B[3, :] = np.array(
        [self_fxy[i+1, j, k], self_fxy[i+1, j, k+1], self_fxyz[i+1, j, k], self_fxyz[i+1, j, k+1]])
    A[:, 2] = np.dot(B, pt)
    B[0, :] = np.array(
        [self_fy[i, j+1, k], self_fy[i, j+1, k+1], self_fyz[i, j+1, k], self_fyz[i, j+1, k+1]])
    B[1, :] = np.array(
        [self_fy[i+1, j+1, k], self_fy[i+1, j+1, k+1], self_fyz[i+1, j+1, k],
         self_fyz[i+1, j+1, k+1]])
    B[2, :] = np.array(
        [self_fxy[i, j+1, k], self_fxy[i, j+1, k+1], self_fxyz[i, j+1, k], self_fxyz[i, j+1, k+1]])
    B[3, :] = np.array(
        [self_fxy[i+1, j+1, k], self_fxy[i+1, j+1, k+1], self_fxyz[i+1, j+1, k],
         self_fxyz[i+1, j+1, k+1]])
    A[:, 3] = np.dot(B, pt)

    return np.dot(pu, np.dot(A, pv))


@numba.njit(parallel=PARALLEL, cache=CACHE)
def eval1d_for_2d(
        points: np.ndarray,
        self_x: np.ndarray, self_y: np.ndarray,
        self_hx: np.ndarray, self_hy: np.ndarray,
        self_f: np.ndarray,
        self_fx: np.ndarray, self_fy: np.ndarray,
        self_fxy: np.ndarray) -> np.ndarray:
    # if np.isscalar(x):
    #     x = np.array([x])
    # if np.isscalar(y):
    #     y = np.array([y])
    Nx = len(self_x) - 1
    Ny = len(self_y) - 1
    f = np.zeros(points.shape[0])

    for ii in numba.prange(points.shape[0]):
        point = points[ii]
        valx = point[0]
        valy = point[1]
        i = int(np.floor(np.where(self_x <= valx)[0][-1]))
        if i == Nx:
            i -= 1
        j = int(np.floor(np.where(self_y <= valy)[0][-1]))
        if j == Ny:
            j -= 1

        A = np.zeros((4, 4))
        u = (valx - self_x[i]) / self_hx[i]
        v = (valy - self_y[j]) / self_hy[j]
        pu = np.array([p1(u), p2(u), self_hx[i] * q1(u), self_hx[i] * q2(u)])
        pv = np.array([p1(v), p2(v), self_hy[j] * q1(v), self_hy[j] * q2(v)])
        A[0, :] = np.array([self_f[i, j], self_f[i, j+1], self_fy[i, j], self_fy[i, j+1]])
        A[1, :] = np.array([self_f[i+1, j], self_f[i+1, j+1], self_fy[i+1, j], self_fy[i+1, j+1]])
        A[2, :] = np.array([self_fx[i, j], self_fx[i, j+1], self_fxy[i, j], self_fxy[i, j+1]])
        A[3, :] = np.array([self_fx[i+1, j], self_fx[i+1, j+1], self_fxy[i+1, j], self_fxy[i+1, j+1]])

        f[ii] = np.dot(pu, np.dot(A, pv))

        # Slower
        # f[ii] = eval_2d_point(
        #     valx, valy, i, j,
        #     self_x, self_y, self_hx, self_hy,
        #     self_f, self_fx, self_fy, self_fxy
        # )

    return f


@numba.njit(parallel=PARALLEL, cache=CACHE)
def eval1d_for_3d(
        points: np.ndarray,
        self_x: np.ndarray, self_y: np.ndarray, self_z: np.ndarray,
        self_hx: np.ndarray, self_hy: np.ndarray, self_hz: np.ndarray,
        self_f: np.ndarray,
        self_fx: np.ndarray, self_fy: np.ndarray, self_fz: np.ndarray,
        self_fxy: np.ndarray, self_fxz: np.ndarray, self_fyz: np.ndarray,
        self_fxyz: np.ndarray) -> np.ndarray:

    Nx = len(self_x) - 1
    Ny = len(self_y) - 1
    Nz = len(self_z) - 1
    f = np.zeros(points.shape[0], )

    for ii in numba.prange(points.shape[0]):
        point = points[ii, :]
        valx = point[0]
        valy = point[1]
        valz = point[2]
        i = int(np.floor(np.where(self_x <= valx)[0][-1]))
        if i == Nx:
            i -= 1
        j = int(np.floor(np.where(self_y <= valy)[0][-1]))
        if j == Ny:
            j -= 1
        k = int(np.floor(np.where(self_z <= valz)[0][-1]))
        if k == Nz:
            k -= 1

        u = (valx - self_x[i]) / self_hx[i]
        v = (valy - self_y[j]) / self_hy[j]
        t = (valz - self_z[k]) / self_hz[k]
        pu = np.array([p1(u), p2(u), self_hx[i] * q1(u), self_hx[i] * q2(u)])
        pv = np.array([p1(v), p2(v), self_hy[j] * q1(v), self_hy[j] * q2(v)])
        pt = np.array([p1(t), p2(t), self_hz[k] * q1(t), self_hz[k] * q2(t)])
        A = np.zeros((4, 4))
        B = np.zeros((4, 4))
        B[0, :] = np.array([self_f[i, j, k], self_f[i, j, k+1], self_fz[i, j, k], self_fz[i, j, k+1]])
        B[1, :] = np.array(
            [self_f[i+1, j, k], self_f[i+1, j, k+1], self_fz[i+1, j, k], self_fz[i+1, j, k+1]])
        B[2, :] = np.array([self_fx[i, j, k], self_fx[i, j, k+1], self_fxz[i, j, k], self_fxz[i, j, k+1]])
        B[3, :] = np.array([
            self_fx[i+1, j, k], self_fx[i+1, j, k+1], self_fxz[i+1, j, k], self_fxz[i+1, j, k+1]])
        A[:, 0] = np.dot(B, pt)
        B[0, :] = np.array(
            [self_f[i, j+1, k], self_f[i, j+1, k+1], self_fz[i, j+1, k], self_fz[i, j+1, k+1]])
        B[1, :] = np.array(
            [self_f[i+1, j+1, k], self_f[i+1, j+1, k+1], self_fz[i+1, j+1, k],
             self_fz[i+1, j+1, k+1]])
        B[2, :] = np.array(
            [self_fx[i, j+1, k], self_fx[i, j+1, k+1], self_fxz[i, j+1, k], self_fxz[i, j+1, k+1]])
        B[3, :] = np.array(
            [self_fx[i+1, j+1, k], self_fx[i+1, j+1, k+1], self_fxz[i+1, j+1, k], self_fxz[i+1, j+1, k+1]])
        A[:, 1] = np.dot(B, pt)

        B[0, :] = np.array([self_fy[i, j, k], self_fy[i, j, k+1], self_fyz[i, j, k], self_fyz[i, j, k+1]])
        B[1, :] = np.array(
            [self_fy[i+1, j, k], self_fy[i+1, j, k+1], self_fyz[i+1, j, k], self_fyz[i+1, j, k+1]])
        B[2, :] = np.array(
            [self_fxy[i, j, k], self_fxy[i, j, k+1], self_fxyz[i, j, k], self_fxyz[i, j, k+1]])
        B[3, :] = np.array(
            [self_fxy[i+1, j, k], self_fxy[i+1, j, k+1], self_fxyz[i+1, j, k], self_fxyz[i+1, j, k+1]])
        A[:, 2] = np.dot(B, pt)
        B[0, :] = np.array(
            [self_fy[i, j+1, k], self_fy[i, j+1, k+1], self_fyz[i, j+1, k], self_fyz[i, j+1, k+1]])
        B[1, :] = np.array(
            [self_fy[i+1, j+1, k], self_fy[i+1, j+1, k+1], self_fyz[i+1, j+1, k],
             self_fyz[i+1, j+1, k+1]])
        B[2, :] = np.array(
            [self_fxy[i, j+1, k], self_fxy[i, j+1, k+1], self_fxyz[i, j+1, k], self_fxyz[i, j+1, k+1]])
        B[3, :] = np.array(
            [self_fxy[i+1, j+1, k], self_fxy[i+1, j+1, k+1], self_fxyz[i+1, j+1, k],
             self_fxyz[i+1, j+1, k+1]])
        A[:, 3] = np.dot(B, pt)

        f[ii] = np.dot(pu, np.dot(A, pv))

        # Slower
        # f[ii] = eval_3d_point(
        #     valx, valy, valz,
        #     i, j, k,
        #     self_x, self_y, self_z,
        #     self_hx, self_hy, self_hz,
        #     self_f, self_fx, self_fy, self_fz,
        #     self_fxy, self_fxz, self_fyz, self_fxyz
        # )

    return f


# Classes

@numba.jitclass([
    ("x", numba.float64[:]),
    ("f", numba.float64[:]),
    ("hx", numba.float64[:]),
    ("fx", numba.float64[:]),
])
class Spline1D:
    def __init__(self, x: np.ndarray, f: np.ndarray):
        self.x = x
        self.f = f
        self.hx = np.diff(x)
        self.fx = init_1d_spline(self.x, self.f, self.hx)

    def eval1d(self, x: np.ndarray) -> np.ndarray:
        return eval1d(x, self.x, self.hx, self.f, self.fx)


@numba.jitclass([
    ("x", numba.float64[:]),
    ("y", numba.float64[:]),
    ("f", numba.float64[:, :]),
    ("hx", numba.float64[:]),
    ("hy", numba.float64[:]),
    ("fx", numba.float64[:, :]),
    ("fy", numba.float64[:, :]),
    ("fz", numba.float64[:, :]),
    ("fxy", numba.float64[:, :]),
    ("fxz", numba.float64[:, :]),
    ("fyz", numba.float64[:, :]),
])
class Spline2D:
    def __init__(self, x: np.ndarray, y: np.ndarray, f: np.ndarray):
        self.x = x
        self.y = y
        self.f = f
        self.hx, self.hy, self.fx, self.fy, self.fxy = init_2d_spline(x, y, f)

    def eval1d(self, points: np.ndarray):
        return eval1d_for_2d(points, self.x, self.y, self.hx, self.hy, self.f, self.fx, self.fy, self.fxy)

    def eval2d(self, x: np.ndarray, y: np.ndarray):
        return eval2d(x, y, self.x, self.y, self.hx, self.hy, self.f, self.fx, self.fy, self.fxy)


@numba.jitclass([
    ("x", numba.float64[:]),
    ("y", numba.float64[:]),
    ("z", numba.float64[:]),
    ("f", numba.float64[:, :, :]),
    ("hx", numba.float64[:]),
    ("hy", numba.float64[:]),
    ("hz", numba.float64[:]),
    ("fx", numba.float64[:, :, :]),
    ("fy", numba.float64[:, :, :]),
    ("fz", numba.float64[:, :, :]),
    ("fxy", numba.float64[:, :, :]),
    ("fxz", numba.float64[:, :, :]),
    ("fyz", numba.float64[:, :, :]),
    ("fxyz", numba.float64[:, :, :]),
])
class Spline3D:
    def __init__(self, x: np.ndarray, y: np.ndarray, z: np.ndarray, f: np.ndarray):
        self.x = x
        self.y = y
        self.z = z
        self.f = f
        self.hx, self.hy, self.hz, \
            self.fx, self.fy, self.fz, \
            self.fxy, self.fxz, self.fyz, self.fxyz = init_3d_spline(x, y, z, f)

    def eval1d(self, points: np.ndarray):
        return eval1d_for_3d(
            points,
            self.x, self.y, self.z,
            self.hx, self.hy, self.hz,
            self.f,
            self.fx, self.fy, self.fz,
            self.fxy, self.fxz, self.fyz,
            self.fxyz
        )

    def eval3d(self, x: np.ndarray, y: np.ndarray, z: np.ndarray):
        return eval3d(
            x, y, z,
            self.x, self.y, self.z,
            self.hx, self.hy, self.hz,
            self.f,
            self.fx, self.fy, self.fz,
            self.fxy, self.fxz, self.fyz,
            self.fxyz
        )


# Testing

def main():
    # 1d example
    x = np.linspace(0., 2. * np.pi, 20)
    y = np.sin(x)
    spl1d = Spline1D(x=x, f=y)
    xx = np.linspace(0., 2. * np.pi, 100)
    plt.figure()
    # function
    plt.plot(xx, spl1d.eval1d(xx))
    plt.plot(x, y, 'o', xx, np.sin(xx), 'r--')
    plt.title('function')

    # 2d example
    fig = plt.figure()
    ax = fig.add_subplot(121)
    x = np.linspace(0.0, 3.0, 11)
    y = np.linspace(0.0, 3.0, 11)
    X, Y = np.meshgrid(x, y)
    Z = (X + Y) * np.exp(-1.0 * (X * X + Y * Y))
    ax.pcolor(X, Y, Z)
    ax.set_title('original')

    spl2d = Spline2D(x=x, y=y, f=Z)
    # plt.figure()
    ax2 = fig.add_subplot(122)
    x = np.linspace(0.0, 3.0, 51)
    y = np.linspace(0.0, 3.0, 51)
    X, Y = np.meshgrid(x, y)
    Z = spl2d.eval2d(x, y)
    ax2.pcolor(X, Y, Z)
    ax2.set_title('interpolated')

    # 3d example
    x = np.linspace(0.0, 3.0, 10)
    y = np.linspace(0.0, 3.0, 10)
    z = np.linspace(0.0, 3.0, 10)
    X, Y, Z = np.meshgrid(x, y, z)
    F = (X + Y + Z) * np.exp(-1.0 * (X * X + Y * Y + Z * Z))
    X, Y = np.meshgrid(x, y)
    fig3d = plt.figure()
    ax = fig3d.add_subplot(121)
    ax.pcolor(X, Y, F[..., int(len(z) / 2)])
    ax.set_title('original')

    spl3d = Spline3D(x=x, y=y, z=z, f=F)
    x = np.linspace(0.0, 3.0, 50)
    y = np.linspace(0.0, 3.0, 50)
    z = np.linspace(0.0, 3.0, 50)
    X, Y = np.meshgrid(x, y)
    ax2 = fig3d.add_subplot(122)
    F = spl3d.eval3d(x, y, z)
    ax2.pcolor(X, Y, F[..., int(len(z) / 2)])
    ax2.set_title('interpolated')

    # plt.show()


if __name__ == "__main__":
    # import time
    # time1 = time.perf_counter()
    main()
    plt.show()
    # time2 = time.perf_counter()
    # main()
    # time3 = time.perf_counter()
    # print(time2 - time1)
    # print(time3 - time2)
    # eval2d.parallel_diagnostics(level=4)
    # eval3d.parallel_diagnostics(level=4)
    # init_3d_spline.parallel_diagnostics(level=4)
