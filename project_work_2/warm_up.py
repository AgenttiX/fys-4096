""" FYS-4096 project work 2 warm up problem

The spline class version used can be fully parallelised!
"""

import time

import h5py
import matplotlib.pyplot as plt
import numpy as np

from project_work_2 import spline


def read_hdf5_file(path: str):
    with h5py.File(path, "r") as f:
        # print('Keys in hdf5 file: ', list(f.keys()))
        return np.array(f["x_grid"]), np.array(f["y_grid"]), np.array(f["data"])


def save_vec_to_hdf5(path: str, vec: np.ndarray):
    with h5py.File(path, "w") as f:
        vec_set = f.create_dataset("vec", data=vec, dtype="f")
        vec_set.attrs["info"] = "Interpolated vector"


def vector_between_points(r0: np.ndarray, r1: np.ndarray, points: int) -> np.ndarray:
    """Linspace between two points in n-dimensional space

    Modified from exercise 4 problem 3
    """
    if r0.ndim != 1:
        raise ValueError
    elif r0.shape != r1.shape:
        raise ValueError
    elif points < 1:
        raise ValueError

    # This is a rather hacky approach, but hey, it's a one-liner!
    # The outer product creates a matrix by multiplying two vectors
    # (compare this to inner product aka. dot product that for vectors results in a scalar)
    # To this matrix of the difference vector points we need to add the starting point, and np.newaxis is used to make
    # the dimensions match
    ret = np.transpose(np.outer(r1 - r0, np.linspace(0, 1, points)) + r0[:, np.newaxis])

    if not np.all(np.isclose(ret[0, :], r0)):
        raise ValueError(ret[0, :], r0)
    elif not np.all(np.isclose(ret[-1, :], r1)):
        raise ValueError(ret[-1, :], r1)

    return ret


def find_nearest_idx(array: np.ndarray, value: float):
    return (np.abs(array - value)).argmin()


def main():
    r0 = np.array([-1.5, -1.5])
    r1 = np.array([1.5, 1.5])

    # Since the spliner is well optimised, we can increase the number of points.
    # Most of the execution time comes from the O(1) part, so this can be increased
    # by orders of magnitude without significant changes in the execution time.
    # However, Matplotlib may choke in the size of the data! :D
    point_count = 1000000

    # This takes about 10 s
    # point_count = 10000000

    points = vector_between_points(r0, r1, point_count)
    # But the task is to evaluate 100 points, so let's do that as well
    points_100 = vector_between_points(r0, r1, 100)

    x_grid, y_grid, data = read_hdf5_file("warm_up_data.h5")
    # The optimized spliner expects the inputs to be in float64
    x_grid = x_grid.astype(np.float64)
    y_grid = y_grid.astype(np.float64)
    data = data.astype(np.float64)

    time1 = time.perf_counter()
    spl = spline.Spline2D(x_grid, y_grid, data)
    time2 = time.perf_counter()
    print("Constructing the spline took {:.3f} s".format(time2 - time1))
    vals = spl.eval1d(points)
    time3 = time.perf_counter()
    print("Evaluating the spline took {:.3f} s".format(time3 - time2))
    vals_100 = spl.eval1d(points_100)

    idx_01 = find_nearest_idx(points[:, 0], 0.1)
    for i in range(idx_01-1, idx_01+2):
        print("[{:.3f} {:.3f}]: {}".format(points[i, 0], points[i, 1], vals[i]))

    val_01 = spl.eval1d(np.array([[0.1, 0.1]]))[0]
    print("[0.1 0.1]: {}".format(val_01))

    # Let's assume that the colleague is ok with more data points than requested
    save_vec_to_hdf5("warm_up_output.h5", vals)     # or vals_100

    fig1, (ax1_1, ax1_2) = plt.subplots(1, 2)

    y_mesh, x_mesh = np.meshgrid(y_grid, x_grid)
    contour = ax1_1.contourf(x_mesh, y_mesh, data)
    plt.colorbar(contour, ax=ax1_1)
    ax1_1.scatter([0.1], [0.1])
    ax1_1.plot([r0[0], r1[0]], [r0[1], r1[1]])
    ax1_1.set_title("Input data")
    ax1_1.set_xlabel("x")
    ax1_1.set_ylabel("y")

    ax1_2.plot(points[:, 0], vals, label="{} points".format(point_count))
    ax1_2.plot(points_100[:, 0], vals_100, label="100 points")
    ax1_2.legend()
    ax1_2.set_title("Interpolated line")
    ax1_2.set_xlabel("x coordinate")
    ax1_2.set_ylabel("Function value")

    plt.show()


if __name__ == "__main__":
    main()
