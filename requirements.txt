coverage >= 4.5.2
cython >= 0.29.5
h5py >= 2.9.0
matplotlib >= 3.0.2
# mayavi >= 4.6.2
mpmath >= 1.0.0
numpy >= 1.15.4
numba >= 0.42.0
pandas >= 0.24.0
pylint >= 2.2.2
pyopengl >= 3.1.0   # For 3D plotting in PyQtGraph
PyQt5 >= 5.11.3
pyqtgraph >= 0.10.0
scipy >= 1.2.0
# sympy >= 1.3
sympy < 1.2     # FEniCS does not support Sympy 1.2 or newer
